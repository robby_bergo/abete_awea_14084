/PROG  CH_MAIN	  Macro
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "";
PROG_SIZE	= 2297;
CREATE		= DATE 11-05-03  TIME 15:18:32;
MODIFIED	= DATE 15-11-18  TIME 18:32:12;
FILE_NAME	= CHIAMA1;
VERSION		= 0;
LINE_COUNT	= 81;
MEMORY_SIZE	= 2729;
PROTECT		= READ;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 7;
DEFAULT_GROUP	= *,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/APPL

AUTO_SINGULARITY_HEADER;
  ENABLE_SINGULARITY_AVOIDANCE   : FALSE;
/MN
   1:   ;
   2:  IF (DO[42:Call master ]),$SHELL_WRK.$CUST_START=(1) ;
   3:   ;
   4:  IF (!DO[42:Call master ]),$SHELL_WRK.$CUST_START=(0) ;
   5:   ;
   6:  IF (DO[42:Call master ]),DO[51:Top Master]=(ON) ;
   7:   ;
   8:  IF (R[11:OVERRIDE]>100),R[11:OVERRIDE]=(100) ;
   9:   ;
  10:  IF (R[11:OVERRIDE]<0),R[11:OVERRIDE]=(0) ;
  11:   ;
  12:  IF (GI[10:PINZE]=1),R[12:N_PINZA A BORDO]=(2) ;
  13:   ;
  14:  IF (GI[10:PINZE]=2),R[12:N_PINZA A BORDO]=(1) ;
  15:   ;
  16:  IF (GI[10:PINZE]=0),R[12:N_PINZA A BORDO]=(0) ;
  17:   ;
  18:  IF (GI[10:PINZE]=3),R[12:N_PINZA A BORDO]=(GI[10:PINZE]) ;
  19:   ;
  20:   ;
  21:  IF (!SO[7:TP enabled] AND SI[8:CE/CR Select b0] AND SI[9:CE/CR Select b1]),$MCR.$GENOVERRIDE=(R[11:OVERRIDE]) ;
  22:   ;
  23:  IF (SO[7:TP enabled] AND SI[8:CE/CR Select b0] AND SI[9:CE/CR Select b1]),$MCR.$GENOVERRIDE=(10) ;
  24:   ;
  25:  IF ($OVRDSLCT.$OVSL_ENB=1 AND !SO[7:TP enabled]),$MCR.$GENOVERRIDE=(10) ;
  26:   ;
  27:   ;
  28:  R[98:ACQ_SIS_1]=($SCR_GRP[1].$MCH_ANG[1]) ;
  29:  R[99:ACQ_SIS_7]=($SCR_GRP[1].$MCH_ANG[7]) ;
  30:   ;
  31:   ;
  32:  IF (DO[42:Call master ]),F[4]=(ON) ;
  33:   ;
  34:  IF (!DO[42:Call master ]),F[4]=(OFF) ;
  35:   ;
  36:  !------------------------------ ;
  37:  !ABILITA AREE ;
  38:  !------------------------------ ;
  39:  IF ($MNUFRAMENUM[1]=7 AND !DO[53:HOME RBT] AND R[8:N�Macchina UT.]=3),$RSPACE1[5].$ENABLED=(OFF) ;
  40:   ;
  41:  IF ($MNUFRAMENUM[1]<>7 OR DO[53:HOME RBT]),$RSPACE1[5].$ENABLED=(ON) ;
  42:   ;
  43:  IF ($MNUFRAMENUM[1]=7 AND !DO[53:HOME RBT] AND R[8:N�Macchina UT.]=1),$RSPACE1[3].$ENABLED=(OFF) ;
  44:   ;
  45:  IF ($MNUFRAMENUM[1]<>7 OR DO[53:HOME RBT]),$RSPACE1[3].$ENABLED=(ON) ;
  46:   ;
  47:  !------------------------------ ;
  48:  !SCAMBIO SEGNALI PC ;
  49:  !------------------------------ ;
  50:   ;
  51:  IF (!DI[58:Pres.Pz.PLT Mag] AND DI[59:Pres.Pz.UT. Mag]),DO[55:PRES_PZ.PLT]=(ON) ;
  52:   ;
  53:  IF (DI[58:Pres.Pz.PLT Mag]),DO[55:PRES_PZ.PLT]=(OFF) ;
  54:   ;
  55:  IF (!DI[59:Pres.Pz.UT. Mag] AND DI[58:Pres.Pz.PLT Mag]),DO[56:PRES.PZ.CONI]=(ON) ;
  56:   ;
  57:  IF (DI[59:Pres.Pz.UT. Mag]),DO[56:PRES.PZ.CONI]=(OFF) ;
  58:   ;
  59:   ;
  60:  !------------------------------ ;
  61:  !PINZA CONI ;
  62:  !------------------------------ ;
  63:  IF (DO[56:PRES.PZ.CONI] AND RO[4:CH.PZ] AND !RI[3:PINZA AP.] AND !RI[4:PINZA CH]),R[101:CONO PRES-PZ]=(1) ;
  64:   ;
  65:  IF (DO[56:PRES.PZ.CONI] AND !RI[3:PINZA AP.] AND RI[4:PINZA CH] OR RI[3:PINZA AP.] OR !DO[56:PRES.PZ.CONI]),R[101:CONO PRES-PZ]=(0) ;
  66:   ;
  67:  !------------------------------ ;
  68:  !PINZA PLT ;
  69:  !------------------------------ ;
  70:  IF (DO[55:PRES_PZ.PLT] AND RO[4:CH.PZ] AND !RI[3:PINZA AP.] AND !RI[4:PINZA CH]),R[100:PLT PRES-PZ]=(1) ;
  71:   ;
  72:  IF (DO[55:PRES_PZ.PLT] AND RO[4:CH.PZ] AND RI[4:PINZA CH] AND DO[12606]),R[100:PLT PRES-PZ]=(1) ;
  73:   ;
  74:  IF (DO[55:PRES_PZ.PLT] AND !RI[3:PINZA AP.] AND RI[4:PINZA CH] AND !DO[12606] OR RI[3:PINZA AP.] OR !DO[55:PRES_PZ.PLT]),R[100:PLT PRES-PZ]=(0) ;
  75:   ;
  76:   ;
  77:  !------------------------------ ;
  78:   ;
  79:  IF (!DO[53:HOME RBT] OR R[6:Strobe]=0),R[16:OK CNT]=(0) ;
  80:   ;
  81:   ;
/POS
/END
