/PROG  BANC_1IN_DP
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "BANC_1IN_DP";
PROG_SIZE	= 2142;
CREATE		= DATE 15-10-15  TIME 15:18:28;
MODIFIED	= DATE 15-10-15  TIME 18:48:24;
FILE_NAME	= BANC_3IN;
VERSION		= 0;
LINE_COUNT	= 78;
MEMORY_SIZE	= 2614;
PROTECT		= READ;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= 1,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/APPL

AUTO_SINGULARITY_HEADER;
  ENABLE_SINGULARITY_AVOIDANCE   : TRUE;
/MN
   1:  !------------------------------- ;
   2:  !DEPOSITO BANCO 1 IN INGRESSO ;
   3:  !------------------------------- ;
   4:  UFRAME_NUM=7 ;
   5:  UTOOL_NUM=1 ;
   6:   ;
   7:  LBL[31] ;
   8:  !Controllo PLT su pinza ;
   9:  IF DI[59:Pres.Pz.UT. Mag]=ON AND DI[58:Pres.Pz.PLT Mag]=OFF,JMP LBL[30] ;
  10:  CALL MESSAGGI(24) ;
  11:  PAUSE ;
  12:  R[9:Message Allarmi]=0    ;
  13:  CALL MESSAGGI(1) ;
  14:  JMP LBL[31] ;
  15:  LBL[30] ;
  16:   ;
  17:  CALL US_7_BC1_MU1    ;
  18:   ;
  19:J P[1] 80% CNT80    ;
  20:J P[2] 80% CNT80    ;
  21:J P[3] 80% CNT50    ;
  22:  !------------------------------- ;
  23:  !CALCOLO POSIZIONE ;
  24:  !------------------------------- ;
  25:  PR[10:Prel.Dep BANCO]=PR[49:Zero XYZ FUT]    ;
  26:  PR[10,1:Prel.Dep BANCO]=R[30:OFFSET_X]+400    ;
  27:  PR[10,2:Prel.Dep BANCO]=R[31:OFFSET_Y]+0    ;
  28:  PR[10,3:Prel.Dep BANCO]=0-R[32:OFFSET_Z]    ;
  29:  PR[10,4:Prel.Dep BANCO]=(-.033)    ;
  30:  PR[10,5:Prel.Dep BANCO]=.032    ;
  31:  PR[10,6:Prel.Dep BANCO]=(-90.244)    ;
  32:  PR[10,7:Prel.Dep BANCO]=100    ;
  33:   ;
  34:  PR[50:*]=PR[49:Zero XYZ FUT]    ;
  35:  PR[50,1:*]=0    ;
  36:  PR[50,2:*]=100    ;
  37:  PR[50,3:*]=600    ;
  38:   ;
  39:  PR[51:*]=PR[49:Zero XYZ FUT]    ;
  40:  PR[51,2:*]=0    ;
  41:  PR[51,3:*]=300    ;
  42:   ;
  43:  PR[52:*]=PR[49:Zero XYZ FUT]    ;
  44:  PR[52,3:*]=100    ;
  45:   ;
  46:  PR[53:*]=PR[49:Zero XYZ FUT]    ;
  47:  PR[53,2:*]=200    ;
  48:  PR[53,3:*]=700    ;
  49:   ;
  50:  !------------------------------- ;
  51:L PR[10:Prel.Dep BANCO] 1000mm/sec CNT30 Offset,PR[53:*]    ;
  52:  !------------------------------ ;
  53:  COL DETECT ON ;
  54:  COL GUARD ADJUST 150 ;
  55:L PR[10:Prel.Dep BANCO] 600mm/sec CNT2 Offset,PR[52:*]    ;
  56:  !------------------------------- ;
  57:  !PUNTO FINALE ;
  58:L PR[10:Prel.Dep BANCO] 100mm/sec FINE    ;
  59:  COL DETECT OFF ;
  60:  !------------------------------- ;
  61:  !Apre PINZA ;
  62:  COL DETECT OFF ;
  63:  CALL P1_APRI    ;
  64:  !------------------------------ ;
  65:  COL DETECT OFF ;
  66:   ;
  67:L PR[10:Prel.Dep BANCO] 300mm/sec CNT2 Offset,PR[51:*]    ;
  68:   ;
  69:  WAIT RI[3:PINZA AP.]=ON    ;
  70:   ;
  71:L PR[10:Prel.Dep BANCO] 1000mm/sec CNT50 Offset,PR[50:*]    ;
  72:  !------------------------------- ;
  73:   ;
  74:L P[3] 1500mm/sec CNT80    ;
  75:  WAIT RI[3:PINZA AP.]=ON AND RI[4:PINZA CH]=OFF    ;
  76:J P[2] 100% CNT100    ;
  77:J P[1] 100% CNT100    ;
  78:   ;
/POS
P[1]{
   GP1:
	UF : 7, UT : 1,	
	J1=     -.001 deg,	J2=   -54.999 deg,	J3=    24.998 deg,
	J4=      .000 deg,	J5=   -25.000 deg,	J6=   -90.000 deg,
	E1=   500.000  mm
};
P[2]{
   GP1:
	UF : 7, UT : 1,	
	J1=   -10.122 deg,	J2=   -28.690 deg,	J3=   -11.229 deg,
	J4=   -68.151 deg,	J5=    28.402 deg,	J6=   -23.743 deg,
	E1=   500.000  mm
};
P[3]{
   GP1:
	UF : 7, UT : 1,	
	J1=   -21.917 deg,	J2=   -29.725 deg,	J3=   -10.864 deg,
	J4=   -92.460 deg,	J5=   103.630 deg,	J6=   -10.838 deg,
	E1=   100.000  mm
};
/END
