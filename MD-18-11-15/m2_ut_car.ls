/PROG  M2_UT_CAR
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "CAR UT 2";
PROG_SIZE	= 2941;
CREATE		= DATE 12-04-17  TIME 00:03:20;
MODIFIED	= DATE 15-10-28  TIME 20:43:04;
FILE_NAME	= ;
VERSION		= 0;
LINE_COUNT	= 121;
MEMORY_SIZE	= 3385;
PROTECT		= READ;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= 1,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/MN
   1:  !------------------------------- ;
   2:  !CARICO la Macch M2_AWEA850_UT ;
   3:  !------------------------------- ;
   4:  UFRAME_NUM=9 ;
   5:  UTOOL_NUM=2 ;
   6:   ;
   7:  CALL USER_9_UT2    ;
   8:   ;
   9:  LBL[31] ;
  10:  !Controllo PLT su pinza ;
  11:  IF DI[58:Pres.Pz.PLT Mag]=ON AND DI[59:Pres.Pz.UT. Mag]=OFF,JMP LBL[30] ;
  12:  CALL MESSAGGI(24) ;
  13:  PAUSE ;
  14:  R[9:Message Allarmi]=0    ;
  15:  CALL MESSAGGI(1) ;
  16:  JMP LBL[31] ;
  17:  LBL[30] ;
  18:   ;
  19:  !------------------------------ ;
  20:J P[1] 80% CNT50    ;
  21:J P[2] 80% CNT50    ;
  22:L P[3] 1500mm/sec CNT50    ;
  23:   ;
  24:  DO[138:CH.PT.Autom.MU2]=OFF ;
  25:  DO[137:AP.PT.Autom.MU2]=ON ;
  26:  ! Verifico stato CNC ;
  27:  WAIT DI[124:Ch.Cari.UT MU2]=ON AND DI[140:Porta Auto.AP.MU2]=ON    ;
  28:  DO[131:RBT Fuor.ing.MU2]=OFF ;
  29:   ;
  30:  !------------------------------- ;
  31:  !Calcolo Pos. pezzo ;
  32:  !------------------------------- ;
  33:  PR[12:POS_MU]=PR[47:Zero XYZ_NUT]    ;
  34:  PR[12,1:POS_MU]=0    ;
  35:  PR[12,2:POS_MU]=0    ;
  36:  PR[12,3:POS_MU]=0    ;
  37:  PR[12,4:POS_MU]=(-.003)    ;
  38:  PR[12,5:POS_MU]=.002    ;
  39:  PR[12,6:POS_MU]=(-90.01)    ;
  40:  PR[12,7:POS_MU]=200    ;
  41:   ;
  42:  PR[51:*]=PR[49:Zero XYZ FUT]    ;
  43:  PR[51,1:*]=(-700)    ;
  44:  PR[51,2:*]=(-100)    ;
  45:  PR[51,6:*]=10    ;
  46:   ;
  47:  PR[52:*]=PR[49:Zero XYZ FUT]    ;
  48:  PR[52,1:*]=50    ;
  49:  PR[52,2:*]=100    ;
  50:  PR[52,3:*]=(-110)    ;
  51:  PR[52,6:*]=20    ;
  52:   ;
  53:  PR[53:*]=PR[49:Zero XYZ FUT]    ;
  54:  PR[53,3:*]=(-110)    ;
  55:   ;
  56:  PR[54:*]=PR[49:Zero XYZ FUT]    ;
  57:  PR[54,1:*]=(-1400)    ;
  58:  PR[54,2:*]=(-200)    ;
  59:  PR[54,6:*]=10    ;
  60:  PR[54,7:*]=600    ;
  61:   ;
  62:  PR[55:*]=PR[49:Zero XYZ FUT]    ;
  63:  PR[55,2:*]=100    ;
  64:   ;
  65:  PR[56:*]=PR[49:Zero XYZ FUT]    ;
  66:  PR[56,6:*]=(-1)    ;
  67:  PR[57]=PR[49:Zero XYZ FUT]    ;
  68:  PR[57,6]=3    ;
  69:  !------------------------------ ;
  70:  COL DETECT ON ;
  71:  COL GUARD ADJUST 120 ;
  72:   ;
  73:L PR[12:POS_MU] 1000mm/sec CNT50 Offset,PR[52:*] Tool_Offset,PR[54:*]    ;
  74:L PR[12:POS_MU] 1000mm/sec CNT20 Offset,PR[52:*] Tool_Offset,PR[51:*]    ;
  75:L PR[12:POS_MU] 600mm/sec CNT1 Offset,PR[52:*]    ;
  76:L PR[12:POS_MU] 300mm/sec CNT1 Offset,PR[53:*]    ;
  77:  !------------------------------ ;
  78:  ! Sblocco il pallet ------------- ;
  79:  CALL CMD_SBL_UT_M2    ;
  80:  !------------------------------ ;
  81:   ;
  82:  ! Punto finale ----------------- ;
  83:L PR[12:POS_MU] 100mm/sec FINE    ;
  84:L PR[12:POS_MU] 100mm/sec CNT1 Offset,PR[56:*]    ;
  85:L PR[12:POS_MU] 100mm/sec CNT1 Offset,PR[57]    ;
  86:   ;
  87:  !------------------------------- ;
  88:  !Blocco il pallet ;
  89:  !------------------------------- ;
  90:  !Blocco Uensile ;
  91:  CALL CMD_BL_UT_M2    ;
  92:   ;
  93:  CALL P1_APRI    ;
  94:  !------------------------------- ;
  95:   ;
  96:L PR[12:POS_MU] 600mm/sec FINE Offset,PR[55:*]    ;
  97:   ;
  98:  !------------------------------- ;
  99:  WAIT RI[3:PINZA AP.]=ON    ;
 100:  COL DETECT OFF ;
 101:   ;
 102:  !------------------------------- ;
 103:L PR[12:POS_MU] 1000mm/sec CNT50 Offset,PR[52:*] Tool_Offset,PR[51:*]    ;
 104:L PR[12:POS_MU] 1000mm/sec CNT50 Offset,PR[52:*] Tool_Offset,PR[54:*]    ;
 105:   ;
 106:  WAIT RI[3:PINZA AP.]=ON AND RI[4:PINZA CH]=OFF    ;
 107:L P[3] 1500mm/sec CNT50    ;
 108:   ;
 109:  !FINE CARICO ------------- ;
 110:  DO[131:RBT Fuor.ing.MU2]=ON ;
 111:  WAIT DO[131:RBT Fuor.ing.MU2]=ON    ;
 112:  WAIT    .50(sec) ;
 113:  DO[137:AP.PT.Autom.MU2]=OFF ;
 114:  DO[138:CH.PT.Autom.MU2]=ON ;
 115:  DO[122:Fin.Cari.PLT MU2]=PULSE,2.0sec ;
 116:  R[6:Strobe]=3    ;
 117:  !------------------------------ ;
 118:   ;
 119:J P[2] 100% CNT50    ;
 120:J P[1] 100% CNT50    ;
 121:   ;
/POS
P[1]{
   GP1:
	UF : 9, UT : 2,	
	J1=     0.000 deg,	J2=   -55.000 deg,	J3=    25.000 deg,
	J4=     0.000 deg,	J5=   -25.000 deg,	J6=    90.000 deg,
	E1=   500.000  mm
};
P[2]{
   GP1:
	UF : 9, UT : 2,	
	J1=   -90.000 deg,	J2=   -55.000 deg,	J3=    25.000 deg,
	J4=     0.000 deg,	J5=   -25.000 deg,	J6=    90.000 deg,
	E1=   542.648  mm
};
P[3]{
   GP1:
	UF : 9, UT : 2,	
	J1=  -128.236 deg,	J2=   -55.560 deg,	J3=    10.951 deg,
	J4=   -73.996 deg,	J5=   -35.037 deg,	J6=   160.587 deg,
	E1=  1000.000  mm
};
/END
