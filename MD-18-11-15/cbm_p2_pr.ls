/PROG  CBM_P2_PR
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "CAMBIO P2 PREL";
PROG_SIZE	= 2277;
CREATE		= DATE 15-05-27  TIME 11:57:42;
MODIFIED	= DATE 15-05-27  TIME 19:20:20;
FILE_NAME	= CBM_P1_P;
VERSION		= 0;
LINE_COUNT	= 92;
MEMORY_SIZE	= 2677;
PROTECT		= READ;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= 1,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/APPL
/MN
   1:  !------------------------------- ;
   2:  !PREL PINZA PLT ;
   3:  !------------------------------- ;
   4:  IF DI[58:Pres.Pz.PLT Mag]=OFF OR DI[59:Pres.Pz.UT. Mag]=OFF,JMP LBL[1] ;
   5:   ;
   6:  UFRAME_NUM=0 ;
   7:  UTOOL_NUM=0 ;
   8:   ;
   9:  ! Verifico stato PINZA ;
  10:  WAIT DI[59:Pres.Pz.UT. Mag]=ON    ;
  11:  IF GI[10:PINZE]=3 AND DO[53:HOME RBT]=OFF,JMP LBL[2] ;
  12:   ;
  13:J P[1] 100% CNT50    ;
  14:J P[2] 100% CNT50    ;
  15:   ;
  16:  LBL[2] ;
  17:  !------------------------------- ;
  18:  !Calcolo Posizione ;
  19:  !------------------------------- ;
  20:  PR[51:*]=PR[49:Zero XYZ FUT]    ;
  21:  PR[51,1:*]=0    ;
  22:  PR[51,2:*]=(-600)    ;
  23:  PR[51,3:*]=0    ;
  24:   ;
  25:  PR[52:*]=PR[49:Zero XYZ FUT]    ;
  26:  PR[52,1:*]=0    ;
  27:  PR[52,2:*]=(-2)    ;
  28:  PR[52,3:*]=20    ;
  29:   ;
  30:  PR[53:*]=PR[49:Zero XYZ FUT]    ;
  31:  PR[53,1:*]=0    ;
  32:  PR[53,2:*]=(-20)    ;
  33:  PR[53,3:*]=0    ;
  34:   ;
  35:  PR[54:*]=PR[49:Zero XYZ FUT]    ;
  36:  PR[54,1:*]=0    ;
  37:  PR[54,2:*]=(-700)    ;
  38:  PR[54,3:*]=80    ;
  39:  PR[54,4:*]=0    ;
  40:   ;
  41:  PR[55:*]=PR[49:Zero XYZ FUT]    ;
  42:  PR[55,1:*]=0    ;
  43:  PR[55,2:*]=(-50)    ;
  44:  PR[55,3:*]=70    ;
  45:  PR[55,4:*]=0    ;
  46:   ;
  47:  PR[56:*]=PR[49:Zero XYZ FUT]    ;
  48:  PR[56,1:*]=0    ;
  49:  PR[56,2:*]=(-100)    ;
  50:  PR[56,3:*]=0    ;
  51:   ;
  52:L P[3:PINZA PLT] 1000mm/sec CNT50 Offset,PR[51:*]    ;
  53:L P[3:PINZA PLT] 600mm/sec CNT20 Offset,PR[56:*]    ;
  54:   ;
  55:  RO[1:CBM.PZ]=ON ;
  56:L P[3:PINZA PLT] 300mm/sec FINE Offset,PR[53:*]    ;
  57:   ;
  58:  RO[1:CBM.PZ]=ON ;
  59:  WAIT RI[1:CBM.ACC]=ON    ;
  60:  WAIT    .50(sec) ;
  61:   ;
  62:  !------------------------------- ;
  63:  COL DETECT ON ;
  64:  COL GUARD ADJUST 120 ;
  65:   ;
  66:  ! Punto finale ----------------- ;
  67:L P[3:PINZA PLT] 50mm/sec FINE    ;
  68:  !------------------------------- ;
  69:  !Blocco la pinza ;
  70:  RO[1:CBM.PZ]=OFF ;
  71:  WAIT RI[1:CBM.ACC]=OFF    ;
  72:  COL DETECT OFF ;
  73:  WAIT    .50(sec) ;
  74:  !------------------------------- ;
  75:   ;
  76:L P[3:PINZA PLT] 600mm/sec CNT1 Offset,PR[52:*]    ;
  77:   ;
  78:  !------------------------------- ;
  79:  !CNT.PRESENZA PINZA ;
  80:  WAIT DI[59:Pres.Pz.UT. Mag]=OFF    ;
  81:  CALL P1_APRI    ;
  82:  WAIT RI[1:CBM.ACC]=OFF AND RI[3:PINZA AP.]=ON    ;
  83:  !------------------------------- ;
  84:L P[3:PINZA PLT] 600mm/sec FINE Offset,PR[55:*]    ;
  85:L P[3:PINZA PLT] 1000mm/sec CNT50 Offset,PR[54:*]    ;
  86:   ;
  87:  WAIT RI[1:CBM.ACC]=OFF AND RI[3:PINZA AP.]=ON    ;
  88:   ;
  89:L P[2] 1000mm/sec CNT100    ;
  90:J P[1] 100% CNT50    ;
  91:   ;
  92:  LBL[1] ;
/POS
P[1]{
   GP1:
	UF : 0, UT : 0,	
	J1=     0.000 deg,	J2=   -55.000 deg,	J3=    25.000 deg,
	J4=      .000 deg,	J5=   -25.000 deg,	J6=    90.000 deg,
	E1=  8000.000  mm
};
P[2]{
   GP1:
	UF : 0, UT : 0,	
	J1=   -10.505 deg,	J2=   -38.442 deg,	J3=    10.331 deg,
	J4=    88.001 deg,	J5=   100.261 deg,	J6=     0.000 deg,
	E1=  8000.000  mm
};
P[3:"PINZA PLT"]{
   GP1:
	UF : 0, UT : 0,		CONFIG : 'F U T, 0, 0, 0',
	X =  9401.312  mm,	Y =   920.000  mm,	Z =    71.305  mm,
	W =    89.699 deg,	P =      .186 deg,	R =  -179.558 deg,
	E1=  8000.003  mm
};
/END
