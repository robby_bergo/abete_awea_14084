/PROG  CBM_P1_PR
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "CAMBIO P1 PREL";
PROG_SIZE	= 2301;
CREATE		= DATE 15-05-27  TIME 11:18:32;
MODIFIED	= DATE 15-10-27  TIME 14:59:06;
FILE_NAME	= CBM_P1_D;
VERSION		= 0;
LINE_COUNT	= 93;
MEMORY_SIZE	= 2697;
PROTECT		= READ;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= 1,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/APPL
/MN
   1:  !------------------------------- ;
   2:  !PREL PINZA PLT ;
   3:  !------------------------------- ;
   4:  IF DI[58:Pres.Pz.PLT Mag]=OFF OR DI[59:Pres.Pz.UT. Mag]=OFF,JMP LBL[1] ;
   5:   ;
   6:  UFRAME_NUM=0 ;
   7:  UTOOL_NUM=0 ;
   8:   ;
   9:  ! Verifico stato PINZA ;
  10:  WAIT DI[58:Pres.Pz.PLT Mag]=ON    ;
  11:  IF GI[10:PINZE]=3 AND DO[53:HOME RBT]=OFF,JMP LBL[2] ;
  12:   ;
  13:J P[1] 100% CNT50    ;
  14:J P[2] 100% CNT50    ;
  15:   ;
  16:  LBL[2] ;
  17:  !------------------------------- ;
  18:  !Calcolo Posizione ;
  19:  !------------------------------- ;
  20:  PR[51:*]=PR[49:Zero XYZ FUT]    ;
  21:  PR[51,1:*]=0    ;
  22:  PR[51,2:*]=(-600)    ;
  23:  PR[51,3:*]=0    ;
  24:   ;
  25:  PR[52:*]=PR[49:Zero XYZ FUT]    ;
  26:  PR[52,1:*]=0    ;
  27:  PR[52,2:*]=0    ;
  28:  PR[52,3:*]=40    ;
  29:   ;
  30:  PR[53:*]=PR[49:Zero XYZ FUT]    ;
  31:  PR[53,1:*]=0    ;
  32:  PR[53,2:*]=(-30)    ;
  33:  PR[53,3:*]=0    ;
  34:   ;
  35:  PR[54:*]=PR[49:Zero XYZ FUT]    ;
  36:  PR[54,1:*]=0    ;
  37:  PR[54,2:*]=(-700)    ;
  38:  PR[54,3:*]=200    ;
  39:  PR[54,4:*]=0    ;
  40:   ;
  41:  PR[55:*]=PR[49:Zero XYZ FUT]    ;
  42:  PR[55,1:*]=0    ;
  43:  PR[55,2:*]=(-50)    ;
  44:  PR[55,3:*]=100    ;
  45:  PR[55,4:*]=0    ;
  46:   ;
  47:  PR[56:*]=PR[49:Zero XYZ FUT]    ;
  48:  PR[56,1:*]=0    ;
  49:  PR[56,2:*]=(-100)    ;
  50:  PR[56,3:*]=0    ;
  51:   ;
  52:L P[3:PINZA PLT] 1000mm/sec CNT50 Offset,PR[51:*]    ;
  53:L P[3:PINZA PLT] 600mm/sec CNT20 Offset,PR[53:*]    ;
  54:   ;
  55:  RO[1:CBM.PZ]=ON ;
  56:  WAIT RI[1:CBM.ACC]=ON    ;
  57:L P[3:PINZA PLT] 300mm/sec FINE Offset,PR[53:*]    ;
  58:   ;
  59:  RO[1:CBM.PZ]=ON ;
  60:  WAIT RI[1:CBM.ACC]=ON    ;
  61:  WAIT    .50(sec) ;
  62:   ;
  63:  !------------------------------- ;
  64:  COL DETECT ON ;
  65:  COL GUARD ADJUST 120 ;
  66:   ;
  67:  ! Punto finale ----------------- ;
  68:L P[3:PINZA PLT] 100mm/sec FINE    ;
  69:  !------------------------------- ;
  70:  !Blocco la pinza ;
  71:  RO[1:CBM.PZ]=OFF ;
  72:  WAIT RI[1:CBM.ACC]=OFF    ;
  73:  COL DETECT OFF ;
  74:  WAIT    .50(sec) ;
  75:  !------------------------------- ;
  76:   ;
  77:L P[3:PINZA PLT] 600mm/sec CNT1 Offset,PR[52:*]    ;
  78:   ;
  79:  !------------------------------- ;
  80:  !CNT.PRESENZA PINZA ;
  81:  WAIT DI[58:Pres.Pz.PLT Mag]=OFF    ;
  82:  CALL P1_APRI    ;
  83:  WAIT RI[1:CBM.ACC]=OFF AND RI[3:PINZA AP.]=ON    ;
  84:  !------------------------------- ;
  85:L P[3:PINZA PLT] 600mm/sec FINE Offset,PR[55:*]    ;
  86:L P[3:PINZA PLT] 1000mm/sec CNT50 Offset,PR[54:*]    ;
  87:   ;
  88:  WAIT RI[1:CBM.ACC]=OFF AND RI[3:PINZA AP.]=ON    ;
  89:   ;
  90:L P[2] 1000mm/sec CNT100    ;
  91:J P[1] 100% CNT50    ;
  92:   ;
  93:  LBL[1] ;
/POS
P[1]{
   GP1:
	UF : 0, UT : 0,	
	J1=     0.000 deg,	J2=   -55.000 deg,	J3=    25.000 deg,
	J4=      .000 deg,	J5=   -25.000 deg,	J6=   -90.000 deg,
	E1=  8000.000  mm
};
P[2]{
   GP1:
	UF : 0, UT : 0,		CONFIG : 'F U T, 0, 0, -1',
	X =  9191.682  mm,	Y =    -5.945  mm,	Z =  1356.951  mm,
	W =   -89.896 deg,	P =     -.498 deg,	R =     -.054 deg,
	E1=  8000.000  mm
};
P[3:"PINZA PLT"]{
   GP1:
	UF : 0, UT : 0,		CONFIG : 'F U T, 0, 0, 0',
	X = 10089.709  mm,	Y =   755.207  mm,	Z =   142.500  mm,
	W =   -89.896 deg,	P =     -.498 deg,	R =     -.054 deg,
	E1=  8000.000  mm
};
/END
