/PROG  BANC_3IN_PR
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "BANC_3IN_PR";
PROG_SIZE	= 2270;
CREATE		= DATE 15-10-15  TIME 14:30:42;
MODIFIED	= DATE 15-10-15  TIME 18:49:26;
FILE_NAME	= BANC_3IN;
VERSION		= 0;
LINE_COUNT	= 85;
MEMORY_SIZE	= 2714;
PROTECT		= READ;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= 1,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/APPL

AUTO_SINGULARITY_HEADER;
  ENABLE_SINGULARITY_AVOIDANCE   : TRUE;
/MN
   1:  !------------------------------- ;
   2:  !DEPOSITO BANCO 1 IN INGRESSO ;
   3:  !------------------------------- ;
   4:  UFRAME_NUM=7 ;
   5:  UTOOL_NUM=1 ;
   6:   ;
   7:  LBL[31] ;
   8:  !Controllo PLT su pinza ;
   9:  IF DI[59:Pres.Pz.UT. Mag]=ON AND DI[58:Pres.Pz.PLT Mag]=OFF,JMP LBL[30] ;
  10:  CALL MESSAGGI(24) ;
  11:  PAUSE ;
  12:  R[9:Message Allarmi]=0    ;
  13:  CALL MESSAGGI(1) ;
  14:  JMP LBL[31] ;
  15:  LBL[30] ;
  16:   ;
  17:  CALL US_7_BC3_MU3    ;
  18:   ;
  19:  LBL[20] ;
  20:  !Controllo PLT su pinza ;
  21:  IF RI[3:PINZA AP.]=ON AND RI[4:PINZA CH]=OFF,JMP LBL[10] ;
  22:  CALL MESSAGGI(16) ;
  23:  PAUSE ;
  24:  R[9:Message Allarmi]=0    ;
  25:  CALL MESSAGGI(1) ;
  26:  JMP LBL[20] ;
  27:  LBL[10] ;
  28:   ;
  29:J P[1] 100% CNT100    ;
  30:J P[2] 100% CNT100    ;
  31:J P[3] 90% CNT80    ;
  32:  !------------------------------- ;
  33:  !CALCOLO POSIZIONE ;
  34:  !------------------------------- ;
  35:  PR[10:Prel.Dep BANCO]=PR[49:Zero XYZ FUT]    ;
  36:  PR[10,1:Prel.Dep BANCO]=R[30:OFFSET_X]+400    ;
  37:  PR[10,2:Prel.Dep BANCO]=R[31:OFFSET_Y]+0    ;
  38:  PR[10,3:Prel.Dep BANCO]=0-R[32:OFFSET_Z]    ;
  39:  PR[10,4:Prel.Dep BANCO]=(-.464)    ;
  40:  PR[10,5:Prel.Dep BANCO]=.036    ;
  41:  PR[10,6:Prel.Dep BANCO]=(-89.655)    ;
  42:  PR[10,7:Prel.Dep BANCO]=6500    ;
  43:   ;
  44:  PR[50:*]=PR[49:Zero XYZ FUT]    ;
  45:  PR[50,1:*]=0    ;
  46:  PR[50,2:*]=100    ;
  47:  PR[50,3:*]=600    ;
  48:   ;
  49:  PR[51:*]=PR[49:Zero XYZ FUT]    ;
  50:  PR[51,2:*]=0    ;
  51:  PR[51,3:*]=100    ;
  52:   ;
  53:  PR[52:*]=PR[49:Zero XYZ FUT]    ;
  54:  PR[52,3:*]=100    ;
  55:   ;
  56:  PR[53:*]=PR[49:Zero XYZ FUT]    ;
  57:  PR[53,2:*]=200    ;
  58:  PR[53,3:*]=700    ;
  59:   ;
  60:  !------------------------------- ;
  61:L PR[10:Prel.Dep BANCO] 1000mm/sec CNT30 Offset,PR[50:*]    ;
  62:  !------------------------------ ;
  63:  COL DETECT ON ;
  64:  COL GUARD ADJUST 150 ;
  65:L PR[10:Prel.Dep BANCO] 600mm/sec CNT2 Offset,PR[51:*]    ;
  66:  !------------------------------- ;
  67:  !PUNTO FINALE ;
  68:L PR[10:Prel.Dep BANCO] 200mm/sec FINE    ;
  69:  !------------------------------- ;
  70:  !CHIUSURA PINZA ;
  71:  CALL P1_CHIUD    ;
  72:   ;
  73:L PR[10:Prel.Dep BANCO] 300mm/sec CNT2 Offset,PR[52:*]    ;
  74:   ;
  75:  COL DETECT OFF ;
  76:  WAIT RI[4:PINZA CH]=OFF    ;
  77:   ;
  78:L PR[10:Prel.Dep BANCO] 1000mm/sec CNT50 Offset,PR[53:*]    ;
  79:  !------------------------------- ;
  80:   ;
  81:L P[3] 1500mm/sec CNT80    ;
  82:  WAIT RI[3:PINZA AP.]=OFF AND RI[4:PINZA CH]=OFF    ;
  83:J P[2] 80% CNT80    ;
  84:J P[1] 80% CNT80    ;
  85:   ;
/POS
P[1]{
   GP1:
	UF : 7, UT : 1,	
	J1=     -.001 deg,	J2=   -54.999 deg,	J3=    24.998 deg,
	J4=      .000 deg,	J5=   -25.000 deg,	J6=   -90.000 deg,
	E1=  6000.000  mm
};
P[2]{
   GP1:
	UF : 7, UT : 1,	
	J1=   -10.122 deg,	J2=   -28.690 deg,	J3=   -11.229 deg,
	J4=   -68.151 deg,	J5=    28.402 deg,	J6=   -23.743 deg,
	E1=  5500.000  mm
};
P[3]{
   GP1:
	UF : 7, UT : 1,	
	J1=   -21.917 deg,	J2=   -29.725 deg,	J3=   -10.864 deg,
	J4=   -92.460 deg,	J5=   103.630 deg,	J6=   -10.838 deg,
	E1=  6000.000  mm
};
/END
