/PROG  CBM_P2_DP
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "CAMBIO P2 DEPO";
PROG_SIZE	= 2598;
CREATE		= DATE 15-05-27  TIME 12:24:52;
MODIFIED	= DATE 15-10-29  TIME 12:15:18;
FILE_NAME	= CBM_P2_P;
VERSION		= 0;
LINE_COUNT	= 106;
MEMORY_SIZE	= 3078;
PROTECT		= READ;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= 1,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/APPL
/MN
   1:  !------------------------------- ;
   2:  !PREL PINZA PLT ;
   3:  !------------------------------- ;
   4:  IF DI[59:Pres.Pz.UT. Mag]=ON,JMP LBL[1] ;
   5:   ;
   6:  LBL[31] ;
   7:  !Controllo PLT su pinza ;
   8:  IF DI[58:Pres.Pz.PLT Mag]=ON AND DI[59:Pres.Pz.UT. Mag]=OFF,JMP LBL[30] ;
   9:  CALL MESSAGGI(24) ;
  10:  PAUSE ;
  11:  R[9:Message Allarmi]=0    ;
  12:  CALL MESSAGGI(1) ;
  13:  JMP LBL[31] ;
  14:  LBL[30] ;
  15:   ;
  16:  LBL[20] ;
  17:  !Controllo PLT su pinza ;
  18:  IF RI[3:PINZA AP.]=ON,JMP LBL[10] ;
  19:  CALL MESSAGGI(16) ;
  20:  PAUSE ;
  21:  R[9:Message Allarmi]=0    ;
  22:  CALL MESSAGGI(1) ;
  23:  JMP LBL[20] ;
  24:  LBL[10] ;
  25:   ;
  26:  CALL AZZERA_CMD    ;
  27:   ;
  28:  UFRAME_NUM=0 ;
  29:  UTOOL_NUM=0 ;
  30:   ;
  31:  ! Verifico stato PINZA ;
  32:  WAIT DI[59:Pres.Pz.UT. Mag]=OFF    ;
  33:   ;
  34:J P[1] 100% CNT50    ;
  35:J P[2] 100% CNT50    ;
  36:  !------------------------------- ;
  37:  !Calcolo Posizione ;
  38:  !------------------------------- ;
  39:  PR[51:*]=PR[49:Zero XYZ FUT]    ;
  40:  PR[51,1:*]=0    ;
  41:  PR[51,2:*]=(-600)    ;
  42:  PR[51,3:*]=0    ;
  43:   ;
  44:  PR[52:*]=PR[49:Zero XYZ FUT]    ;
  45:  PR[52,1:*]=0    ;
  46:  PR[52,2:*]=(-2)    ;
  47:  PR[52,3:*]=20    ;
  48:   ;
  49:  PR[53:*]=PR[49:Zero XYZ FUT]    ;
  50:  PR[53,1:*]=0    ;
  51:  PR[53,2:*]=(-20)    ;
  52:  PR[53,3:*]=0    ;
  53:   ;
  54:  PR[54:*]=PR[49:Zero XYZ FUT]    ;
  55:  PR[54,1:*]=0    ;
  56:  PR[54,2:*]=(-700)    ;
  57:  PR[54,3:*]=80    ;
  58:  PR[54,4:*]=0    ;
  59:   ;
  60:  PR[55:*]=PR[49:Zero XYZ FUT]    ;
  61:  PR[55,1:*]=0    ;
  62:  PR[55,2:*]=(-50)    ;
  63:  PR[55,3:*]=70    ;
  64:  PR[55,4:*]=0    ;
  65:   ;
  66:  PR[56:*]=PR[49:Zero XYZ FUT]    ;
  67:  PR[56,1:*]=0    ;
  68:  PR[56,2:*]=(-5)    ;
  69:  PR[56,3:*]=0    ;
  70:   ;
  71:  WAIT DI[59:Pres.Pz.UT. Mag]=OFF    ;
  72:L P[3:PINZA PLT] 1000mm/sec CNT50 Offset,PR[54:*]    ;
  73:L P[3:PINZA PLT] 600mm/sec CNT50 Offset,PR[55:*]    ;
  74:   ;
  75:L P[3:PINZA PLT] 600mm/sec FINE Offset,PR[52:*]    ;
  76:   ;
  77:  !------------------------------- ;
  78:  COL DETECT ON ;
  79:  COL GUARD ADJUST 120 ;
  80:   ;
  81:  ! Punto finale ----------------- ;
  82:L P[3:PINZA PLT] 100mm/sec FINE    ;
  83:  !------------------------------- ;
  84:  !SBlocco la pinza ;
  85:  RO[1:CBM.PZ]=ON ;
  86:  WAIT RI[1:CBM.ACC]=ON    ;
  87:  COL DETECT OFF ;
  88:  WAIT    .50(sec) ;
  89:  !------------------------------- ;
  90:   ;
  91:L P[3:PINZA PLT] 50mm/sec FINE Offset,PR[56:*]    ;
  92:   ;
  93:  !------------------------------- ;
  94:  !CNT.PRESENZA PINZA ;
  95:  WAIT RI[1:CBM.ACC]=ON AND RI[3:PINZA AP.]=OFF    ;
  96:  RO[3:AP PZ]=OFF ;
  97:  RO[4:CH.PZ]=OFF ;
  98:  !------------------------------- ;
  99:L P[4:PINZA PLT] 300mm/sec CNT1 Offset,PR[53:*]    ;
 100:L P[3:PINZA PLT] 1000mm/sec CNT50 Offset,PR[51:*]    ;
 101:   ;
 102:  IF R[5:Tipo di pinza]<>0,JMP LBL[1] ;
 103:   ;
 104:L P[2] 1000mm/sec CNT100    ;
 105:J P[1] 100% CNT50    ;
 106:  LBL[1] ;
/POS
P[1]{
   GP1:
	UF : 0, UT : 0,	
	J1=     0.000 deg,	J2=   -55.000 deg,	J3=    25.000 deg,
	J4=      .000 deg,	J5=   -25.000 deg,	J6=    90.000 deg,
	E1=  8000.000  mm
};
P[2]{
   GP1:
	UF : 0, UT : 0,	
	J1=   -10.505 deg,	J2=   -38.442 deg,	J3=    10.331 deg,
	J4=    88.001 deg,	J5=   100.261 deg,	J6=     0.000 deg,
	E1=  8000.000  mm
};
P[3:"PINZA PLT"]{
   GP1:
	UF : 0, UT : 0,		CONFIG : 'F U T, 0, 0, 0',
	X =  9401.312  mm,	Y =   920.000  mm,	Z =    71.305  mm,
	W =    89.699 deg,	P =      .186 deg,	R =  -179.558 deg,
	E1=  8000.003  mm
};
P[4:"PINZA PLT"]{
   GP1:
	UF : 0, UT : 0,		CONFIG : 'F U T, 0, 0, 0',
	X =  9401.312  mm,	Y =   920.000  mm,	Z =    71.305  mm,
	W =    89.699 deg,	P =      .186 deg,	R =  -179.558 deg,
	E1=  8000.003  mm
};
/END
