/PROG  AINIZIO
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "Gestione HOME";
PROG_SIZE	= 1276;
CREATE		= DATE 05-05-09  TIME 21:19:42;
MODIFIED	= DATE 15-02-27  TIME 16:34:10;
FILE_NAME	= ;
VERSION		= 0;
LINE_COUNT	= 58;
MEMORY_SIZE	= 1724;
PROTECT		= READ;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= 1,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/MN
   1:  !*** Gestione Home TP *** ;
   2:  LBL[1] ;
   3:   ;
   4:  CALL MESSAGGI(1) ;
   5:  MESSAGE[ ] ;
   6:  MESSAGE[ATTENZIONE ROBOT AUTO!] ;
   7:  MESSAGE[ ] ;
   8:  MESSAGE[Mettere Il Robot] ;
   9:  MESSAGE[In Manuale Per Muoverlo] ;
  10:  MESSAGE[In Una Posizione Sicura] ;
  11:  MESSAGE[Per Poter Raggiungere] ;
  12:  MESSAGE[La Posizione Di HOME!] ;
  13:  MESSAGE[ ] ;
  14:  CALL FKEY_COM('   SI  ','       ','       ','       ','   NO  ') ;
  15:  CALL TPFKEY('Attesa Risposta:',50) ;
  16:  CALL FKEY_CLR    ;
  17:  CALL MESSAGGI(1) ;
  18:   ;
  19:  IF R[50:TPFKEY]<>1,JMP LBL[1] ;
  20:  R[50:TPFKEY]=0    ;
  21:   ;
  22:  CALL FKEY_CLR    ;
  23:  MESSAGE[ ] ;
  24:  MESSAGE[ ] ;
  25:  MESSAGE[ATTENZIONE Il Robot Pu�] ;
  26:  MESSAGE[Raggiungere La] ;
  27:  MESSAGE[Posizione Di HOME?] ;
  28:  MESSAGE[ ] ;
  29:  MESSAGE[ATTENZIONE Sei Sicuro?] ;
  30:  MESSAGE[ ] ;
  31:  MESSAGE[ ] ;
  32:  CALL FKEY_COM('   MUOVI','       ','       ','       ','   NO  ') ;
  33:  CALL TPFKEY('Attesa Risposta:',50) ;
  34:  CALL FKEY_CLR    ;
  35:  CALL MESSAGGI(1) ;
  36:   ;
  37:  IF R[50:TPFKEY]<>1,JMP LBL[1] ;
  38:  R[50:TPFKEY]=0    ;
  39:   ;
  40:  UFRAME_NUM=0 ;
  41:  UTOOL_NUM=0 ;
  42:   ;
  43:  IF DO[53:HOME RBT]=OFF,JMP LBL[10] ;
  44:   ;
  45:  R[59:APPOG_OVER]=20    ;
  46:  R[60:OVERRIDE]=20    ;
  47:  CALL HOME_RBT    ;
  48:  CALL AZZERA    ;
  49:  JMP LBL[20] ;
  50:   ;
  51:  LBL[10] ;
  52:  JMP LBL[1] ;
  53:   ;
  54:  LBL[20] ;
  55:  R[9:Message Allarmi]=0    ;
  56:  END ;
  57:   ;
  58:   ;
/POS
/END
