/PROG  CH_MAIN_OLD	  Macro
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "";
PROG_SIZE	= 2427;
CREATE		= DATE 15-11-18  TIME 13:06:28;
MODIFIED	= DATE 15-11-18  TIME 13:06:28;
FILE_NAME	= CH_MAIN;
VERSION		= 0;
LINE_COUNT	= 93;
MEMORY_SIZE	= 2811;
PROTECT		= READ;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 7;
DEFAULT_GROUP	= *,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/APPL

AUTO_SINGULARITY_HEADER;
  ENABLE_SINGULARITY_AVOIDANCE   : FALSE;
/MN
   1:   ;
   2:  IF (DO[42:Call master ]),$SHELL_WRK.$CUST_START=(1) ;
   3:   ;
   4:  IF (!DO[42:Call master ]),$SHELL_WRK.$CUST_START=(0) ;
   5:   ;
   6:  IF (DO[42:Call master ]),DO[51:Top Master]=(ON) ;
   7:   ;
   8:  IF (R[11:OVERRIDE]>100),R[11:OVERRIDE]=(100) ;
   9:   ;
  10:  IF (R[11:OVERRIDE]<0),R[11:OVERRIDE]=(0) ;
  11:   ;
  12:  IF (GI[10:PINZE]=1),R[12:N_PINZA A BORDO]=(2) ;
  13:   ;
  14:  IF (GI[10:PINZE]=2),R[12:N_PINZA A BORDO]=(1) ;
  15:   ;
  16:  IF (GI[10:PINZE]=0),R[12:N_PINZA A BORDO]=(0) ;
  17:   ;
  18:  IF (GI[10:PINZE]=3),R[12:N_PINZA A BORDO]=(GI[10:PINZE]) ;
  19:   ;
  20:   ;
  21:  IF (!SO[7:TP enabled] AND SI[8:CE/CR Select b0] AND SI[9:CE/CR Select b1]),$MCR.$GENOVERRIDE=(R[11:OVERRIDE]) ;
  22:   ;
  23:  IF (SO[7:TP enabled] AND SI[8:CE/CR Select b0] AND SI[9:CE/CR Select b1]),$MCR.$GENOVERRIDE=(10) ;
  24:   ;
  25:  IF ($OVRDSLCT.$OVSL_ENB=1 AND !SO[7:TP enabled]),$MCR.$GENOVERRIDE=(10) ;
  26:   ;
  27:   ;
  28:  R[98:ACQ_SIS_1]=($SCR_GRP[1].$MCH_ANG[1]) ;
  29:  R[99:ACQ_SIS_7]=($SCR_GRP[1].$MCH_ANG[7]) ;
  30:   ;
  31:   ;
  32:  IF (DO[42:Call master ]),F[4]=(ON) ;
  33:   ;
  34:  IF (!DO[42:Call master ]),F[4]=(OFF) ;
  35:   ;
  36:  !------------------------------ ;
  37:  !SCAMBIO SEGNALI PC ;
  38:  !------------------------------ ;
  39:   ;
  40:  IF (DI[49:Modul emergenza ok]),R[100:PLT PRES-PZ]=(1) ;
  41:  IF (!DI[49:Modul emergenza ok]),R[100:PLT PRES-PZ]=(0) ;
  42:  IF (DI[50:Modul cancel ok]),R[101:CONO PRES-PZ]=(1) ;
  43:  IF (!DI[50:Modul cancel ok]),R[101:CONO PRES-PZ]=(0) ;
  44:  IF (DI[57:Aria OK]),R[102]=(1) ;
  45:  IF (!DI[57:Aria OK]),R[102]=(0) ;
  46:   ;
  47:  IF (DI[60:Rich. Ap. Canc.]),R[103]=(1) ;
  48:  IF (!DI[60:Rich. Ap. Canc.]),R[103]=(0) ;
  49:  IF (DI[65:OK ENTRA St1-C1]),R[104]=(1) ;
  50:  IF (!DI[65:OK ENTRA St1-C1]),R[104]=(0) ;
  51:  IF (DI[73:OK ENTRARE Staz2]),R[105]=(1) ;
  52:  IF (!DI[73:OK ENTRARE Staz2]),R[105]=(0) ;
  53:   ;
  54:  IF (DI[66:PLT Pres.St-1-C1]),R[106]=(1) ;
  55:  IF (!DI[66:PLT Pres.St-1-C1]),R[106]=(0) ;
  56:  IF (DI[75:PLT Pres.Staz2]),R[107]=(1) ;
  57:  IF (!DI[75:PLT Pres.Staz2]),R[107]=(0) ;
  58:   ;
  59:  IF (DI[108:Porta Auto.AP.MU1]),R[108]=(1) ;
  60:  IF (!DI[108:Porta Auto.AP.MU1]),R[108]=(0) ;
  61:  IF (DI[140:Porta Auto.AP.MU2]),R[109]=(1) ;
  62:  IF (!DI[140:Porta Auto.AP.MU2]),R[109]=(0) ;
  63:  IF (DI[172:Porta Auto.AP.MU3]),R[110]=(1) ;
  64:  IF (!DI[172:Porta Auto.AP.MU3]),R[110]=(0) ;
  65:  IF (DI[204:Porta Auto.AP.MU4]),R[111]=(1) ;
  66:  IF (!DI[204:Porta Auto.AP.MU4]),R[111]=(0) ;
  67:  IF (DI[236:Porta Auto.AP.MU5]),R[112]=(1) ;
  68:  IF (!DI[236:Porta Auto.AP.MU5]),R[112]=(0) ;
  69:  IF (DI[268:Porta Auto.AP.MU6]),R[113]=(1) ;
  70:  IF (!DI[268:Porta Auto.AP.MU6]),R[113]=(0) ;
  71:   ;
  72:   ;
  73:   ;
  74:  IF (!DI[58:Pres.Pz.PLT Mag] AND DI[59:Pres.Pz.UT. Mag]),DO[55:PRES_PZ.PLT]=(ON) ;
  75:   ;
  76:  IF (DI[58:Pres.Pz.PLT Mag]),DO[55:PRES_PZ.PLT]=(OFF) ;
  77:   ;
  78:  IF (!DI[59:Pres.Pz.UT. Mag] AND DI[58:Pres.Pz.PLT Mag]),DO[56:PRES.PZ.CONI]=(ON) ;
  79:   ;
  80:  IF (DI[59:Pres.Pz.UT. Mag]),DO[56:PRES.PZ.CONI]=(OFF) ;
  81:   ;
  82:   ;
  83:  IF (!DO[53:HOME RBT] AND R[6:Strobe]=0),R[16:OK CNT]=(0) ;
  84:   ;
  85:  IF ($MNUFRAMENUM[1]=7 AND !DO[53:HOME RBT] AND R[8:N�Macchina UT.]=3),$RSPACE1[5].$ENABLED=(OFF) ;
  86:   ;
  87:  IF ($MNUFRAMENUM[1]<>7 OR DO[53:HOME RBT]),$RSPACE1[5].$ENABLED=(ON) ;
  88:   ;
  89:  IF ($MNUFRAMENUM[1]=7 AND !DO[53:HOME RBT] AND R[8:N�Macchina UT.]=1),$RSPACE1[3].$ENABLED=(OFF) ;
  90:   ;
  91:  IF ($MNUFRAMENUM[1]<>7 OR DO[53:HOME RBT]),$RSPACE1[3].$ENABLED=(ON) ;
  92:   ;
  93:   ;
/POS
/END
