/PROG  P1_CMD	  Macro
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "APRI-CHIUDI PZ_1";
PROG_SIZE	= 564;
CREATE		= DATE 07-04-06  TIME 00:17:16;
MODIFIED	= DATE 15-02-27  TIME 16:34:20;
FILE_NAME	= ;
VERSION		= 0;
LINE_COUNT	= 27;
MEMORY_SIZE	= 968;
PROTECT		= READ;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 7;
DEFAULT_GROUP	= *,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/MN
   1:  !------------------------------- ;
   2:  !Comando apri-chiudi ;
   3:  IF RO[3:AP PZ]=ON,JMP LBL[1] ;
   4:  R[50:TPFKEY]=R[50:TPFKEY]+1    ;
   5:   ;
   6:  IF R[50:TPFKEY]=1,JMP LBL[2] ;
   7:  IF R[50:TPFKEY]=2,JMP LBL[3] ;
   8:   ;
   9:   ;
  10:  RO[3:AP PZ]=ON ;
  11:  RO[4:CH.PZ]=OFF ;
  12:  R[50:TPFKEY]=0    ;
  13:  CALL MESSAGGI(1) ;
  14:  END ;
  15:   ;
  16:  LBL[1] ;
  17:  RO[3:AP PZ]=OFF ;
  18:  RO[4:CH.PZ]=ON ;
  19:  R[50:TPFKEY]=0    ;
  20:   ;
  21:  END ;
  22:  LBL[2] ;
  23:  CALL MESSAGGI(18) ;
  24:  PAUSE ;
  25:  LBL[3] ;
  26:  CALL MESSAGGI(19) ;
  27:  PAUSE ;
/POS
/END
