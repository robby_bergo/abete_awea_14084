/PROG  M4_AWEA850_CAR
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "CAR AWEA850 4";
PROG_SIZE	= 2923;
CREATE		= DATE 12-04-17  TIME 00:03:20;
MODIFIED	= DATE 15-10-28  TIME 20:42:08;
FILE_NAME	= ;
VERSION		= 0;
LINE_COUNT	= 119;
MEMORY_SIZE	= 3383;
PROTECT		= READ;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= 1,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/MN
   1:  !------------------------------- ;
   2:  !CARICO la Macch M4_AWEA850 ;
   3:  !------------------------------- ;
   4:  UFRAME_NUM=8 ;
   5:  UTOOL_NUM=1 ;
   6:   ;
   7:  CALL USER_8_MU4    ;
   8:   ;
   9:  LBL[31] ;
  10:  !Controllo PLT su pinza ;
  11:  IF DI[59:Pres.Pz.UT. Mag]=ON AND DI[58:Pres.Pz.PLT Mag]=OFF,JMP LBL[30] ;
  12:  CALL MESSAGGI(24) ;
  13:  PAUSE ;
  14:  R[9:Message Allarmi]=0    ;
  15:  CALL MESSAGGI(1) ;
  16:  JMP LBL[31] ;
  17:  LBL[30] ;
  18:   ;
  19:  !CNT Sblocco Del pallet --------- ;
  20:  IF DI[205:PLT Sblocc.MU4]=OFF,CALL CMD_SBL_PLT_M4 ;
  21:   ;
  22:  !------------------------------ ;
  23:J P[1] 80% CNT50    ;
  24:J P[2] 80% CNT50    ;
  25:L P[3] 1500mm/sec CNT50    ;
  26:   ;
  27:  DO[202:CH.PT.Autom.MU4]=OFF ;
  28:  DO[201:AP.PT.Autom.MU4]=ON ;
  29:  ! Verifico stato CNC ;
  30:  WAIT DI[186:Ch.Cari.PLT MU4]=ON AND DI[204:Porta Auto.AP.MU4]=ON    ;
  31:  DO[195:RBT Fuor.ing.MU4]=OFF ;
  32:   ;
  33:  !------------------------------- ;
  34:  !Calcolo Pos. pezzo ;
  35:  !------------------------------- ;
  36:  PR[12:POS_MU]=PR[47:Zero XYZ_NUT]    ;
  37:  PR[12,1:POS_MU]=0    ;
  38:  PR[12,2:POS_MU]=0    ;
  39:  PR[12,3:POS_MU]=0    ;
  40:  PR[12,4:POS_MU]=(-.003)    ;
  41:  PR[12,5:POS_MU]=.002    ;
  42:  PR[12,6:POS_MU]=(-90.01)    ;
  43:  PR[12,7:POS_MU]=9000    ;
  44:   ;
  45:  PR[51:*]=PR[49:Zero XYZ FUT]    ;
  46:  PR[51,1:*]=(-700)    ;
  47:  PR[51,2:*]=(-100)    ;
  48:  PR[51,6:*]=10    ;
  49:   ;
  50:  PR[52:*]=PR[49:Zero XYZ FUT]    ;
  51:  PR[52,1:*]=50    ;
  52:  PR[52,2:*]=100    ;
  53:  PR[52,3:*]=(-110)    ;
  54:  PR[52,6:*]=20    ;
  55:   ;
  56:  PR[53:*]=PR[49:Zero XYZ FUT]    ;
  57:  PR[53,3:*]=(-110)    ;
  58:   ;
  59:  PR[54:*]=PR[49:Zero XYZ FUT]    ;
  60:  PR[54,1:*]=(-1400)    ;
  61:  PR[54,2:*]=(-200)    ;
  62:  PR[54,6:*]=10    ;
  63:  PR[54,7:*]=600    ;
  64:   ;
  65:  PR[55:*]=PR[49:Zero XYZ FUT]    ;
  66:  PR[55,2:*]=100    ;
  67:   ;
  68:  !------------------------------ ;
  69:  COL DETECT ON ;
  70:  COL GUARD ADJUST 120 ;
  71:   ;
  72:L PR[12:POS_MU] 1000mm/sec CNT50 Offset,PR[52:*] Tool_Offset,PR[54:*]    ;
  73:L PR[12:POS_MU] 1000mm/sec CNT20 Offset,PR[52:*] Tool_Offset,PR[51:*]    ;
  74:L PR[12:POS_MU] 600mm/sec CNT1 Offset,PR[52:*]    ;
  75:L PR[12:POS_MU] 300mm/sec CNT1 Offset,PR[53:*]    ;
  76:  !------------------------------ ;
  77:  ! Sblocco il pallet ------------- ;
  78:  CALL CMD_SBL_UT_M4    ;
  79:  !------------------------------ ;
  80:   ;
  81:  ! Punto finale ----------------- ;
  82:L PR[12:POS_MU] 100mm/sec FINE    ;
  83:   ;
  84:  !------------------------------- ;
  85:  !Blocco il pallet ;
  86:  !------------------------------- ;
  87:  !Blocco Uensile ;
  88:  CALL CMD_BL_UT_M4    ;
  89:   ;
  90:  CALL P1_APRI    ;
  91:  !------------------------------- ;
  92:   ;
  93:L PR[12:POS_MU] 600mm/sec FINE Offset,PR[55:*]    ;
  94:   ;
  95:  !------------------------------- ;
  96:  WAIT RI[3:PINZA AP.]=ON    ;
  97:  COL DETECT OFF ;
  98:   ;
  99:  !------------------------------- ;
 100:L PR[12:POS_MU] 1000mm/sec CNT50 Offset,PR[52:*] Tool_Offset,PR[51:*]    ;
 101:L PR[12:POS_MU] 1000mm/sec CNT50 Offset,PR[52:*] Tool_Offset,PR[54:*]    ;
 102:   ;
 103:  WAIT RI[3:PINZA AP.]=ON AND RI[4:PINZA CH]=OFF    ;
 104:L P[3] 1500mm/sec CNT50    ;
 105:   ;
 106:   ;
 107:  !FINE CARICO ------------- ;
 108:  DO[195:RBT Fuor.ing.MU4]=ON ;
 109:  WAIT DO[195:RBT Fuor.ing.MU4]=ON    ;
 110:  WAIT    .50(sec) ;
 111:  DO[201:AP.PT.Autom.MU4]=OFF ;
 112:  DO[202:CH.PT.Autom.MU4]=ON ;
 113:  DO[186:Fin.Cari.PLT MU4]=PULSE,2.0sec ;
 114:  R[6:Strobe]=3    ;
 115:  !------------------------------ ;
 116:   ;
 117:J P[2] 100% CNT50    ;
 118:J P[1] 100% CNT50    ;
 119:   ;
/POS
P[1]{
   GP1:
	UF : 8, UT : 1,	
	J1=     -.001 deg,	J2=   -54.999 deg,	J3=    24.998 deg,
	J4=     0.000 deg,	J5=   -25.000 deg,	J6=   -90.000 deg,
	E1=  5500.000  mm
};
P[2]{
   GP1:
	UF : 8, UT : 1,	
	J1=   -16.288 deg,	J2=   -29.147 deg,	J3=     3.450 deg,
	J4=    76.255 deg,	J5=   -14.249 deg,	J6=  -165.837 deg,
	E1=  5500.000  mm
};
P[3]{
   GP1:
	UF : 8, UT : 1,		CONFIG : 'N U T, 0, 0, 0',
	X =   185.503  mm,	Y =  1748.104  mm,	Z =   856.003  mm,
	W =     0.000 deg,	P =     0.000 deg,	R =   -89.998 deg,
	E1=  5900.000  mm
};
/END
