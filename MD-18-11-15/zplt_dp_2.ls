/PROG  ZPLT_DP_2
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "Depo.MAG_2";
PROG_SIZE	= 1597;
CREATE		= DATE 15-11-17  TIME 12:12:26;
MODIFIED	= DATE 15-11-17  TIME 12:14:22;
FILE_NAME	= ZPLT_DP_;
VERSION		= 0;
LINE_COUNT	= 69;
MEMORY_SIZE	= 2089;
PROTECT		= READ;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= 1,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/APPL

AUTO_SINGULARITY_HEADER;
  ENABLE_SINGULARITY_AVOIDANCE   : TRUE;
/MN
   1:  !------------------------------- ;
   2:  !Avvicinamento ai punti di presa ;
   3:  !------------------------------- ;
   4:  UFRAME_NUM=2 ;
   5:  UTOOL_NUM=1 ;
   6:   ;
   7:  LBL[10] ;
   8:   ;
   9:  CALL PLT2_XYZ    ;
  10:   ;
  11:  !------------------------------- ;
  12:  !DEPOSITO PLT ;
  13:  !------------------------------- ;
  14:  R[20:Valore OFFSET X]=314.5*R[19:N'COLLONNE]    ;
  15:  R[21:Valore OFFSET Y]=320*R[18:N'FILE]    ;
  16:  PR[1:Prel.Dep MAGAZ.]=PR[49:Zero XYZ FUT]    ;
  17:  PR[1,1:Prel.Dep MAGAZ.]=R[20:Valore OFFSET X]+R[23:CORRET.IN.X]-R[30:OFFSET_X]    ;
  18:  PR[1,2:Prel.Dep MAGAZ.]=R[21:Valore OFFSET Y]+R[24:CORRET.IN.Y]-R[32:OFFSET_Z]    ;
  19:  PR[1,3:Prel.Dep MAGAZ.]=R[22:CORRET.IN.Z]+R[31:OFFSET_Y]    ;
  20:  PR[1,4:Prel.Dep MAGAZ.]=(-159.383)    ;
  21:  PR[1,5:Prel.Dep MAGAZ.]=89.927    ;
  22:  PR[1,6:Prel.Dep MAGAZ.]=(-68.801)    ;
  23:  PR[1,7:Prel.Dep MAGAZ.]=4000+R[20:Valore OFFSET X]+R[23:CORRET.IN.X]    ;
  24:   ;
  25:  PR[50:*]=PR[49:Zero XYZ FUT]    ;
  26:  PR[50,1:*]=0    ;
  27:  PR[50,2:*]=5    ;
  28:  PR[50,3:*]=600    ;
  29:   ;
  30:  PR[51:*]=PR[49:Zero XYZ FUT]    ;
  31:  PR[51,2:*]=0    ;
  32:  PR[51,3:*]=5    ;
  33:   ;
  34:  PR[52:*]=PR[49:Zero XYZ FUT]    ;
  35:  PR[52,2:*]=10    ;
  36:   ;
  37:  PR[53:*]=PR[49:Zero XYZ FUT]    ;
  38:  PR[53,2:*]=10    ;
  39:  PR[53,3:*]=700    ;
  40:   ;
  41:  PR[54:*]=PR[49:Zero XYZ FUT]    ;
  42:  PR[54,1:*]=0    ;
  43:  PR[54,2:*]=(-345)    ;
  44:  PR[54,3:*]=600    ;
  45:   ;
  46:  !------------------------------- ;
  47:L PR[1:Prel.Dep MAGAZ.] 1000mm/sec CNT2 Offset,PR[53:*]    ;
  48:  !------------------------------ ;
  49:  COL DETECT ON ;
  50:  COL GUARD ADJUST 150 ;
  51:L PR[1:Prel.Dep MAGAZ.] 800mm/sec CNT2 Offset,PR[52:*]    ;
  52:  !------------------------------- ;
  53:  !PUNTO FINALE ;
  54:L PR[1:Prel.Dep MAGAZ.] 100mm/sec FINE    ;
  55:  !------------------------------- ;
  56:  !Apre PINZA ;
  57:  PAUSE ;
  58:  COL DETECT OFF ;
  59:  !------------------------------ ;
  60:  COL DETECT OFF ;
  61:   ;
  62:L PR[1:Prel.Dep MAGAZ.] 200mm/sec FINE Offset,PR[52:*]    ;
  63:   ;
  64:   ;
  65:L PR[1:Prel.Dep MAGAZ.] 1000mm/sec CNT20 Offset,PR[53:*]    ;
  66:  !------------------------------- ;
  67:  R[2:N�Baia]=R[2:N�Baia]+1    ;
  68:  JMP LBL[10] ;
  69:   ;
/POS
/END
