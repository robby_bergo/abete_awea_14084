/PROG  M1_AWEA850_CAR
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "CAR AWEA850 1";
PROG_SIZE	= 2738;
CREATE		= DATE 12-04-17  TIME 00:03:20;
MODIFIED	= DATE 15-10-28  TIME 20:10:50;
FILE_NAME	= ;
VERSION		= 0;
LINE_COUNT	= 111;
MEMORY_SIZE	= 3222;
PROTECT		= READ;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= 1,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/MN
   1:  !------------------------------- ;
   2:  !CARICO la Macch M1_AWEA850 ;
   3:  !------------------------------- ;
   4:  UFRAME_NUM=8 ;
   5:  UTOOL_NUM=1 ;
   6:   ;
   7:  CALL USER_8_MU1    ;
   8:   ;
   9:  LBL[31] ;
  10:  !Controllo PLT su pinza ;
  11:  IF DI[59:Pres.Pz.UT. Mag]=ON AND DI[58:Pres.Pz.PLT Mag]=OFF,JMP LBL[30] ;
  12:  CALL MESSAGGI(24) ;
  13:  PAUSE ;
  14:  R[9:Message Allarmi]=0    ;
  15:  CALL MESSAGGI(1) ;
  16:  JMP LBL[31] ;
  17:  LBL[30] ;
  18:   ;
  19:   ;
  20:  !------------------------------ ;
  21:J P[1] 80% CNT50    ;
  22:J P[2] 80% CNT50    ;
  23:J P[3] 75% CNT50    ;
  24:L P[4] 1500mm/sec CNT50    ;
  25:   ;
  26:  DO[106:CH.PT.Autom.MU1]=OFF ;
  27:  DO[105:AP.PT.Autom.MU1]=ON ;
  28:  ! Verifico stato CNC ;
  29:  WAIT DI[90:Ch.Cari.PLT MU1]=ON AND DI[108:Porta Auto.AP.MU1]=ON    ;
  30:  DO[99:RBT Fuor.ing.MU1]=OFF ;
  31:   ;
  32:  !CNT Sblocco Del pallet --------- ;
  33:  IF DI[109:PLT Sblocc.MU1]=OFF,CALL CMD_SBL_PLT_M1 ;
  34:   ;
  35:  !------------------------------- ;
  36:  !Calcolo Pos. pezzo ;
  37:  !------------------------------- ;
  38:  PR[12:POS_MU]=PR[47:Zero XYZ_NUT]    ;
  39:  PR[12,1:POS_MU]=R[30:OFFSET_X]+0    ;
  40:  PR[12,2:POS_MU]=R[31:OFFSET_Y]+0    ;
  41:  PR[12,3:POS_MU]=0-R[32:OFFSET_Z]    ;
  42:  PR[12,4:POS_MU]=0    ;
  43:  PR[12,5:POS_MU]=0    ;
  44:  PR[12,6:POS_MU]=(-90.005)    ;
  45:  PR[12,7:POS_MU]=1000    ;
  46:   ;
  47:  PR[51:*]=PR[49:Zero XYZ FUT]    ;
  48:  PR[51,1:*]=0    ;
  49:  PR[51,2:*]=700    ;
  50:  PR[51,3:*]=200    ;
  51:   ;
  52:  PR[52:*]=PR[49:Zero XYZ FUT]    ;
  53:  PR[52,1:*]=0    ;
  54:  PR[52,2:*]=0    ;
  55:  PR[52,3:*]=150    ;
  56:   ;
  57:  PR[53:*]=PR[49:Zero XYZ FUT]    ;
  58:  PR[53,3:*]=20    ;
  59:   ;
  60:  PR[54:*]=PR[49:Zero XYZ FUT]    ;
  61:  PR[54,1:*]=0    ;
  62:  PR[54,2:*]=1400    ;
  63:  PR[54,3:*]=300    ;
  64:  PR[54,7:*]=600    ;
  65:   ;
  66:  ! Sblocco il pallet ------------- ;
  67:  CALL CMD_SBL_PLT_M1    ;
  68:  !------------------------------ ;
  69:  COL DETECT ON ;
  70:  COL GUARD ADJUST 120 ;
  71:   ;
  72:L PR[12:POS_MU] 1000mm/sec CNT50 Offset,PR[54:*]    ;
  73:L PR[12:POS_MU] 1000mm/sec CNT20 Offset,PR[51:*]    ;
  74:L PR[12:POS_MU] 600mm/sec CNT1 Offset,PR[52:*]    ;
  75:   ;
  76:  ! Punto finale ----------------- ;
  77:L PR[12:POS_MU] 100mm/sec FINE    ;
  78:   ;
  79:  CALL P1_APRI    ;
  80:  !------------------------------- ;
  81:   ;
  82:L PR[12:POS_MU] 600mm/sec FINE Offset,PR[53:*]    ;
  83:   ;
  84:  !------------------------------- ;
  85:  !Blocco il pallet ;
  86:  WAIT RI[3:PINZA AP.]=ON    ;
  87:  CALL CMD_BL_PLT_M1    ;
  88:  COL DETECT OFF ;
  89:   ;
  90:  !------------------------------- ;
  91:L PR[12:POS_MU] 600mm/sec CNT10 Offset,PR[52:*]    ;
  92:L PR[12:POS_MU] 1000mm/sec CNT50 Offset,PR[51:*]    ;
  93:L PR[12:POS_MU] 1000mm/sec CNT50 Offset,PR[54:*]    ;
  94:   ;
  95:  WAIT RI[3:PINZA AP.]=ON AND RI[4:PINZA CH]=OFF    ;
  96:L P[4] 1500mm/sec CNT50    ;
  97:J P[3] 75% CNT50    ;
  98:   ;
  99:  !FINE CARICO ------------- ;
 100:  DO[99:RBT Fuor.ing.MU1]=ON ;
 101:  WAIT DO[99:RBT Fuor.ing.MU1]=ON    ;
 102:  WAIT    .50(sec) ;
 103:  DO[105:AP.PT.Autom.MU1]=OFF ;
 104:  DO[106:CH.PT.Autom.MU1]=ON ;
 105:  DO[90:Fin.Cari.PLT MU1]=PULSE,2.0sec ;
 106:  R[6:Strobe]=3    ;
 107:  !------------------------------ ;
 108:   ;
 109:J P[2] 100% CNT50    ;
 110:J P[1] 100% CNT50    ;
 111:   ;
/POS
P[1]{
   GP1:
	UF : 8, UT : 1,	
	J1=     -.001 deg,	J2=   -54.999 deg,	J3=    24.998 deg,
	J4=     0.000 deg,	J5=   -25.000 deg,	J6=   -90.000 deg,
	E1=   500.000  mm
};
P[2]{
   GP1:
	UF : 8, UT : 1,	
	J1=   -16.288 deg,	J2=   -29.147 deg,	J3=     3.450 deg,
	J4=    76.255 deg,	J5=   -14.249 deg,	J6=  -165.837 deg,
	E1=   500.000  mm
};
P[3]{
   GP1:
	UF : 8, UT : 1,	
	J1=   -88.508 deg,	J2=   -57.683 deg,	J3=    26.679 deg,
	J4=    -2.897 deg,	J5=   -24.521 deg,	J6=   -88.128 deg,
	E1=  1500.000  mm
};
P[4]{
   GP1:
	UF : 8, UT : 1,	
	J1=  -137.231 deg,	J2=   -48.959 deg,	J3=    16.334 deg,
	J4=   -59.469 deg,	J5=   -28.660 deg,	J6=   -34.306 deg,
	E1=  2000.000  mm
};
/END
