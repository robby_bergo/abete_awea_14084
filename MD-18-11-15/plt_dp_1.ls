/PROG  PLT_DP_1
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "Depo.MAG_1";
PROG_SIZE	= 2281;
CREATE		= DATE 12-03-30  TIME 22:25:42;
MODIFIED	= DATE 15-10-16  TIME 10:09:08;
FILE_NAME	= PLT_PR_1;
VERSION		= 0;
LINE_COUNT	= 88;
MEMORY_SIZE	= 2721;
PROTECT		= READ;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= 1,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/APPL

AUTO_SINGULARITY_HEADER;
  ENABLE_SINGULARITY_AVOIDANCE   : TRUE;
/MN
   1:  !------------------------------- ;
   2:  !Avvicinamento ai punti di presa ;
   3:  !------------------------------- ;
   4:  UFRAME_NUM=1 ;
   5:  UTOOL_NUM=2 ;
   6:   ;
   7:  LBL[20] ;
   8:  !Controllo PLT su pinza ;
   9:  IF DI[58:Pres.Pz.PLT Mag]=ON AND DI[59:Pres.Pz.UT. Mag]=OFF,JMP LBL[10] ;
  10:  CALL MESSAGGI(24) ;
  11:  PAUSE ;
  12:  R[9:Message Allarmi]=0    ;
  13:  CALL MESSAGGI(1) ;
  14:  JMP LBL[20] ;
  15:  LBL[10] ;
  16:   ;
  17:  CALL PLT1_XYZ    ;
  18:   ;
  19:J P[1] 80% CNT50    ;
  20:J P[2] 80% CNT50    ;
  21:   ;
  22:  !------------------------------- ;
  23:  !DEPOSITO PLT ;
  24:  !------------------------------- ;
  25:  R[20:Valore OFFSET X]=100*R[19:N'COLLONNE]    ;
  26:  R[21:Valore OFFSET Y]=400.5*R[18:N'FILE]    ;
  27:  PR[1:Prel.Dep MAGAZ.]=PR[49:Zero XYZ FUT]    ;
  28:  PR[1,1:Prel.Dep MAGAZ.]=R[20:Valore OFFSET X]+R[23:CORRET.IN.X]    ;
  29:  PR[1,2:Prel.Dep MAGAZ.]=R[21:Valore OFFSET Y]+R[24:CORRET.IN.Y]    ;
  30:  PR[1,3:Prel.Dep MAGAZ.]=R[22:CORRET.IN.Z]    ;
  31:  PR[1,4:Prel.Dep MAGAZ.]=30.26    ;
  32:  PR[1,5:Prel.Dep MAGAZ.]=89.454    ;
  33:  PR[1,6:Prel.Dep MAGAZ.]=120.467    ;
  34:  PR[1,7:Prel.Dep MAGAZ.]=2000+R[20:Valore OFFSET X]+R[23:CORRET.IN.X]    ;
  35:   ;
  36:  PR[50:*]=PR[49:Zero XYZ FUT]    ;
  37:  PR[50,1:*]=0    ;
  38:  PR[50,2:*]=5    ;
  39:  PR[50,3:*]=300    ;
  40:   ;
  41:  PR[51:*]=PR[49:Zero XYZ FUT]    ;
  42:  PR[51,2:*]=0    ;
  43:  PR[51,3:*]=60    ;
  44:   ;
  45:  PR[52:*]=PR[49:Zero XYZ FUT]    ;
  46:  PR[52,3:*]=20    ;
  47:   ;
  48:  PR[54:*]=PR[49:Zero XYZ FUT]    ;
  49:  PR[54,1:*]=0    ;
  50:  PR[54,2:*]=300    ;
  51:  PR[54,3:*]=500    ;
  52:   ;
  53:  !------------------------------- ;
  54:L PR[1:Prel.Dep MAGAZ.] 1000mm/sec FINE Offset,PR[54:*]    ;
  55:  CALL CNT_PRES_CONO    ;
  56:   ;
  57:  IF R[130:CONO PRES.IN POS]=1,JMP LBL[1] ;
  58:  !------------------------------- ;
  59:L PR[1:Prel.Dep MAGAZ.] 1000mm/sec CNT2 Offset,PR[50:*] ACC40    ;
  60:  !------------------------------ ;
  61:  COL DETECT ON ;
  62:  COL GUARD ADJUST 150 ;
  63:L PR[1:Prel.Dep MAGAZ.] 1000mm/sec CNT2 Offset,PR[51:*]    ;
  64:  !------------------------------- ;
  65:  !PUNTO FINALE ;
  66:L PR[1:Prel.Dep MAGAZ.] 100mm/sec FINE    ;
  67:  !------------------------------- ;
  68:  !Apre PINZA ;
  69:  COL DETECT OFF ;
  70:  CALL P1_APRI    ;
  71:  !------------------------------ ;
  72:  COL DETECT OFF ;
  73:   ;
  74:L PR[1:Prel.Dep MAGAZ.] 200mm/sec FINE Offset,PR[52:*]    ;
  75:   ;
  76:  WAIT RI[3:PINZA AP.]=ON    ;
  77:   ;
  78:L PR[1:Prel.Dep MAGAZ.] 1000mm/sec CNT20 Offset,PR[50:*]    ;
  79:  WAIT RI[3:PINZA AP.]=ON AND RI[4:PINZA CH]=OFF    ;
  80:   ;
  81:  LBL[1] ;
  82:  !------------------------------- ;
  83:L P[2] 1500mm/sec CNT50    ;
  84:J P[1] 100% CNT100    ;
  85:   ;
  86:   ;
  87:   ;
  88:   ;
/POS
P[1]{
   GP1:
	UF : 1, UT : 2,	
	J1=     -.000 deg,	J2=   -55.000 deg,	J3=    25.000 deg,
	J4=      .000 deg,	J5=   -25.000 deg,	J6=    90.000 deg,
	E1=  2000.000  mm
};
P[2]{
   GP1:
	UF : 1, UT : 2,	
	J1=    -5.738 deg,	J2=   -35.890 deg,	J3=    -3.081 deg,
	J4=    90.027 deg,	J5=    96.091 deg,	J6=     3.249 deg,
	E1=  2000.000  mm
};
/END
