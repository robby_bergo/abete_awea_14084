/PROG  M2_AWEA850_CAR
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "CAR AWEA850 2";
PROG_SIZE	= 2667;
CREATE		= DATE 12-04-17  TIME 00:03:20;
MODIFIED	= DATE 15-10-28  TIME 20:42:06;
FILE_NAME	= ;
VERSION		= 0;
LINE_COUNT	= 109;
MEMORY_SIZE	= 3159;
PROTECT		= READ;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= 1,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/MN
   1:  !------------------------------- ;
   2:  !CARICO la Macch M2_AWEA850 ;
   3:  !------------------------------- ;
   4:  UFRAME_NUM=8 ;
   5:  UTOOL_NUM=1 ;
   6:   ;
   7:  CALL USER_8_MU2    ;
   8:   ;
   9:  LBL[31] ;
  10:  !Controllo PLT su pinza ;
  11:  IF DI[59:Pres.Pz.UT. Mag]=ON AND DI[58:Pres.Pz.PLT Mag]=OFF,JMP LBL[30] ;
  12:  CALL MESSAGGI(24) ;
  13:  PAUSE ;
  14:  R[9:Message Allarmi]=0    ;
  15:  CALL MESSAGGI(1) ;
  16:  JMP LBL[31] ;
  17:  LBL[30] ;
  18:   ;
  19:   ;
  20:  !CNT Sblocco Del pallet --------- ;
  21:  IF DI[141:PLT Sblocc.MU2]=OFF,CALL CMD_SBL_PLT_M2 ;
  22:   ;
  23:  !------------------------------ ;
  24:J P[1] 80% CNT50    ;
  25:J P[2] 80% CNT50    ;
  26:L P[3] 1500mm/sec CNT50    ;
  27:   ;
  28:  DO[138:CH.PT.Autom.MU2]=OFF ;
  29:  DO[137:AP.PT.Autom.MU2]=ON ;
  30:  ! Verifico stato CNC ;
  31:  WAIT DI[122:Ch.Cari.PLT MU2]=ON AND DI[140:Porta Auto.AP.MU2]=ON    ;
  32:  DO[131:RBT Fuor.ing.MU2]=OFF ;
  33:   ;
  34:  !------------------------------- ;
  35:  !Calcolo Pos. pezzo ;
  36:  !------------------------------- ;
  37:  PR[12:POS_MU]=PR[47:Zero XYZ_NUT]    ;
  38:  PR[12,1:POS_MU]=R[30:OFFSET_X]+0    ;
  39:  PR[12,2:POS_MU]=R[31:OFFSET_Y]+0    ;
  40:  PR[12,3:POS_MU]=0-R[32:OFFSET_Z]    ;
  41:  PR[12,4:POS_MU]=0    ;
  42:  PR[12,5:POS_MU]=0    ;
  43:  PR[12,6:POS_MU]=(-89.996)    ;
  44:  PR[12,7:POS_MU]=6500    ;
  45:   ;
  46:  PR[51:*]=PR[49:Zero XYZ FUT]    ;
  47:  PR[51,1:*]=0    ;
  48:  PR[51,2:*]=700    ;
  49:  PR[51,3:*]=200    ;
  50:   ;
  51:  PR[52:*]=PR[49:Zero XYZ FUT]    ;
  52:  PR[52,1:*]=0    ;
  53:  PR[52,2:*]=0    ;
  54:  PR[52,3:*]=150    ;
  55:   ;
  56:  PR[53:*]=PR[49:Zero XYZ FUT]    ;
  57:  PR[53,3:*]=20    ;
  58:   ;
  59:  PR[54:*]=PR[49:Zero XYZ FUT]    ;
  60:  PR[54,1:*]=0    ;
  61:  PR[54,2:*]=1400    ;
  62:  PR[54,3:*]=300    ;
  63:  PR[54,7:*]=(-600)    ;
  64:   ;
  65:  ! Sblocco il pallet ------------- ;
  66:  CALL CMD_SBL_PLT_M2    ;
  67:  !------------------------------ ;
  68:  COL DETECT ON ;
  69:  COL GUARD ADJUST 120 ;
  70:   ;
  71:L PR[12:POS_MU] 1000mm/sec CNT50 Offset,PR[54:*]    ;
  72:L PR[12:POS_MU] 1000mm/sec CNT20 Offset,PR[51:*]    ;
  73:L PR[12:POS_MU] 600mm/sec CNT1 Offset,PR[52:*]    ;
  74:   ;
  75:  ! Punto finale ----------------- ;
  76:L PR[12:POS_MU] 100mm/sec FINE    ;
  77:   ;
  78:  CALL P1_APRI    ;
  79:  !------------------------------- ;
  80:   ;
  81:L PR[12:POS_MU] 600mm/sec FINE Offset,PR[53:*]    ;
  82:   ;
  83:  !------------------------------- ;
  84:  !Blocco il pallet ;
  85:  WAIT RI[3:PINZA AP.]=ON    ;
  86:  CALL CMD_BL_PLT_M2    ;
  87:  COL DETECT OFF ;
  88:   ;
  89:  !------------------------------- ;
  90:L PR[12:POS_MU] 600mm/sec CNT10 Offset,PR[52:*]    ;
  91:L PR[12:POS_MU] 1000mm/sec CNT50 Offset,PR[51:*]    ;
  92:L PR[12:POS_MU] 1000mm/sec CNT50 Offset,PR[54:*]    ;
  93:   ;
  94:  WAIT RI[3:PINZA AP.]=ON AND RI[4:PINZA CH]=OFF    ;
  95:L P[3] 1500mm/sec CNT50    ;
  96:   ;
  97:  !FINE CARICO ------------- ;
  98:  DO[131:RBT Fuor.ing.MU2]=ON ;
  99:  WAIT DO[131:RBT Fuor.ing.MU2]=ON    ;
 100:  WAIT    .50(sec) ;
 101:  DO[137:AP.PT.Autom.MU2]=OFF ;
 102:  DO[138:CH.PT.Autom.MU2]=ON ;
 103:  DO[122:Fin.Cari.PLT MU2]=PULSE,2.0sec ;
 104:  R[6:Strobe]=3    ;
 105:  !------------------------------ ;
 106:   ;
 107:J P[2] 100% CNT50    ;
 108:J P[1] 100% CNT50    ;
 109:   ;
/POS
P[1]{
   GP1:
	UF : 8, UT : 1,	
	J1=     -.001 deg,	J2=   -54.999 deg,	J3=    24.998 deg,
	J4=     0.000 deg,	J5=   -25.000 deg,	J6=   -90.000 deg,
	E1=  5500.000  mm
};
P[2]{
   GP1:
	UF : 8, UT : 1,	
	J1=   -16.288 deg,	J2=   -29.147 deg,	J3=     3.450 deg,
	J4=    76.255 deg,	J5=   -14.249 deg,	J6=  -165.837 deg,
	E1=  5500.000  mm
};
P[3]{
   GP1:
	UF : 8, UT : 1,		CONFIG : 'N U T, 0, 0, 0',
	X =   185.503  mm,	Y =  1748.104  mm,	Z =   856.003  mm,
	W =     0.000 deg,	P =     0.000 deg,	R =   -89.998 deg,
	E1=  5900.000  mm
};
/END
