/PROG  CBM_PINZE
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "CBM PINZE";
PROG_SIZE	= 1367;
CREATE		= DATE 13-06-06  TIME 01:00:46;
MODIFIED	= DATE 15-10-29  TIME 12:20:18;
FILE_NAME	= ;
VERSION		= 0;
LINE_COUNT	= 60;
MEMORY_SIZE	= 1791;
PROTECT		= READ;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= 1,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/APPL

AUTO_SINGULARITY_HEADER;
  ENABLE_SINGULARITY_AVOIDANCE   : FALSE;
/MN
   1:   ;
   2:  !------------------------------- ;
   3:  !PRELIEVO PINZE ;
   4:  !------------------------------- ;
   5:   ;
   6:  SELECT R[12:N_PINZA A BORDO]=3,JMP LBL[2] ;
   7:         =1,JMP LBL[3] ;
   8:         =2,JMP LBL[3] ;
   9:  JMP LBL[2] ;
  10:   ;
  11:  LBL[1000] ;
  12:  IF RI[1:CBM.ACC]=ON,JMP LBL[1] ;
  13:  !------------------------------- ;
  14:  SELECT R[5:Tipo di pinza]=0,JMP LBL[1] ;
  15:         =1,JMP LBL[10] ;
  16:         =2,JMP LBL[20] ;
  17:  JMP LBL[1] ;
  18:   ;
  19:  !------------------------------- ;
  20:  LBL[10] ;
  21:  CALL CBM_P1_PR    ;
  22:  JMP LBL[1] ;
  23:   ;
  24:  !------------------------------- ;
  25:  LBL[20] ;
  26:  CALL CBM_P2_PR    ;
  27:  JMP LBL[1] ;
  28:  !------------------------------- ;
  29:   ;
  30:   ;
  31:  LBL[1] ;
  32:  END ;
  33:   ;
  34:  LBL[3] ;
  35:  !------------------------------- ;
  36:  !DEPOSITO PINZE ;
  37:  !------------------------------- ;
  38:  IF R[5:Tipo di pinza]=R[12:N_PINZA A BORDO],JMP LBL[2] ;
  39:  !------------------------------- ;
  40:  SELECT R[12:N_PINZA A BORDO]=3,JMP LBL[2] ;
  41:         =1,JMP LBL[100] ;
  42:         =2,JMP LBL[200] ;
  43:  JMP LBL[2] ;
  44:   ;
  45:  !------------------------------- ;
  46:  LBL[100] ;
  47:  IF R[12:N_PINZA A BORDO]=1,CALL CBM_P1_DP ;
  48:  JMP LBL[2] ;
  49:   ;
  50:  !------------------------------- ;
  51:  LBL[200] ;
  52:  IF R[12:N_PINZA A BORDO]=2,CALL CBM_P2_DP ;
  53:  JMP LBL[2] ;
  54:   ;
  55:  !------------------------------- ;
  56:  LBL[2] ;
  57:  IF R[12:N_PINZA A BORDO]=3 AND R[5:Tipo di pinza]<>0,JMP LBL[1000] ;
  58:   ;
  59:   ;
  60:   ;
/POS
/END
