/PROG  ZBANC_1OT_DP
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "BANC_1IN_DP";
PROG_SIZE	= 1979;
CREATE		= DATE 15-10-15  TIME 15:16:26;
MODIFIED	= DATE 15-10-15  TIME 15:16:26;
FILE_NAME	= BANC_1OT;
VERSION		= 0;
LINE_COUNT	= 69;
MEMORY_SIZE	= 2339;
PROTECT		= READ_WRITE;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= 1,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/APPL

AUTO_SINGULARITY_HEADER;
  ENABLE_SINGULARITY_AVOIDANCE   : TRUE;
/MN
   1:  !------------------------------- ;
   2:  !DEPOSITO BANCO 1 IN INGRESSO ;
   3:  !------------------------------- ;
   4:  UFRAME_NUM=7 ;
   5:  UTOOL_NUM=1 ;
   6:   ;
   7:J P[1] 80% CNT50    ;
   8:J P[2] 80% CNT50    ;
   9:J P[3] 80% CNT50    ;
  10:   ;
  11:L P[4] 500mm/sec FINE    ;
  12:   ;
  13:  !------------------------------- ;
  14:  !CALCOLO POSIZIONE ;
  15:  !------------------------------- ;
  16:  PR[10:Prel.Dep BANCO]=PR[49:Zero XYZ FUT]    ;
  17:  PR[10,1:Prel.Dep BANCO]=0    ;
  18:  PR[10,2:Prel.Dep BANCO]=0    ;
  19:  PR[10,3:Prel.Dep BANCO]=0    ;
  20:  PR[10,4:Prel.Dep BANCO]=(-159.383)    ;
  21:  PR[10,5:Prel.Dep BANCO]=89.927    ;
  22:  PR[10,6:Prel.Dep BANCO]=(-68.801)    ;
  23:  PR[10,7:Prel.Dep BANCO]=4000    ;
  24:   ;
  25:  PR[50:*]=PR[49:Zero XYZ FUT]    ;
  26:  PR[50,1:*]=0    ;
  27:  PR[50,2:*]=5    ;
  28:  PR[50,3:*]=600    ;
  29:   ;
  30:  PR[51:*]=PR[49:Zero XYZ FUT]    ;
  31:  PR[51,2:*]=0    ;
  32:  PR[51,3:*]=300    ;
  33:   ;
  34:  PR[52:*]=PR[49:Zero XYZ FUT]    ;
  35:  PR[52,2:*]=10    ;
  36:   ;
  37:  PR[53:*]=PR[49:Zero XYZ FUT]    ;
  38:  PR[53,2:*]=10    ;
  39:  PR[53,3:*]=700    ;
  40:   ;
  41:  !------------------------------- ;
  42:L PR[10:Prel.Dep BANCO] 1000mm/sec CNT2 Offset,PR[53:*]    ;
  43:  !------------------------------ ;
  44:  COL DETECT ON ;
  45:  COL GUARD ADJUST 150 ;
  46:L PR[10:Prel.Dep BANCO] 600mm/sec CNT2 Offset,PR[52:*]    ;
  47:  !------------------------------- ;
  48:  !PUNTO FINALE ;
  49:L PR[10:Prel.Dep BANCO] 200mm/sec FINE    ;
  50:  COL DETECT OFF ;
  51:  !------------------------------- ;
  52:  !Apre PINZA ;
  53:  COL DETECT OFF ;
  54:  CALL P1_APRI    ;
  55:  !------------------------------ ;
  56:  COL DETECT OFF ;
  57:   ;
  58:L PR[10:Prel.Dep BANCO] 200mm/sec FINE Offset,PR[51:*]    ;
  59:   ;
  60:  WAIT RI[3:PINZA AP.]=ON    ;
  61:   ;
  62:L PR[10:Prel.Dep BANCO] 1000mm/sec CNT20 Offset,PR[50:*]    ;
  63:  !------------------------------- ;
  64:   ;
  65:L P[3] 1500mm/sec CNT50    ;
  66:  WAIT RI[3:PINZA AP.]=ON AND RI[4:PINZA CH]=OFF    ;
  67:J P[2] 100% CNT100    ;
  68:J P[1] 100% CNT100    ;
  69:   ;
/POS
P[1]{
   GP1:
	UF : 7, UT : 1,	
	J1=     -.001 deg,	J2=   -54.999 deg,	J3=    24.998 deg,
	J4=      .000 deg,	J5=   -25.000 deg,	J6=   -90.000 deg,
	E1=   100.000  mm
};
P[2]{
   GP1:
	UF : 7, UT : 1,	
	J1=   -13.773 deg,	J2=   -31.412 deg,	J3=     3.647 deg,
	J4=   -74.918 deg,	J5=   -13.714 deg,	J6=   -15.752 deg,
	E1=   100.000  mm
};
P[3]{
   GP1:
	UF : 7, UT : 1,	
	J1=   -44.705 deg,	J2=   -40.470 deg,	J3=     8.513 deg,
	J4=   -81.396 deg,	J5=   -45.043 deg,	J6=   -12.085 deg,
	E1=   100.000  mm
};
P[4]{
   GP1:
	UF : 7, UT : 1,	
	J1=  -127.061 deg,	J2=     9.220 deg,	J3=   -31.181 deg,
	J4=   -68.878 deg,	J5=  -120.647 deg,	J6=    37.041 deg,
	E1=   100.000  mm
};
/END
