/PROG  ZPLT_DP_5
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "Depo.MAG_1";
PROG_SIZE	= 1573;
CREATE		= DATE 15-11-17  TIME 16:39:00;
MODIFIED	= DATE 15-11-17  TIME 16:45:44;
FILE_NAME	= ZPLT_DP_;
VERSION		= 0;
LINE_COUNT	= 68;
MEMORY_SIZE	= 2069;
PROTECT		= READ;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= 1,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/APPL

AUTO_SINGULARITY_HEADER;
  ENABLE_SINGULARITY_AVOIDANCE   : TRUE;
/MN
   1:  !------------------------------- ;
   2:  !Avvicinamento ai punti di presa ;
   3:  !------------------------------- ;
   4:  UFRAME_NUM=5 ;
   5:  UTOOL_NUM=2 ;
   6:   ;
   7:  LBL[10] ;
   8:   ;
   9:  CALL PLT5_XYZ    ;
  10:   ;
  11:  !------------------------------- ;
  12:  !Prelievo plt ;
  13:  !------------------------------- ;
  14:  R[20:Valore OFFSET X]=100*R[19:N'COLLONNE]    ;
  15:  R[21:Valore OFFSET Y]=400.5*R[18:N'FILE]    ;
  16:  PR[1:Prel.Dep MAGAZ.]=PR[49:Zero XYZ FUT]    ;
  17:  PR[1,1:Prel.Dep MAGAZ.]=R[20:Valore OFFSET X]+R[23:CORRET.IN.X]    ;
  18:  PR[1,2:Prel.Dep MAGAZ.]=R[21:Valore OFFSET Y]+R[24:CORRET.IN.Y]    ;
  19:  PR[1,3:Prel.Dep MAGAZ.]=R[22:CORRET.IN.Z]    ;
  20:  PR[1,4:Prel.Dep MAGAZ.]=128.991    ;
  21:  PR[1,5:Prel.Dep MAGAZ.]=89.493    ;
  22:  PR[1,6:Prel.Dep MAGAZ.]=(-140.832)    ;
  23:  PR[1,7:Prel.Dep MAGAZ.]=14200+R[20:Valore OFFSET X]+R[23:CORRET.IN.X]    ;
  24:   ;
  25:  PR[50:*]=PR[49:Zero XYZ FUT]    ;
  26:  PR[50,1:*]=0    ;
  27:  PR[50,2:*]=5    ;
  28:  PR[50,3:*]=300    ;
  29:   ;
  30:  PR[51:*]=PR[49:Zero XYZ FUT]    ;
  31:  PR[51,2:*]=0    ;
  32:  PR[51,3:*]=60    ;
  33:   ;
  34:  PR[52:*]=PR[49:Zero XYZ FUT]    ;
  35:  PR[52,3:*]=20    ;
  36:   ;
  37:  PR[54:*]=PR[49:Zero XYZ FUT]    ;
  38:  PR[54,1:*]=0    ;
  39:  PR[54,2:*]=300    ;
  40:  PR[54,3:*]=500    ;
  41:   ;
  42:  !------------------------------- ;
  43:  !------------------------------- ;
  44:L PR[1:Prel.Dep MAGAZ.] 1000mm/sec CNT2 Offset,PR[50:*] ACC40    ;
  45:  !------------------------------ ;
  46:  COL DETECT ON ;
  47:  COL GUARD ADJUST 150 ;
  48:L PR[1:Prel.Dep MAGAZ.] 1000mm/sec CNT2 Offset,PR[51:*]    ;
  49:  !------------------------------- ;
  50:  !PUNTO FINALE ;
  51:L PR[1:Prel.Dep MAGAZ.] 100mm/sec FINE    ;
  52:  !------------------------------- ;
  53:  !Apre PINZA ;
  54:  COL DETECT OFF ;
  55:  PAUSE ;
  56:  !------------------------------ ;
  57:  COL DETECT OFF ;
  58:   ;
  59:L PR[1:Prel.Dep MAGAZ.] 200mm/sec FINE Offset,PR[51:*]    ;
  60:   ;
  61:L PR[1:Prel.Dep MAGAZ.] 1000mm/sec CNT20 Offset,PR[50:*]    ;
  62:   ;
  63:  R[2:N�Baia]=R[2:N�Baia]+1    ;
  64:  JMP LBL[10] ;
  65:  !------------------------------- ;
  66:   ;
  67:   ;
  68:   ;
/POS
/END
