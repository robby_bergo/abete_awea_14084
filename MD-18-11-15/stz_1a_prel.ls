/PROG  STZ_1A_PREL
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "Prel.Staz.1A ";
PROG_SIZE	= 2438;
CREATE		= DATE 13-07-15  TIME 19:33:54;
MODIFIED	= DATE 15-10-15  TIME 18:43:30;
FILE_NAME	= STZ_2_P3;
VERSION		= 0;
LINE_COUNT	= 93;
MEMORY_SIZE	= 2882;
PROTECT		= READ;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= 1,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/APPL
/MN
   1:  !------------------------------- ;
   2:  !Avvicinamento ai punti di presa ;
   3:  !------------------------------- ;
   4:  UFRAME_NUM=0 ;
   5:  UTOOL_NUM=2 ;
   6:   ;
   7:   ;
   8:  LBL[31] ;
   9:  !Controllo PLT su pinza ;
  10:  IF DI[58:Pres.Pz.PLT Mag]=ON AND DI[59:Pres.Pz.UT. Mag]=OFF,JMP LBL[30] ;
  11:  CALL MESSAGGI(24) ;
  12:  PAUSE ;
  13:  R[9:Message Allarmi]=0    ;
  14:  CALL MESSAGGI(1) ;
  15:  JMP LBL[31] ;
  16:  LBL[30] ;
  17:   ;
  18:  LBL[20] ;
  19:  !Controllo pinza AP. ;
  20:  IF RI[3:PINZA AP.]=ON AND RI[4:PINZA CH]=OFF,JMP LBL[10] ;
  21:  CALL MESSAGGI(16) ;
  22:  PAUSE ;
  23:  R[9:Message Allarmi]=0    ;
  24:  CALL MESSAGGI(1) ;
  25:  JMP LBL[20] ;
  26:  LBL[10] ;
  27:   ;
  28:  !------------------------------- ;
  29:J P[1] 80% CNT50    ;
  30:J P[2] 80% CNT50    ;
  31:   ;
  32:  !------------------------------- ;
  33:  !ATTESA ENTRARE ;
  34:  WAIT DI[64:TAPPARE.CH-1]=ON AND DI[65:OK ENTRA St1-C1]=ON AND DI[66:PLT Pres.St-1-C1]=ON    ;
  35:  WAIT    .50(sec) ;
  36:   ;
  37:  DO[66:RICH.DEPO.BAIA-1A]=OFF ;
  38:  DO[65:RICH.PREL BAIA-1A]=ON ;
  39:  DO[64:RBT Fuor.ing.St1]=OFF ;
  40:   ;
  41:  !------------------------------- ;
  42:  !Prelievo pezzo ;
  43:  !------------------------------- ;
  44:  PR[50:*]=PR[47:Zero XYZ_NUT]    ;
  45:  PR[50,2:*]=650    ;
  46:  PR[50,3:*]=300    ;
  47:   ;
  48:  PR[51:*]=PR[47:Zero XYZ_NUT]    ;
  49:  PR[51,3:*]=20    ;
  50:   ;
  51:  PR[52:*]=PR[47:Zero XYZ_NUT]    ;
  52:  PR[52,2:*]=70    ;
  53:   ;
  54:  PR[53:*]=PR[47:Zero XYZ_NUT]    ;
  55:  PR[53,2:*]=650    ;
  56:  PR[53,3:*]=10    ;
  57:   ;
  58:  PR[54:*]=PR[47:Zero XYZ_NUT]    ;
  59:  PR[54,2:*]=400    ;
  60:  PR[54,3:*]=300    ;
  61:   ;
  62:  !------------------------------- ;
  63:L P[3] 1000mm/sec CNT2 Offset,PR[53:*]    ;
  64:L P[3] 600mm/sec CNT2 Offset,PR[52:*]    ;
  65:   ;
  66:  !------------------------------- ;
  67:  !PUNTO FINALE ;
  68:L P[3] 50mm/sec FINE    ;
  69:  CALL P1_CHIUD    ;
  70:  !------------------------------- ;
  71:L P[3] 500mm/sec FINE Offset,PR[51:*]    ;
  72:   ;
  73:  !ATTESA PRESENZA PEZZO ;
  74:  $WAITTMOUT=100 ;
  75:  WAIT RI[4:PINZA CH]=ON TIMEOUT,LBL[2] ;
  76:  LBL[2] ;
  77:  $WAITTMOUT=3000 ;
  78:   ;
  79:  WAIT RI[3:PINZA AP.]=OFF AND RI[4:PINZA CH]=OFF    ;
  80:  !------------------------------- ;
  81:L P[4] 1000mm/sec CNT10 Offset,PR[54:*]    ;
  82:   ;
  83:  CALL CNT_PRES_CONO    ;
  84:   ;
  85:L P[3] 1000mm/sec CNT100 Offset,PR[50:*]    ;
  86:   ;
  87:  !------------------------------- ;
  88:  DO[65:RICH.PREL BAIA-1A]=OFF ;
  89:  DO[64:RBT Fuor.ing.St1]=ON ;
  90:J P[2] 80% CNT50    ;
  91:J P[1] 80% CNT50    ;
  92:   ;
  93:   ;
/POS
P[1]{
   GP1:
	UF : 0, UT : 2,	
	J1=     0.000 deg,	J2=   -55.000 deg,	J3=    25.000 deg,
	J4=      .000 deg,	J5=   -25.000 deg,	J6=    90.000 deg,
	E1=  7000.000  mm
};
P[2]{
   GP1:
	UF : 0, UT : 2,	
	J1=   -19.524 deg,	J2=   -41.156 deg,	J3=    -2.945 deg,
	J4=   -88.797 deg,	J5=    70.517 deg,	J6=   176.620 deg,
	E1=  7000.000  mm
};
P[3]{
   GP1:
	UF : 0, UT : 2,		CONFIG : 'F U T, 0, 0, 0',
	X =  8179.712  mm,	Y = -1804.410  mm,	Z =   180.377  mm,
	W =      .202 deg,	P =     -.150 deg,	R =   -90.005 deg,
	E1=  7000.000  mm
};
P[4]{
   GP1:
	UF : 0, UT : 2,		CONFIG : 'F U T, 0, 0, 0',
	X =  8179.712  mm,	Y = -1804.410  mm,	Z =   180.377  mm,
	W =      .202 deg,	P =     -.150 deg,	R =   -90.005 deg,
	E1=  7000.000  mm
};
/END
