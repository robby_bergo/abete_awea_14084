/PROG  PLT5_XYZ
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "coordinate MAG5";
PROG_SIZE	= 14450;
CREATE		= DATE 07-12-08  TIME 21:47:16;
MODIFIED	= DATE 15-11-17  TIME 19:26:50;
FILE_NAME	= ;
VERSION		= 0;
LINE_COUNT	= 924;
MEMORY_SIZE	= 15642;
PROTECT		= READ;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= 1,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/MN
   1:  !------------------------------- ;
   2:  !Calcola le coordinate dei pallet ;
   3:  !sul magazzino ;
   4:  !------------------------------- ;
   5:   ;
   6:  IF R[2:N�Baia]<1 OR R[2:N�Baia]>100,JMP LBL[1000] ;
   7:   ;
   8:  JMP LBL[R[2]] ;
   9:   ;
  10:  LBL[1] ;
  11:  !Piano dall' Baia_1 ;
  12:  R[18:N'FILE]=0    ;
  13:  R[19:N'COLLONNE]=0    ;
  14:  R[22:CORRET.IN.Z]=0    ;
  15:  R[23:CORRET.IN.X]=0    ;
  16:  R[24:CORRET.IN.Y]=0    ;
  17:  END ;
  18:   ;
  19:  LBL[2] ;
  20:  !Piano dall' Baia_2 ;
  21:  R[18:N'FILE]=0    ;
  22:  R[19:N'COLLONNE]=1    ;
  23:  R[22:CORRET.IN.Z]=0    ;
  24:  R[23:CORRET.IN.X]=0    ;
  25:  R[24:CORRET.IN.Y]=0    ;
  26:  END ;
  27:   ;
  28:  LBL[3] ;
  29:  !Piano dall' Baia_3 ;
  30:  R[18:N'FILE]=0    ;
  31:  R[19:N'COLLONNE]=2    ;
  32:  R[22:CORRET.IN.Z]=0    ;
  33:  R[23:CORRET.IN.X]=0    ;
  34:  R[24:CORRET.IN.Y]=0    ;
  35:  END ;
  36:   ;
  37:  LBL[4] ;
  38:  !Piano dall' Baia_4 ;
  39:  R[18:N'FILE]=0    ;
  40:  R[19:N'COLLONNE]=3    ;
  41:  R[22:CORRET.IN.Z]=0    ;
  42:  R[23:CORRET.IN.X]=0    ;
  43:  R[24:CORRET.IN.Y]=0    ;
  44:  END ;
  45:   ;
  46:  LBL[5] ;
  47:  !Piano dall' Baia_5 ;
  48:  R[18:N'FILE]=0    ;
  49:  R[19:N'COLLONNE]=4    ;
  50:  R[22:CORRET.IN.Z]=0    ;
  51:  R[23:CORRET.IN.X]=0    ;
  52:  R[24:CORRET.IN.Y]=0    ;
  53:  END ;
  54:   ;
  55:  LBL[6] ;
  56:  !Piano dall' Baia_6 ;
  57:  R[18:N'FILE]=0    ;
  58:  R[19:N'COLLONNE]=5    ;
  59:  R[22:CORRET.IN.Z]=0    ;
  60:  R[23:CORRET.IN.X]=0    ;
  61:  R[24:CORRET.IN.Y]=1    ;
  62:  END ;
  63:   ;
  64:  LBL[7] ;
  65:  !Piano dall' Baia_7 ;
  66:  R[18:N'FILE]=0    ;
  67:  R[19:N'COLLONNE]=6    ;
  68:  R[22:CORRET.IN.Z]=0    ;
  69:  R[23:CORRET.IN.X]=0    ;
  70:  R[24:CORRET.IN.Y]=1    ;
  71:  END ;
  72:   ;
  73:  LBL[8] ;
  74:  !Piano dall' Baia_8 ;
  75:  R[18:N'FILE]=0    ;
  76:  R[19:N'COLLONNE]=7    ;
  77:  R[22:CORRET.IN.Z]=0    ;
  78:  R[23:CORRET.IN.X]=0    ;
  79:  R[24:CORRET.IN.Y]=1    ;
  80:  END ;
  81:   ;
  82:  LBL[9] ;
  83:  !Piano dall' Baia_9 ;
  84:  R[18:N'FILE]=0    ;
  85:  R[19:N'COLLONNE]=8    ;
  86:  R[22:CORRET.IN.Z]=0    ;
  87:  R[23:CORRET.IN.X]=0    ;
  88:  R[24:CORRET.IN.Y]=1    ;
  89:  END ;
  90:   ;
  91:  LBL[10] ;
  92:  !Piano dall' Baia_10 ;
  93:  R[18:N'FILE]=0    ;
  94:  R[19:N'COLLONNE]=9    ;
  95:  R[22:CORRET.IN.Z]=0    ;
  96:  R[23:CORRET.IN.X]=0    ;
  97:  R[24:CORRET.IN.Y]=1    ;
  98:  END ;
  99:   ;
 100:  LBL[11] ;
 101:  !Piano dall' Baia_11 ;
 102:  R[18:N'FILE]=0    ;
 103:  R[19:N'COLLONNE]=10    ;
 104:  R[22:CORRET.IN.Z]=0    ;
 105:  R[23:CORRET.IN.X]=0    ;
 106:  R[24:CORRET.IN.Y]=1    ;
 107:  END ;
 108:   ;
 109:  LBL[12] ;
 110:  !Piano dall' Baia_12 ;
 111:  R[18:N'FILE]=0    ;
 112:  R[19:N'COLLONNE]=11    ;
 113:  R[22:CORRET.IN.Z]=0    ;
 114:  R[23:CORRET.IN.X]=0    ;
 115:  R[24:CORRET.IN.Y]=1    ;
 116:  END ;
 117:   ;
 118:  LBL[13] ;
 119:  !Piano dall' Baia_13 ;
 120:  R[18:N'FILE]=0    ;
 121:  R[19:N'COLLONNE]=12    ;
 122:  R[22:CORRET.IN.Z]=0    ;
 123:  R[23:CORRET.IN.X]=0    ;
 124:  R[24:CORRET.IN.Y]=1.5    ;
 125:  END ;
 126:   ;
 127:  LBL[14] ;
 128:  !Piano dall' Baia_14 ;
 129:  R[18:N'FILE]=0    ;
 130:  R[19:N'COLLONNE]=13    ;
 131:  R[22:CORRET.IN.Z]=0    ;
 132:  R[23:CORRET.IN.X]=0    ;
 133:  R[24:CORRET.IN.Y]=1.5    ;
 134:  END ;
 135:   ;
 136:  LBL[15] ;
 137:  !Piano dall' Baia_15 ;
 138:  R[18:N'FILE]=0    ;
 139:  R[19:N'COLLONNE]=14    ;
 140:  R[22:CORRET.IN.Z]=0    ;
 141:  R[23:CORRET.IN.X]=0    ;
 142:  R[24:CORRET.IN.Y]=1.5    ;
 143:  END ;
 144:   ;
 145:  LBL[16] ;
 146:  !Piano dall' Baia_16 ;
 147:  R[18:N'FILE]=0    ;
 148:  R[19:N'COLLONNE]=15    ;
 149:  R[22:CORRET.IN.Z]=0    ;
 150:  R[23:CORRET.IN.X]=0    ;
 151:  R[24:CORRET.IN.Y]=1.5    ;
 152:  END ;
 153:   ;
 154:  LBL[17] ;
 155:  !Piano dall' Baia_17 ;
 156:  R[18:N'FILE]=0    ;
 157:  R[19:N'COLLONNE]=16    ;
 158:  R[22:CORRET.IN.Z]=0    ;
 159:  R[23:CORRET.IN.X]=0    ;
 160:  R[24:CORRET.IN.Y]=1.5    ;
 161:  END ;
 162:   ;
 163:  LBL[18] ;
 164:  !Piano dall' Baia_18 ;
 165:  R[18:N'FILE]=0    ;
 166:  R[19:N'COLLONNE]=17    ;
 167:  R[22:CORRET.IN.Z]=0    ;
 168:  R[23:CORRET.IN.X]=0    ;
 169:  R[24:CORRET.IN.Y]=1.5    ;
 170:  END ;
 171:   ;
 172:  LBL[19] ;
 173:  !Piano dall' Baia_19 ;
 174:  R[18:N'FILE]=0    ;
 175:  R[19:N'COLLONNE]=18    ;
 176:  R[22:CORRET.IN.Z]=0    ;
 177:  R[23:CORRET.IN.X]=0    ;
 178:  R[24:CORRET.IN.Y]=1.5    ;
 179:  END ;
 180:   ;
 181:  LBL[20] ;
 182:  !Piano dall' Baia_20 ;
 183:  R[18:N'FILE]=0    ;
 184:  R[19:N'COLLONNE]=19    ;
 185:  R[22:CORRET.IN.Z]=0    ;
 186:  R[23:CORRET.IN.X]=0    ;
 187:  R[24:CORRET.IN.Y]=1.5    ;
 188:  END ;
 189:  !------------------------------- ;
 190:  !------------------------------- ;
 191:   ;
 192:  LBL[21] ;
 193:  !Piano dall' Baia_21 ;
 194:  R[18:N'FILE]=1    ;
 195:  R[19:N'COLLONNE]=0    ;
 196:  R[22:CORRET.IN.Z]=0    ;
 197:  R[23:CORRET.IN.X]=0    ;
 198:  R[24:CORRET.IN.Y]=(-2)    ;
 199:  END ;
 200:   ;
 201:  LBL[22] ;
 202:  !Piano dall' Baia_22 ;
 203:  R[18:N'FILE]=1    ;
 204:  R[19:N'COLLONNE]=1    ;
 205:  R[22:CORRET.IN.Z]=0    ;
 206:  R[23:CORRET.IN.X]=0    ;
 207:  R[24:CORRET.IN.Y]=(-2)    ;
 208:  END ;
 209:   ;
 210:  LBL[23] ;
 211:  !Piano dall' Baia_23 ;
 212:  R[18:N'FILE]=1    ;
 213:  R[19:N'COLLONNE]=2    ;
 214:  R[22:CORRET.IN.Z]=0    ;
 215:  R[23:CORRET.IN.X]=0    ;
 216:  R[24:CORRET.IN.Y]=(-2)    ;
 217:  END ;
 218:   ;
 219:  LBL[24] ;
 220:  !Piano dall' Baia_24 ;
 221:  R[18:N'FILE]=1    ;
 222:  R[19:N'COLLONNE]=3    ;
 223:  R[22:CORRET.IN.Z]=0    ;
 224:  R[23:CORRET.IN.X]=0    ;
 225:  R[24:CORRET.IN.Y]=(-1)    ;
 226:  END ;
 227:   ;
 228:  LBL[25] ;
 229:  !Piano dall' Baia_25 ;
 230:  R[18:N'FILE]=1    ;
 231:  R[19:N'COLLONNE]=4    ;
 232:  R[22:CORRET.IN.Z]=0    ;
 233:  R[23:CORRET.IN.X]=0    ;
 234:  R[24:CORRET.IN.Y]=(-1)    ;
 235:  END ;
 236:   ;
 237:  LBL[26] ;
 238:  !Piano dall' Baia_26 ;
 239:  R[18:N'FILE]=1    ;
 240:  R[19:N'COLLONNE]=5    ;
 241:  R[22:CORRET.IN.Z]=0    ;
 242:  R[23:CORRET.IN.X]=0    ;
 243:  R[24:CORRET.IN.Y]=(-1)    ;
 244:  END ;
 245:   ;
 246:  LBL[27] ;
 247:  !Piano dall' Baia_27 ;
 248:  R[18:N'FILE]=1    ;
 249:  R[19:N'COLLONNE]=6    ;
 250:  R[22:CORRET.IN.Z]=0    ;
 251:  R[23:CORRET.IN.X]=0    ;
 252:  R[24:CORRET.IN.Y]=(-1)    ;
 253:  END ;
 254:   ;
 255:  LBL[28] ;
 256:  !Piano dall' Baia_28 ;
 257:  R[18:N'FILE]=1    ;
 258:  R[19:N'COLLONNE]=7    ;
 259:  R[22:CORRET.IN.Z]=0    ;
 260:  R[23:CORRET.IN.X]=0    ;
 261:  R[24:CORRET.IN.Y]=(-1)    ;
 262:  END ;
 263:   ;
 264:  LBL[29] ;
 265:  !Piano dall' Baia_29 ;
 266:  R[18:N'FILE]=1    ;
 267:  R[19:N'COLLONNE]=8    ;
 268:  R[22:CORRET.IN.Z]=0    ;
 269:  R[23:CORRET.IN.X]=0    ;
 270:  R[24:CORRET.IN.Y]=(-1)    ;
 271:  END ;
 272:   ;
 273:  LBL[30] ;
 274:  !Piano dall' Baia_30 ;
 275:  R[18:N'FILE]=1    ;
 276:  R[19:N'COLLONNE]=9    ;
 277:  R[22:CORRET.IN.Z]=0    ;
 278:  R[23:CORRET.IN.X]=0    ;
 279:  R[24:CORRET.IN.Y]=(-1)    ;
 280:  END ;
 281:   ;
 282:  LBL[31] ;
 283:  !Piano dall' Baia_31 ;
 284:  R[18:N'FILE]=1    ;
 285:  R[19:N'COLLONNE]=10    ;
 286:  R[22:CORRET.IN.Z]=0    ;
 287:  R[23:CORRET.IN.X]=0    ;
 288:  R[24:CORRET.IN.Y]=(-1)    ;
 289:  END ;
 290:   ;
 291:  LBL[32] ;
 292:  !Piano dall' Baia_32 ;
 293:  R[18:N'FILE]=1    ;
 294:  R[19:N'COLLONNE]=11    ;
 295:  R[22:CORRET.IN.Z]=0    ;
 296:  R[23:CORRET.IN.X]=0    ;
 297:  R[24:CORRET.IN.Y]=0    ;
 298:  END ;
 299:   ;
 300:  LBL[33] ;
 301:  !Piano dall' Baia_33 ;
 302:  R[18:N'FILE]=1    ;
 303:  R[19:N'COLLONNE]=12    ;
 304:  R[22:CORRET.IN.Z]=0    ;
 305:  R[23:CORRET.IN.X]=0    ;
 306:  R[24:CORRET.IN.Y]=0    ;
 307:  END ;
 308:   ;
 309:  LBL[34] ;
 310:  !Piano dall' Baia_34 ;
 311:  R[18:N'FILE]=1    ;
 312:  R[19:N'COLLONNE]=13    ;
 313:  R[22:CORRET.IN.Z]=0    ;
 314:  R[23:CORRET.IN.X]=0    ;
 315:  R[24:CORRET.IN.Y]=0    ;
 316:  END ;
 317:   ;
 318:  LBL[35] ;
 319:  !Piano dall' Baia_35 ;
 320:  R[18:N'FILE]=1    ;
 321:  R[19:N'COLLONNE]=14    ;
 322:  R[22:CORRET.IN.Z]=0    ;
 323:  R[23:CORRET.IN.X]=0    ;
 324:  R[24:CORRET.IN.Y]=0    ;
 325:  END ;
 326:   ;
 327:  LBL[36] ;
 328:  !Piano dall' Baia_36 ;
 329:  R[18:N'FILE]=1    ;
 330:  R[19:N'COLLONNE]=15    ;
 331:  R[22:CORRET.IN.Z]=0    ;
 332:  R[23:CORRET.IN.X]=0    ;
 333:  R[24:CORRET.IN.Y]=0    ;
 334:  END ;
 335:   ;
 336:  LBL[37] ;
 337:  !Piano dall' Baia_37 ;
 338:  R[18:N'FILE]=1    ;
 339:  R[19:N'COLLONNE]=16    ;
 340:  R[22:CORRET.IN.Z]=0    ;
 341:  R[23:CORRET.IN.X]=0    ;
 342:  R[24:CORRET.IN.Y]=0    ;
 343:  END ;
 344:   ;
 345:  LBL[38] ;
 346:  !Piano dall' Baia_38 ;
 347:  R[18:N'FILE]=1    ;
 348:  R[19:N'COLLONNE]=17    ;
 349:  R[22:CORRET.IN.Z]=0    ;
 350:  R[23:CORRET.IN.X]=0    ;
 351:  R[24:CORRET.IN.Y]=0    ;
 352:  END ;
 353:   ;
 354:  LBL[39] ;
 355:  !Piano dall' Baia_39 ;
 356:  R[18:N'FILE]=1    ;
 357:  R[19:N'COLLONNE]=18    ;
 358:  R[22:CORRET.IN.Z]=0    ;
 359:  R[23:CORRET.IN.X]=0    ;
 360:  R[24:CORRET.IN.Y]=0    ;
 361:  END ;
 362:   ;
 363:  LBL[40] ;
 364:  !Piano dall' Baia_40 ;
 365:  R[18:N'FILE]=1    ;
 366:  R[19:N'COLLONNE]=19    ;
 367:  R[22:CORRET.IN.Z]=0    ;
 368:  R[23:CORRET.IN.X]=0    ;
 369:  R[24:CORRET.IN.Y]=1    ;
 370:  END ;
 371:  !------------------------------- ;
 372:  !------------------------------- ;
 373:  LBL[41] ;
 374:  !Piano dall' Baia_41 ;
 375:  R[18:N'FILE]=2    ;
 376:  R[19:N'COLLONNE]=0    ;
 377:  R[22:CORRET.IN.Z]=0    ;
 378:  R[23:CORRET.IN.X]=0    ;
 379:  R[24:CORRET.IN.Y]=0    ;
 380:  END ;
 381:   ;
 382:  LBL[42] ;
 383:  !Piano dall' Baia_42 ;
 384:  R[18:N'FILE]=2    ;
 385:  R[19:N'COLLONNE]=1    ;
 386:  R[22:CORRET.IN.Z]=0    ;
 387:  R[23:CORRET.IN.X]=0    ;
 388:  R[24:CORRET.IN.Y]=0    ;
 389:  END ;
 390:   ;
 391:  LBL[43] ;
 392:  !Piano dall' Baia_43 ;
 393:  R[18:N'FILE]=2    ;
 394:  R[19:N'COLLONNE]=2    ;
 395:  R[22:CORRET.IN.Z]=0    ;
 396:  R[23:CORRET.IN.X]=0    ;
 397:  R[24:CORRET.IN.Y]=0    ;
 398:  END ;
 399:   ;
 400:  LBL[44] ;
 401:  !Piano dall' Baia_44 ;
 402:  R[18:N'FILE]=2    ;
 403:  R[19:N'COLLONNE]=3    ;
 404:  R[22:CORRET.IN.Z]=0    ;
 405:  R[23:CORRET.IN.X]=0    ;
 406:  R[24:CORRET.IN.Y]=0    ;
 407:  END ;
 408:   ;
 409:  LBL[45] ;
 410:  !Piano dall' Baia_45 ;
 411:  R[18:N'FILE]=2    ;
 412:  R[19:N'COLLONNE]=4    ;
 413:  R[22:CORRET.IN.Z]=0    ;
 414:  R[23:CORRET.IN.X]=0    ;
 415:  R[24:CORRET.IN.Y]=0    ;
 416:  END ;
 417:   ;
 418:  LBL[46] ;
 419:  !Piano dall' Baia_46 ;
 420:  R[18:N'FILE]=2    ;
 421:  R[19:N'COLLONNE]=5    ;
 422:  R[22:CORRET.IN.Z]=0    ;
 423:  R[23:CORRET.IN.X]=0    ;
 424:  R[24:CORRET.IN.Y]=0    ;
 425:  END ;
 426:   ;
 427:  LBL[47] ;
 428:  !Piano dall' Baia_47 ;
 429:  R[18:N'FILE]=2    ;
 430:  R[19:N'COLLONNE]=6    ;
 431:  R[22:CORRET.IN.Z]=0    ;
 432:  R[23:CORRET.IN.X]=0    ;
 433:  R[24:CORRET.IN.Y]=0    ;
 434:  END ;
 435:   ;
 436:  LBL[48] ;
 437:  !Piano dall' Baia_48 ;
 438:  R[18:N'FILE]=2    ;
 439:  R[19:N'COLLONNE]=7    ;
 440:  R[22:CORRET.IN.Z]=0    ;
 441:  R[23:CORRET.IN.X]=0    ;
 442:  R[24:CORRET.IN.Y]=0    ;
 443:  END ;
 444:   ;
 445:  LBL[49] ;
 446:  !Piano dall' Baia_49 ;
 447:  R[18:N'FILE]=2    ;
 448:  R[19:N'COLLONNE]=8    ;
 449:  R[22:CORRET.IN.Z]=0    ;
 450:  R[23:CORRET.IN.X]=0    ;
 451:  R[24:CORRET.IN.Y]=0    ;
 452:  END ;
 453:   ;
 454:  LBL[50] ;
 455:  !Piano dall' Baia_50 ;
 456:  R[18:N'FILE]=2    ;
 457:  R[19:N'COLLONNE]=9    ;
 458:  R[22:CORRET.IN.Z]=0    ;
 459:  R[23:CORRET.IN.X]=0    ;
 460:  R[24:CORRET.IN.Y]=0    ;
 461:  END ;
 462:   ;
 463:  LBL[51] ;
 464:  !Piano dall' Baia_51 ;
 465:  R[18:N'FILE]=2    ;
 466:  R[19:N'COLLONNE]=10    ;
 467:  R[22:CORRET.IN.Z]=0    ;
 468:  R[23:CORRET.IN.X]=0    ;
 469:  R[24:CORRET.IN.Y]=2    ;
 470:  END ;
 471:   ;
 472:  LBL[52] ;
 473:  !Piano dall' Baia_52 ;
 474:  R[18:N'FILE]=2    ;
 475:  R[19:N'COLLONNE]=11    ;
 476:  R[22:CORRET.IN.Z]=0    ;
 477:  R[23:CORRET.IN.X]=0    ;
 478:  R[24:CORRET.IN.Y]=2    ;
 479:  END ;
 480:   ;
 481:  LBL[53] ;
 482:  !Piano dall' Baia_53 ;
 483:  R[18:N'FILE]=2    ;
 484:  R[19:N'COLLONNE]=12    ;
 485:  R[22:CORRET.IN.Z]=0    ;
 486:  R[23:CORRET.IN.X]=0    ;
 487:  R[24:CORRET.IN.Y]=2    ;
 488:  END ;
 489:   ;
 490:  LBL[54] ;
 491:  !Piano dall' Baia_54 ;
 492:  R[18:N'FILE]=2    ;
 493:  R[19:N'COLLONNE]=13    ;
 494:  R[22:CORRET.IN.Z]=0    ;
 495:  R[23:CORRET.IN.X]=0    ;
 496:  R[24:CORRET.IN.Y]=2    ;
 497:  END ;
 498:   ;
 499:  LBL[55] ;
 500:  !Piano dall' Baia_55 ;
 501:  R[18:N'FILE]=2    ;
 502:  R[19:N'COLLONNE]=14    ;
 503:  R[22:CORRET.IN.Z]=0    ;
 504:  R[23:CORRET.IN.X]=0    ;
 505:  R[24:CORRET.IN.Y]=2    ;
 506:  END ;
 507:   ;
 508:  LBL[56] ;
 509:  !Piano dall' Baia_56 ;
 510:  R[18:N'FILE]=2    ;
 511:  R[19:N'COLLONNE]=15    ;
 512:  R[22:CORRET.IN.Z]=0    ;
 513:  R[23:CORRET.IN.X]=0    ;
 514:  R[24:CORRET.IN.Y]=2    ;
 515:  END ;
 516:   ;
 517:  LBL[57] ;
 518:  !Piano dall' Baia_57 ;
 519:  R[18:N'FILE]=2    ;
 520:  R[19:N'COLLONNE]=16    ;
 521:  R[22:CORRET.IN.Z]=0    ;
 522:  R[23:CORRET.IN.X]=0    ;
 523:  R[24:CORRET.IN.Y]=2    ;
 524:  END ;
 525:   ;
 526:  LBL[58] ;
 527:  !Piano dall' Baia_58 ;
 528:  R[18:N'FILE]=2    ;
 529:  R[19:N'COLLONNE]=17    ;
 530:  R[22:CORRET.IN.Z]=0    ;
 531:  R[23:CORRET.IN.X]=0    ;
 532:  R[24:CORRET.IN.Y]=2    ;
 533:  END ;
 534:   ;
 535:  LBL[59] ;
 536:  !Piano dall' Baia_59 ;
 537:  R[18:N'FILE]=2    ;
 538:  R[19:N'COLLONNE]=18    ;
 539:  R[22:CORRET.IN.Z]=0    ;
 540:  R[23:CORRET.IN.X]=0    ;
 541:  R[24:CORRET.IN.Y]=2    ;
 542:  END ;
 543:   ;
 544:  LBL[60] ;
 545:  !Piano dall' Baia_60 ;
 546:  R[18:N'FILE]=2    ;
 547:  R[19:N'COLLONNE]=19    ;
 548:  R[22:CORRET.IN.Z]=0    ;
 549:  R[23:CORRET.IN.X]=0    ;
 550:  R[24:CORRET.IN.Y]=2    ;
 551:  END ;
 552:  !------------------------------- ;
 553:  !------------------------------- ;
 554:   ;
 555:  LBL[61] ;
 556:  !Piano dall' Baia_61 ;
 557:  R[18:N'FILE]=3    ;
 558:  R[19:N'COLLONNE]=0    ;
 559:  R[22:CORRET.IN.Z]=0    ;
 560:  R[23:CORRET.IN.X]=2    ;
 561:  R[24:CORRET.IN.Y]=0    ;
 562:  END ;
 563:   ;
 564:  LBL[62] ;
 565:  !Piano dall' Baia_62 ;
 566:  R[18:N'FILE]=3    ;
 567:  R[19:N'COLLONNE]=1    ;
 568:  R[22:CORRET.IN.Z]=0    ;
 569:  R[23:CORRET.IN.X]=2    ;
 570:  R[24:CORRET.IN.Y]=0    ;
 571:  END ;
 572:   ;
 573:  LBL[63] ;
 574:  !Piano dall' Baia_63 ;
 575:  R[18:N'FILE]=3    ;
 576:  R[19:N'COLLONNE]=2    ;
 577:  R[22:CORRET.IN.Z]=0    ;
 578:  R[23:CORRET.IN.X]=2    ;
 579:  R[24:CORRET.IN.Y]=0    ;
 580:  END ;
 581:   ;
 582:  LBL[64] ;
 583:  !Piano dall' Baia_64 ;
 584:  R[18:N'FILE]=3    ;
 585:  R[19:N'COLLONNE]=3    ;
 586:  R[22:CORRET.IN.Z]=0    ;
 587:  R[23:CORRET.IN.X]=2    ;
 588:  R[24:CORRET.IN.Y]=0    ;
 589:  END ;
 590:   ;
 591:  LBL[65] ;
 592:  !Piano dall' Baia_65 ;
 593:  R[18:N'FILE]=3    ;
 594:  R[19:N'COLLONNE]=4    ;
 595:  R[22:CORRET.IN.Z]=0    ;
 596:  R[23:CORRET.IN.X]=2    ;
 597:  R[24:CORRET.IN.Y]=0    ;
 598:  END ;
 599:   ;
 600:  LBL[66] ;
 601:  !Piano dall' Baia_66 ;
 602:  R[18:N'FILE]=3    ;
 603:  R[19:N'COLLONNE]=5    ;
 604:  R[22:CORRET.IN.Z]=0    ;
 605:  R[23:CORRET.IN.X]=2    ;
 606:  R[24:CORRET.IN.Y]=0    ;
 607:  END ;
 608:   ;
 609:  LBL[67] ;
 610:  !Piano dall' Baia_67 ;
 611:  R[18:N'FILE]=3    ;
 612:  R[19:N'COLLONNE]=6    ;
 613:  R[22:CORRET.IN.Z]=0    ;
 614:  R[23:CORRET.IN.X]=2    ;
 615:  R[24:CORRET.IN.Y]=0    ;
 616:  END ;
 617:   ;
 618:  LBL[68] ;
 619:  !Piano dall' Baia_68 ;
 620:  R[18:N'FILE]=3    ;
 621:  R[19:N'COLLONNE]=7    ;
 622:  R[22:CORRET.IN.Z]=0    ;
 623:  R[23:CORRET.IN.X]=2    ;
 624:  R[24:CORRET.IN.Y]=1    ;
 625:  END ;
 626:   ;
 627:  LBL[69] ;
 628:  !Piano dall' Baia_69 ;
 629:  R[18:N'FILE]=3    ;
 630:  R[19:N'COLLONNE]=8    ;
 631:  R[22:CORRET.IN.Z]=0    ;
 632:  R[23:CORRET.IN.X]=2    ;
 633:  R[24:CORRET.IN.Y]=1    ;
 634:  END ;
 635:   ;
 636:  LBL[70] ;
 637:  !Piano dall' Baia_70 ;
 638:  R[18:N'FILE]=3    ;
 639:  R[19:N'COLLONNE]=9    ;
 640:  R[22:CORRET.IN.Z]=0    ;
 641:  R[23:CORRET.IN.X]=2    ;
 642:  R[24:CORRET.IN.Y]=1    ;
 643:  END ;
 644:   ;
 645:  LBL[71] ;
 646:  !Piano dall' Baia_71 ;
 647:  R[18:N'FILE]=3    ;
 648:  R[19:N'COLLONNE]=10    ;
 649:  R[22:CORRET.IN.Z]=1    ;
 650:  R[23:CORRET.IN.X]=2    ;
 651:  R[24:CORRET.IN.Y]=2    ;
 652:  END ;
 653:   ;
 654:  LBL[72] ;
 655:  !Piano dall' Baia_72 ;
 656:  R[18:N'FILE]=3    ;
 657:  R[19:N'COLLONNE]=11    ;
 658:  R[22:CORRET.IN.Z]=1    ;
 659:  R[23:CORRET.IN.X]=2    ;
 660:  R[24:CORRET.IN.Y]=2    ;
 661:  END ;
 662:   ;
 663:  LBL[73] ;
 664:  !Piano dall' Baia_73 ;
 665:  R[18:N'FILE]=3    ;
 666:  R[19:N'COLLONNE]=12    ;
 667:  R[22:CORRET.IN.Z]=1    ;
 668:  R[23:CORRET.IN.X]=2    ;
 669:  R[24:CORRET.IN.Y]=2    ;
 670:  END ;
 671:   ;
 672:  LBL[74] ;
 673:  !Piano dall' Baia_74 ;
 674:  R[18:N'FILE]=3    ;
 675:  R[19:N'COLLONNE]=13    ;
 676:  R[22:CORRET.IN.Z]=1    ;
 677:  R[23:CORRET.IN.X]=2    ;
 678:  R[24:CORRET.IN.Y]=1    ;
 679:  END ;
 680:   ;
 681:  LBL[75] ;
 682:  !Piano dall' Baia_75 ;
 683:  R[18:N'FILE]=3    ;
 684:  R[19:N'COLLONNE]=14    ;
 685:  R[22:CORRET.IN.Z]=1    ;
 686:  R[23:CORRET.IN.X]=2    ;
 687:  R[24:CORRET.IN.Y]=2    ;
 688:  END ;
 689:   ;
 690:  LBL[76] ;
 691:  !Piano dall' Baia_76 ;
 692:  R[18:N'FILE]=3    ;
 693:  R[19:N'COLLONNE]=15    ;
 694:  R[22:CORRET.IN.Z]=1    ;
 695:  R[23:CORRET.IN.X]=2    ;
 696:  R[24:CORRET.IN.Y]=2    ;
 697:  END ;
 698:   ;
 699:  LBL[77] ;
 700:  !Piano dall' Baia_77 ;
 701:  R[18:N'FILE]=3    ;
 702:  R[19:N'COLLONNE]=16    ;
 703:  R[22:CORRET.IN.Z]=1    ;
 704:  R[23:CORRET.IN.X]=2    ;
 705:  R[24:CORRET.IN.Y]=2    ;
 706:  END ;
 707:   ;
 708:  LBL[78] ;
 709:  !Piano dall' Baia_78 ;
 710:  R[18:N'FILE]=3    ;
 711:  R[19:N'COLLONNE]=17    ;
 712:  R[22:CORRET.IN.Z]=1    ;
 713:  R[23:CORRET.IN.X]=2    ;
 714:  R[24:CORRET.IN.Y]=2    ;
 715:  END ;
 716:   ;
 717:  LBL[79] ;
 718:  !Piano dall' Baia_79 ;
 719:  R[18:N'FILE]=3    ;
 720:  R[19:N'COLLONNE]=18    ;
 721:  R[22:CORRET.IN.Z]=1    ;
 722:  R[23:CORRET.IN.X]=2    ;
 723:  R[24:CORRET.IN.Y]=2    ;
 724:  END ;
 725:   ;
 726:  LBL[80] ;
 727:  !Piano dall' Baia_80 ;
 728:  R[18:N'FILE]=3    ;
 729:  R[19:N'COLLONNE]=19    ;
 730:  R[22:CORRET.IN.Z]=1    ;
 731:  R[23:CORRET.IN.X]=2    ;
 732:  R[24:CORRET.IN.Y]=2    ;
 733:  END ;
 734:  !------------------------------- ;
 735:  !------------------------------- ;
 736:   ;
 737:  LBL[81] ;
 738:  !Piano dall' Baia_81 ;
 739:  R[18:N'FILE]=4    ;
 740:  R[19:N'COLLONNE]=0    ;
 741:  R[22:CORRET.IN.Z]=0    ;
 742:  R[23:CORRET.IN.X]=2    ;
 743:  R[24:CORRET.IN.Y]=0    ;
 744:  END ;
 745:   ;
 746:  LBL[82] ;
 747:  !Piano dall' Baia_82 ;
 748:  R[18:N'FILE]=4    ;
 749:  R[19:N'COLLONNE]=1    ;
 750:  R[22:CORRET.IN.Z]=0    ;
 751:  R[23:CORRET.IN.X]=2    ;
 752:  R[24:CORRET.IN.Y]=0    ;
 753:  END ;
 754:   ;
 755:  LBL[83] ;
 756:  !Piano dall' Baia_83 ;
 757:  R[18:N'FILE]=4    ;
 758:  R[19:N'COLLONNE]=2    ;
 759:  R[22:CORRET.IN.Z]=0    ;
 760:  R[23:CORRET.IN.X]=2    ;
 761:  R[24:CORRET.IN.Y]=0    ;
 762:  END ;
 763:   ;
 764:  LBL[84] ;
 765:  !Piano dall' Baia_84 ;
 766:  R[18:N'FILE]=4    ;
 767:  R[19:N'COLLONNE]=3    ;
 768:  R[22:CORRET.IN.Z]=0    ;
 769:  R[23:CORRET.IN.X]=2    ;
 770:  R[24:CORRET.IN.Y]=0    ;
 771:  END ;
 772:   ;
 773:  LBL[85] ;
 774:  !Piano dall' Baia_85 ;
 775:  R[18:N'FILE]=4    ;
 776:  R[19:N'COLLONNE]=4    ;
 777:  R[22:CORRET.IN.Z]=0    ;
 778:  R[23:CORRET.IN.X]=1    ;
 779:  R[24:CORRET.IN.Y]=0    ;
 780:  END ;
 781:   ;
 782:  LBL[86] ;
 783:  !Piano dall' Baia_86 ;
 784:  R[18:N'FILE]=4    ;
 785:  R[19:N'COLLONNE]=5    ;
 786:  R[22:CORRET.IN.Z]=0    ;
 787:  R[23:CORRET.IN.X]=1    ;
 788:  R[24:CORRET.IN.Y]=0    ;
 789:  END ;
 790:   ;
 791:  LBL[87] ;
 792:  !Piano dall' Baia_87 ;
 793:  R[18:N'FILE]=4    ;
 794:  R[19:N'COLLONNE]=6    ;
 795:  R[22:CORRET.IN.Z]=0    ;
 796:  R[23:CORRET.IN.X]=2    ;
 797:  R[24:CORRET.IN.Y]=0    ;
 798:  END ;
 799:   ;
 800:  LBL[88] ;
 801:  !Piano dall' Baia_88 ;
 802:  R[18:N'FILE]=4    ;
 803:  R[19:N'COLLONNE]=7    ;
 804:  R[22:CORRET.IN.Z]=0    ;
 805:  R[23:CORRET.IN.X]=1    ;
 806:  R[24:CORRET.IN.Y]=0    ;
 807:  END ;
 808:   ;
 809:  LBL[89] ;
 810:  !Piano dall' Baia_89 ;
 811:  R[18:N'FILE]=4    ;
 812:  R[19:N'COLLONNE]=8    ;
 813:  R[22:CORRET.IN.Z]=0    ;
 814:  R[23:CORRET.IN.X]=1    ;
 815:  R[24:CORRET.IN.Y]=1    ;
 816:  END ;
 817:   ;
 818:  LBL[90] ;
 819:  !Piano dall' Baia_90 ;
 820:  R[18:N'FILE]=4    ;
 821:  R[19:N'COLLONNE]=9    ;
 822:  R[22:CORRET.IN.Z]=0    ;
 823:  R[23:CORRET.IN.X]=1    ;
 824:  R[24:CORRET.IN.Y]=1    ;
 825:  END ;
 826:   ;
 827:  LBL[91] ;
 828:  !Piano dall' Baia_91 ;
 829:  R[18:N'FILE]=4    ;
 830:  R[19:N'COLLONNE]=10    ;
 831:  R[22:CORRET.IN.Z]=0    ;
 832:  R[23:CORRET.IN.X]=1    ;
 833:  R[24:CORRET.IN.Y]=0    ;
 834:  END ;
 835:   ;
 836:  LBL[92] ;
 837:  !Piano dall' Baia_92 ;
 838:  R[18:N'FILE]=4    ;
 839:  R[19:N'COLLONNE]=11    ;
 840:  R[22:CORRET.IN.Z]=0    ;
 841:  R[23:CORRET.IN.X]=1    ;
 842:  R[24:CORRET.IN.Y]=0    ;
 843:  END ;
 844:   ;
 845:  LBL[93] ;
 846:  !Piano dall' Baia_93 ;
 847:  R[18:N'FILE]=4    ;
 848:  R[19:N'COLLONNE]=12    ;
 849:  R[22:CORRET.IN.Z]=0    ;
 850:  R[23:CORRET.IN.X]=1    ;
 851:  R[24:CORRET.IN.Y]=0    ;
 852:  END ;
 853:   ;
 854:  LBL[94] ;
 855:  !Piano dall' Baia_94 ;
 856:  R[18:N'FILE]=4    ;
 857:  R[19:N'COLLONNE]=13    ;
 858:  R[22:CORRET.IN.Z]=0    ;
 859:  R[23:CORRET.IN.X]=1    ;
 860:  R[24:CORRET.IN.Y]=0    ;
 861:  END ;
 862:   ;
 863:  LBL[95] ;
 864:  !Piano dall' Baia_95 ;
 865:  R[18:N'FILE]=4    ;
 866:  R[19:N'COLLONNE]=14    ;
 867:  R[22:CORRET.IN.Z]=0    ;
 868:  R[23:CORRET.IN.X]=1    ;
 869:  R[24:CORRET.IN.Y]=0    ;
 870:  END ;
 871:   ;
 872:  LBL[96] ;
 873:  !Piano dall' Baia_96 ;
 874:  R[18:N'FILE]=4    ;
 875:  R[19:N'COLLONNE]=15    ;
 876:  R[22:CORRET.IN.Z]=0    ;
 877:  R[23:CORRET.IN.X]=1    ;
 878:  R[24:CORRET.IN.Y]=0    ;
 879:  END ;
 880:   ;
 881:  LBL[97] ;
 882:  !Piano dall' Baia_97 ;
 883:  R[18:N'FILE]=4    ;
 884:  R[19:N'COLLONNE]=16    ;
 885:  R[22:CORRET.IN.Z]=0    ;
 886:  R[23:CORRET.IN.X]=1    ;
 887:  R[24:CORRET.IN.Y]=0    ;
 888:  END ;
 889:   ;
 890:  LBL[98] ;
 891:  !Piano dall' Baia_98 ;
 892:  R[18:N'FILE]=4    ;
 893:  R[19:N'COLLONNE]=17    ;
 894:  R[22:CORRET.IN.Z]=0    ;
 895:  R[23:CORRET.IN.X]=1    ;
 896:  R[24:CORRET.IN.Y]=1    ;
 897:  END ;
 898:   ;
 899:  LBL[99] ;
 900:  !Piano dall' Baia_99 ;
 901:  R[18:N'FILE]=4    ;
 902:  R[19:N'COLLONNE]=18    ;
 903:  R[22:CORRET.IN.Z]=0    ;
 904:  R[23:CORRET.IN.X]=1    ;
 905:  R[24:CORRET.IN.Y]=1    ;
 906:  END ;
 907:   ;
 908:  LBL[100] ;
 909:  !Piano dall' Baia_100 ;
 910:  R[18:N'FILE]=4    ;
 911:  R[19:N'COLLONNE]=19    ;
 912:  R[22:CORRET.IN.Z]=0    ;
 913:  R[23:CORRET.IN.X]=1    ;
 914:  R[24:CORRET.IN.Y]=1    ;
 915:  END ;
 916:  !------------------------------- ;
 917:  !------------------------------- ;
 918:   ;
 919:  LBL[1000] ;
 920:  !Fine pallet da lavorare ;
 921:  CALL MESSAGGI(10) ;
 922:  PAUSE ;
 923:  ABORT ;
 924:  END ;
/POS
/END
