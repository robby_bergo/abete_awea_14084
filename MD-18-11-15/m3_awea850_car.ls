/PROG  M3_AWEA850_CAR
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "CAR AWEA850 3";
PROG_SIZE	= 2659;
CREATE		= DATE 12-04-17  TIME 00:03:20;
MODIFIED	= DATE 15-10-28  TIME 20:13:36;
FILE_NAME	= ;
VERSION		= 0;
LINE_COUNT	= 108;
MEMORY_SIZE	= 3155;
PROTECT		= READ;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= 1,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/MN
   1:  !------------------------------- ;
   2:  !CARICO la Macch M3_AWEA850 ;
   3:  !------------------------------- ;
   4:  UFRAME_NUM=8 ;
   5:  UTOOL_NUM=1 ;
   6:   ;
   7:  CALL USER_8_MU3    ;
   8:   ;
   9:  LBL[31] ;
  10:  !Controllo PLT su pinza ;
  11:  IF DI[59:Pres.Pz.UT. Mag]=ON AND DI[58:Pres.Pz.PLT Mag]=OFF,JMP LBL[30] ;
  12:  CALL MESSAGGI(24) ;
  13:  PAUSE ;
  14:  R[9:Message Allarmi]=0    ;
  15:  CALL MESSAGGI(1) ;
  16:  JMP LBL[31] ;
  17:  LBL[30] ;
  18:   ;
  19:  !------------------------------ ;
  20:J P[1] 80% CNT50    ;
  21:J P[2] 80% CNT50    ;
  22:L P[3] 1500mm/sec CNT50    ;
  23:   ;
  24:  DO[170:CH.PT.Autom.MU3]=OFF ;
  25:  DO[169:AP.PT.Autom.MU3]=ON ;
  26:  ! Verifico stato CNC ;
  27:  WAIT DI[154:Ch.Cari.PLT MU3]=ON AND DI[172:Porta Auto.AP.MU3]=ON    ;
  28:  DO[165:RBT In Auto.MU3]=OFF ;
  29:   ;
  30:  !CNT Sblocco Del pallet --------- ;
  31:  IF DI[173:PLT Sblocc.MU3]=OFF,CALL CMD_SBL_PLT_M3 ;
  32:   ;
  33:  !------------------------------- ;
  34:  !Calcolo Pos. pezzo ;
  35:  !------------------------------- ;
  36:  PR[12:POS_MU]=PR[47:Zero XYZ_NUT]    ;
  37:  PR[12,1:POS_MU]=R[30:OFFSET_X]+0    ;
  38:  PR[12,2:POS_MU]=R[31:OFFSET_Y]+0    ;
  39:  PR[12,3:POS_MU]=0-R[32:OFFSET_Z]    ;
  40:  PR[12,4:POS_MU]=0    ;
  41:  PR[12,5:POS_MU]=0    ;
  42:  PR[12,6:POS_MU]=(-89.996)    ;
  43:  PR[12,7:POS_MU]=6500    ;
  44:   ;
  45:  PR[51:*]=PR[49:Zero XYZ FUT]    ;
  46:  PR[51,1:*]=0    ;
  47:  PR[51,2:*]=700    ;
  48:  PR[51,3:*]=200    ;
  49:   ;
  50:  PR[52:*]=PR[49:Zero XYZ FUT]    ;
  51:  PR[52,1:*]=0    ;
  52:  PR[52,2:*]=0    ;
  53:  PR[52,3:*]=150    ;
  54:   ;
  55:  PR[53:*]=PR[49:Zero XYZ FUT]    ;
  56:  PR[53,3:*]=20    ;
  57:   ;
  58:  PR[54:*]=PR[49:Zero XYZ FUT]    ;
  59:  PR[54,1:*]=0    ;
  60:  PR[54,2:*]=1400    ;
  61:  PR[54,3:*]=300    ;
  62:  PR[54,7:*]=(-600)    ;
  63:   ;
  64:  ! Sblocco il pallet ------------- ;
  65:  CALL CMD_SBL_PLT_M3    ;
  66:  !------------------------------ ;
  67:  COL DETECT ON ;
  68:  COL GUARD ADJUST 120 ;
  69:   ;
  70:L PR[12:POS_MU] 1000mm/sec CNT50 Offset,PR[54:*]    ;
  71:L PR[12:POS_MU] 1000mm/sec CNT20 Offset,PR[51:*]    ;
  72:L PR[12:POS_MU] 600mm/sec CNT1 Offset,PR[52:*]    ;
  73:   ;
  74:  ! Punto finale ----------------- ;
  75:L PR[12:POS_MU] 100mm/sec FINE    ;
  76:   ;
  77:  CALL P1_APRI    ;
  78:  !------------------------------- ;
  79:   ;
  80:L PR[12:POS_MU] 600mm/sec FINE Offset,PR[53:*]    ;
  81:   ;
  82:  !------------------------------- ;
  83:  !Blocco il pallet ;
  84:  WAIT RI[3:PINZA AP.]=ON    ;
  85:  CALL CMD_BL_PLT_M3    ;
  86:  COL DETECT OFF ;
  87:   ;
  88:  !------------------------------- ;
  89:L PR[12:POS_MU] 600mm/sec CNT10 Offset,PR[52:*]    ;
  90:L PR[12:POS_MU] 1000mm/sec CNT50 Offset,PR[51:*]    ;
  91:L PR[12:POS_MU] 1000mm/sec CNT50 Offset,PR[54:*]    ;
  92:   ;
  93:  WAIT RI[3:PINZA AP.]=ON AND RI[4:PINZA CH]=OFF    ;
  94:L P[3] 1500mm/sec CNT50    ;
  95:   ;
  96:  !FINE CARICO ------------- ;
  97:  DO[165:RBT In Auto.MU3]=ON ;
  98:  WAIT DO[165:RBT In Auto.MU3]=ON    ;
  99:  WAIT    .50(sec) ;
 100:  DO[169:AP.PT.Autom.MU3]=OFF ;
 101:  DO[170:CH.PT.Autom.MU3]=ON ;
 102:  DO[154:Fin.Cari.PLT MU3]=PULSE,2.0sec ;
 103:  R[6:Strobe]=3    ;
 104:  !------------------------------ ;
 105:   ;
 106:J P[2] 100% CNT50    ;
 107:J P[1] 100% CNT50    ;
 108:   ;
/POS
P[1]{
   GP1:
	UF : 8, UT : 1,	
	J1=     -.001 deg,	J2=   -54.999 deg,	J3=    24.998 deg,
	J4=      .000 deg,	J5=   -25.000 deg,	J6=   -90.000 deg,
	E1=  5500.000  mm
};
P[2]{
   GP1:
	UF : 8, UT : 1,	
	J1=   -16.288 deg,	J2=   -29.147 deg,	J3=     3.450 deg,
	J4=    76.255 deg,	J5=   -14.249 deg,	J6=  -165.837 deg,
	E1=  5500.000  mm
};
P[3]{
   GP1:
	UF : 8, UT : 1,		CONFIG : 'N U T, 0, 0, 0',
	X =   185.503  mm,	Y =  1748.104  mm,	Z =   856.003  mm,
	W =     -.000 deg,	P =      .000 deg,	R =   -89.998 deg,
	E1=  5900.000  mm
};
/END
