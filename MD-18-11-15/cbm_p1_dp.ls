/PROG  CBM_P1_DP
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "CAMBIO P1 DEPO";
PROG_SIZE	= 2585;
CREATE		= DATE 13-06-06  TIME 00:56:10;
MODIFIED	= DATE 15-10-29  TIME 12:15:04;
FILE_NAME	= CBM_P3_D;
VERSION		= 0;
LINE_COUNT	= 107;
MEMORY_SIZE	= 3061;
PROTECT		= READ;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= 1,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/APPL
/MN
   1:  !------------------------------- ;
   2:  !DEPO PINZA PLT ;
   3:  !------------------------------- ;
   4:  IF DI[58:Pres.Pz.PLT Mag]=ON,JMP LBL[1] ;
   5:   ;
   6:  LBL[31] ;
   7:  !Controllo PLT su pinza ;
   8:  IF DI[59:Pres.Pz.UT. Mag]=ON AND DI[58:Pres.Pz.PLT Mag]=OFF,JMP LBL[30] ;
   9:  CALL MESSAGGI(24) ;
  10:  PAUSE ;
  11:  R[9:Message Allarmi]=0    ;
  12:  CALL MESSAGGI(1) ;
  13:  JMP LBL[31] ;
  14:  LBL[30] ;
  15:   ;
  16:  LBL[20] ;
  17:  !Controllo PLT su pinza ;
  18:  IF RI[3:PINZA AP.]=ON,JMP LBL[10] ;
  19:  CALL MESSAGGI(16) ;
  20:  PAUSE ;
  21:  R[9:Message Allarmi]=0    ;
  22:  CALL MESSAGGI(1) ;
  23:  JMP LBL[20] ;
  24:  LBL[10] ;
  25:   ;
  26:  CALL AZZERA_CMD    ;
  27:   ;
  28:  UFRAME_NUM=0 ;
  29:  UTOOL_NUM=0 ;
  30:   ;
  31:  ! Verifico stato PINZA ;
  32:  WAIT DI[58:Pres.Pz.PLT Mag]=OFF    ;
  33:   ;
  34:J P[1] 100% CNT50    ;
  35:J P[2] 100% CNT50    ;
  36:  !------------------------------- ;
  37:  !Calcolo Posizione ;
  38:  !------------------------------- ;
  39:  PR[51:*]=PR[49:Zero XYZ FUT]    ;
  40:  PR[51,1:*]=(-300)    ;
  41:  PR[51,2:*]=(-400)    ;
  42:  PR[51,3:*]=0    ;
  43:  PR[51,4:*]=(-35)    ;
  44:  PR[51,5:*]=(-90)    ;
  45:   ;
  46:  PR[52:*]=PR[49:Zero XYZ FUT]    ;
  47:  PR[52,1:*]=0    ;
  48:  PR[52,2:*]=0    ;
  49:  PR[52,3:*]=40    ;
  50:   ;
  51:  PR[53:*]=PR[49:Zero XYZ FUT]    ;
  52:  PR[53,1:*]=0    ;
  53:  PR[53,2:*]=(-80)    ;
  54:  PR[53,3:*]=0    ;
  55:   ;
  56:  PR[54:*]=PR[49:Zero XYZ FUT]    ;
  57:  PR[54,1:*]=0    ;
  58:  PR[54,2:*]=(-700)    ;
  59:  PR[54,3:*]=200    ;
  60:  PR[54,4:*]=0    ;
  61:   ;
  62:  PR[55:*]=PR[49:Zero XYZ FUT]    ;
  63:  PR[55,1:*]=0    ;
  64:  PR[55,2:*]=(-50)    ;
  65:  PR[55,3:*]=100    ;
  66:  PR[55,4:*]=0    ;
  67:   ;
  68:  PR[56:*]=PR[49:Zero XYZ FUT]    ;
  69:  PR[56,1:*]=0    ;
  70:  PR[56,2:*]=(-5)    ;
  71:  PR[56,3:*]=0    ;
  72:   ;
  73:  WAIT DI[58:Pres.Pz.PLT Mag]=OFF    ;
  74:L P[3:PINZA PLT] 1000mm/sec CNT50 Offset,PR[54:*]    ;
  75:L P[3:PINZA PLT] 600mm/sec FINE Offset,PR[55:*]    ;
  76:   ;
  77:L P[3:PINZA PLT] 600mm/sec FINE Offset,PR[52:*]    ;
  78:  !------------------------------- ;
  79:  COL DETECT ON ;
  80:  COL GUARD ADJUST 120 ;
  81:   ;
  82:  ! Punto finale ----------------- ;
  83:L P[3:PINZA PLT] 100mm/sec FINE    ;
  84:  !------------------------------- ;
  85:  !SBlocco la pinza ;
  86:  RO[1:CBM.PZ]=ON ;
  87:  WAIT RI[1:CBM.ACC]=ON    ;
  88:  COL DETECT OFF ;
  89:  WAIT    .50(sec) ;
  90:  !------------------------------- ;
  91:   ;
  92:L P[3:PINZA PLT] 50mm/sec FINE Offset,PR[56:*]    ;
  93:   ;
  94:  !------------------------------- ;
  95:  !CNT.PRESENZA PINZA ;
  96:  WAIT RI[1:CBM.ACC]=ON AND RI[3:PINZA AP.]=OFF    ;
  97:  RO[3:AP PZ]=OFF ;
  98:  RO[4:CH.PZ]=OFF ;
  99:  !------------------------------- ;
 100:L P[3:PINZA PLT] 600mm/sec CNT1 Offset,PR[53:*]    ;
 101:L P[3:PINZA PLT] 1000mm/sec CNT50 Offset,PR[51:*]    ;
 102:   ;
 103:  IF R[5:Tipo di pinza]<>0,JMP LBL[1] ;
 104:   ;
 105:L P[2] 1000mm/sec CNT100    ;
 106:J P[1] 100% CNT50    ;
 107:  LBL[1] ;
/POS
P[1]{
   GP1:
	UF : 0, UT : 0,	
	J1=     0.000 deg,	J2=   -55.000 deg,	J3=    25.000 deg,
	J4=      .000 deg,	J5=   -25.000 deg,	J6=   -90.000 deg,
	E1=  8000.000  mm
};
P[2]{
   GP1:
	UF : 0, UT : 0,		CONFIG : 'F U T, 0, 0, -1',
	X =  9191.682  mm,	Y =    -5.945  mm,	Z =  1356.951  mm,
	W =   -89.896 deg,	P =     -.498 deg,	R =     -.054 deg,
	E1=  8000.000  mm
};
P[3:"PINZA PLT"]{
   GP1:
	UF : 0, UT : 0,		CONFIG : 'F U T, 0, 0, 0',
	X = 10089.709  mm,	Y =   755.207  mm,	Z =   142.500  mm,
	W =   -89.896 deg,	P =     -.498 deg,	R =     -.054 deg,
	E1=  8000.000  mm
};
/END
