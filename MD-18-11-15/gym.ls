/PROG  GYM
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "";
PROG_SIZE	= 1233;
CREATE		= DATE 15-04-24  TIME 09:47:16;
MODIFIED	= DATE 15-04-24  TIME 17:13:10;
FILE_NAME	= ;
VERSION		= 0;
LINE_COUNT	= 20;
MEMORY_SIZE	= 1657;
PROTECT		= READ_WRITE;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= 1,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/APPL

AUTO_SINGULARITY_HEADER;
  ENABLE_SINGULARITY_AVOIDANCE   : TRUE;
/MN
   1:J P[1:Home] 100% FINE    ;
   2:  LBL[1] ;
   3:J P[2] 100% FINE    ;
   4:J P[3] 100% FINE    ;
   5:  JMP LBL[1] ;
   6:J P[4:Master] 100% FINE    ;
   7:  LBL[2] ;
   8:J P[5] 100% FINE    ;
   9:J P[6] 100% FINE    ;
  10:  JMP LBL[2] ;
  11:J P[7] 100% FINE    ;
  12:J P[8] 100% FINE    ;
  13:  LBL[3] ;
  14:J P[9] 100% FINE    ;
  15:J P[10] 100% FINE    ;
  16:  JMP LBL[3] ;
  17:  LBL[4] ;
  18:J P[11] 100% FINE    ;
  19:J P[12] 100% FINE    ;
  20:  JMP LBL[4] ;
/POS
P[1:"Home"]{
   GP1:
	UF : 0, UT : 0,	
	J1=    -1.405 deg,	J2=   -56.181 deg,	J3=    -2.015 deg,
	J4=     -.147 deg,	J5=   -82.776 deg,	J6=   -18.317 deg,
	E1=  3238.641  mm
};
P[2]{
   GP1:
	UF : 0, UT : 0,		CONFIG : 'N U T, 0, 0, 0',
	X =  3617.072  mm,	Y =   -25.950  mm,	Z =   551.563  mm,
	W =  -178.219 deg,	P =    -4.897 deg,	R =   -19.810 deg,
	E1=  2537.074  mm
};
P[3]{
   GP1:
	UF : 0, UT : 0,		CONFIG : 'N U T, 0, 0, 0',
	X =  7327.285  mm,	Y =   -25.950  mm,	Z =   551.563  mm,
	W =  -178.219 deg,	P =    -4.897 deg,	R =   -19.810 deg,
	E1=  6247.287  mm
};
P[4:"Master"]{
   GP1:
	UF : 0, UT : 0,	
	J1=    -1.405 deg,	J2=   -56.168 deg,	J3=    -2.040 deg,
	J4=     -.147 deg,	J5=   -82.777 deg,	J6=   -18.317 deg,
	E1=  5937.196  mm
};
P[5]{
   GP1:
	UF : 0, UT : 0,	
	J1=    -2.334 deg,	J2=   -56.167 deg,	J3=    -2.043 deg,
	J4=     -.147 deg,	J5=   -82.777 deg,	J6=   -18.318 deg,
	E1=  7544.625  mm
};
P[6]{
   GP1:
	UF : 0, UT : 0,	
	J1=    -2.334 deg,	J2=   -56.167 deg,	J3=    -2.043 deg,
	J4=     -.147 deg,	J5=   -82.778 deg,	J6=   -18.318 deg,
	E1=  3918.582  mm
};
P[7]{
   GP1:
	UF : 0, UT : 0,	
	J1=    -2.334 deg,	J2=   -56.166 deg,	J3=    -2.045 deg,
	J4=     -.147 deg,	J5=   -82.778 deg,	J6=   -18.318 deg,
	E1=  3807.392  mm
};
P[8]{
   GP1:
	UF : 0, UT : 0,	
	J1=    -2.334 deg,	J2=   -56.166 deg,	J3=    -2.045 deg,
	J4=     -.147 deg,	J5=   -82.778 deg,	J6=   -18.318 deg,
	E1=  4807.392  mm
};
P[9]{
   GP1:
	UF : 0, UT : 0,	
	J1=    -2.335 deg,	J2=   -56.164 deg,	J3=    -2.049 deg,
	J4=     -.147 deg,	J5=   -82.778 deg,	J6=   -18.318 deg,
	E1=  5124.049  mm
};
P[10]{
   GP1:
	UF : 0, UT : 0,	
	J1=    -2.335 deg,	J2=   -56.164 deg,	J3=    -2.049 deg,
	J4=     -.147 deg,	J5=   -82.778 deg,	J6=   -18.318 deg,
	E1=  8124.000  mm
};
P[11]{
   GP1:
	UF : 0, UT : 0,	
	J1=    -2.335 deg,	J2=   -56.161 deg,	J3=    -2.056 deg,
	J4=     -.148 deg,	J5=   -82.779 deg,	J6=   -18.319 deg,
	E1=   430.875  mm
};
P[12]{
   GP1:
	UF : 0, UT : 0,	
	J1=    -2.335 deg,	J2=   -56.161 deg,	J3=    -2.056 deg,
	J4=     -.148 deg,	J5=   -82.779 deg,	J6=   -18.319 deg,
	E1=  5000.000  mm
};
/END
