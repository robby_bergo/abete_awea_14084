/PROG  STZ_1B_PREL
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "Prel.Staz.1B ";
PROG_SIZE	= 2507;
CREATE		= DATE 13-07-15  TIME 19:33:54;
MODIFIED	= DATE 15-10-21  TIME 12:00:10;
FILE_NAME	= STZ_2_P3;
VERSION		= 0;
LINE_COUNT	= 98;
MEMORY_SIZE	= 3059;
PROTECT		= READ;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= 1,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/APPL
/MN
   1:  !------------------------------- ;
   2:  !Avvicinamento ai punti di presa ;
   3:  !------------------------------- ;
   4:  UFRAME_NUM=6 ;
   5:  UTOOL_NUM=1 ;
   6:   ;
   7:  LBL[31] ;
   8:  !Controllo PLT su pinza ;
   9:  IF DI[59:Pres.Pz.UT. Mag]=ON AND DI[58:Pres.Pz.PLT Mag]=OFF,JMP LBL[30] ;
  10:  CALL MESSAGGI(24) ;
  11:  PAUSE ;
  12:  R[9:Message Allarmi]=0    ;
  13:  CALL MESSAGGI(1) ;
  14:  JMP LBL[31] ;
  15:  LBL[30] ;
  16:   ;
  17:  CALL USER_6_ST1    ;
  18:   ;
  19:  LBL[20] ;
  20:  !Controllo pinza AP. ;
  21:  IF RI[3:PINZA AP.]=ON AND RI[4:PINZA CH]=OFF,JMP LBL[10] ;
  22:  CALL MESSAGGI(16) ;
  23:  PAUSE ;
  24:  R[9:Message Allarmi]=0    ;
  25:  CALL MESSAGGI(1) ;
  26:  JMP LBL[20] ;
  27:  LBL[10] ;
  28:   ;
  29:  !------------------------------- ;
  30:J P[1] 80% CNT50    ;
  31:J P[2] 80% CNT50    ;
  32:J P[3] 80% CNT50    ;
  33:  !------------------------------- ;
  34:  !ATTESA ENTRARE ;
  35:  WAIT DI[64:TAPPARE.CH-1]=ON AND DI[67:OK ENTRA St1-C2]=ON AND DI[68:PLT Pres.St-1-C2]=ON    ;
  36:  WAIT    .50(sec) ;
  37:   ;
  38:  DO[68:RICH.DEPO.BAIA-1B]=OFF ;
  39:  DO[67:RICH.PREL BAIA-1B]=ON ;
  40:  DO[64:RBT Fuor.ing.St1]=OFF ;
  41:   ;
  42:  !------------------------------- ;
  43:  !Prelievo pezzo ;
  44:  !------------------------------- ;
  45:  PR[8:POS_STAZ.PR]=PR[49:Zero XYZ FUT]    ;
  46:  PR[8,1:POS_STAZ.PR]=R[30:OFFSET_X]+0    ;
  47:  PR[8,2:POS_STAZ.PR]=R[31:OFFSET_Y]+0    ;
  48:  PR[8,3:POS_STAZ.PR]=0-R[32:OFFSET_Z]    ;
  49:  PR[8,4:POS_STAZ.PR]=0    ;
  50:  PR[8,5:POS_STAZ.PR]=0    ;
  51:  PR[8,6:POS_STAZ.PR]=(-90.004)    ;
  52:  PR[8,7:POS_STAZ.PR]=7000    ;
  53:   ;
  54:  PR[50:*]=PR[47:Zero XYZ_NUT]    ;
  55:  PR[50,2:*]=650    ;
  56:  PR[50,3:*]=100    ;
  57:   ;
  58:  PR[51:*]=PR[47:Zero XYZ_NUT]    ;
  59:  PR[51,3:*]=50    ;
  60:   ;
  61:  PR[52:*]=PR[47:Zero XYZ_NUT]    ;
  62:  PR[52,2:*]=20    ;
  63:  PR[52,3:*]=20    ;
  64:   ;
  65:  PR[53:*]=PR[47:Zero XYZ_NUT]    ;
  66:  PR[53,2:*]=650    ;
  67:  PR[53,3:*]=100    ;
  68:   ;
  69:  !------------------------------- ;
  70:L PR[8:POS_STAZ.PR] 1000mm/sec CNT2 Offset,PR[53:*]    ;
  71:L PR[8:POS_STAZ.PR] 600mm/sec CNT2 Offset,PR[52:*]    ;
  72:   ;
  73:  !------------------------------- ;
  74:  !PUNTO FINALE ;
  75:L PR[8:POS_STAZ.PR] 100mm/sec FINE    ;
  76:  CALL P1_CHIUD    ;
  77:  !------------------------------- ;
  78:L PR[8:POS_STAZ.PR] 500mm/sec FINE Offset,PR[51:*]    ;
  79:   ;
  80:  !ATTESA PRESENZA PEZZO ;
  81:  $WAITTMOUT=100 ;
  82:  WAIT RI[4:PINZA CH]=OFF TIMEOUT,LBL[2] ;
  83:  LBL[2] ;
  84:  $WAITTMOUT=3000 ;
  85:   ;
  86:  WAIT RI[3:PINZA AP.]=OFF AND RI[4:PINZA CH]=OFF    ;
  87:  !------------------------------- ;
  88:L PR[8:POS_STAZ.PR] 1000mm/sec CNT100 Offset,PR[50:*]    ;
  89:   ;
  90:  !------------------------------- ;
  91:  DO[67:RICH.PREL BAIA-1B]=OFF ;
  92:  DO[64:RBT Fuor.ing.St1]=ON ;
  93:   ;
  94:J P[3] 80% CNT50    ;
  95:J P[2] 80% CNT50    ;
  96:J P[1] 80% CNT50    ;
  97:   ;
  98:   ;
/POS
P[1]{
   GP1:
	UF : 6, UT : 1,	
	J1=     0.000 deg,	J2=   -55.000 deg,	J3=    25.000 deg,
	J4=     0.000 deg,	J5=   -25.000 deg,	J6=   -90.000 deg,
	E1=  7000.000  mm
};
P[2]{
   GP1:
	UF : 6, UT : 1,	
	J1=     1.000 deg,	J2=   -27.334 deg,	J3=    -1.515 deg,
	J4=   -84.120 deg,	J5=    20.527 deg,	J6=    -4.879 deg,
	E1=  7000.000  mm
};
P[3]{
   GP1:
	UF : 6, UT : 1,	
	J1=    -7.011 deg,	J2=   -17.715 deg,	J3=    -2.557 deg,
	J4=   -89.533 deg,	J5=    83.482 deg,	J6=    -2.773 deg,
	E1=  7000.000  mm
};
/END
