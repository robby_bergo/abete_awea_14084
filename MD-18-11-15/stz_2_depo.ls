/PROG  STZ_2_DEPO
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "Depo.Staz.2 ";
PROG_SIZE	= 2231;
CREATE		= DATE 13-07-15  TIME 19:33:46;
MODIFIED	= DATE 15-10-22  TIME 18:22:20;
FILE_NAME	= STZ_2_P3;
VERSION		= 0;
LINE_COUNT	= 83;
MEMORY_SIZE	= 2715;
PROTECT		= READ;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= 1,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/APPL
/MN
   1:  !------------------------------- ;
   2:  !Avvicinamento ai punti di presa ;
   3:  !------------------------------- ;
   4:  UFRAME_NUM=6 ;
   5:  UTOOL_NUM=1 ;
   6:   ;
   7:  LBL[31] ;
   8:  !Controllo PLT su pinza ;
   9:  IF DI[59:Pres.Pz.UT. Mag]=ON AND DI[58:Pres.Pz.PLT Mag]=OFF,JMP LBL[30] ;
  10:  CALL MESSAGGI(24) ;
  11:  PAUSE ;
  12:  R[9:Message Allarmi]=0    ;
  13:  CALL MESSAGGI(1) ;
  14:  JMP LBL[31] ;
  15:  LBL[30] ;
  16:   ;
  17:  CALL USER_6_ST2    ;
  18:   ;
  19:  !------------------------------- ;
  20:J P[1] 80% CNT50    ;
  21:J P[2] 80% CNT50    ;
  22:J P[3] 50% CNT50    ;
  23:  !------------------------------- ;
  24:  !ATTESA ENTRARE ;
  25:  DO[75:RICH.DEPO.BAIA2 ]=ON ;
  26:  WAIT DI[73:OK ENTRARE Staz2]=ON AND DI[75:PLT Pres.Staz2]=OFF    ;
  27:  DO[73:RBT Fuor.ing.St2]=OFF ;
  28:  !------------------------------- ;
  29:  !Prelievo pezzo ;
  30:  !------------------------------- ;
  31:  PR[9:POS_STAZ.DP]=PR[49:Zero XYZ FUT]    ;
  32:  PR[9,1:POS_STAZ.DP]=R[30:OFFSET_X]+0    ;
  33:  PR[9,2:POS_STAZ.DP]=R[31:OFFSET_Y]+0    ;
  34:  PR[9,3:POS_STAZ.DP]=0-R[32:OFFSET_Z]    ;
  35:  PR[9,4:POS_STAZ.DP]=(-.191)    ;
  36:  PR[9,5:POS_STAZ.DP]=(-.106)    ;
  37:  PR[9,6:POS_STAZ.DP]=(-89.657)    ;
  38:  PR[9,7:POS_STAZ.DP]=9000    ;
  39:   ;
  40:   ;
  41:  PR[50:*]=PR[47:Zero XYZ_NUT]    ;
  42:  PR[50,2:*]=900    ;
  43:  PR[50,3:*]=200    ;
  44:   ;
  45:  PR[51:*]=PR[47:Zero XYZ_NUT]    ;
  46:  PR[51,3:*]=100    ;
  47:   ;
  48:  PR[52:*]=PR[47:Zero XYZ_NUT]    ;
  49:  PR[52,2:*]=10    ;
  50:   ;
  51:  PR[53:*]=PR[47:Zero XYZ_NUT]    ;
  52:  PR[53,2:*]=800    ;
  53:  PR[53,3:*]=10    ;
  54:   ;
  55:  !------------------------------- ;
  56:L PR[9:POS_STAZ.DP] 1000mm/sec CNT2 Offset,PR[50:*]    ;
  57:L PR[9:POS_STAZ.DP] 600mm/sec CNT2 Offset,PR[51:*]    ;
  58:   ;
  59:  !------------------------------- ;
  60:  !PUNTO FINALE ;
  61:L PR[9:POS_STAZ.DP] 100mm/sec FINE    ;
  62:  !------------------------------- ;
  63:  CALL P1_APRI    ;
  64:L PR[9:POS_STAZ.DP] 500mm/sec FINE Offset,PR[52:*]    ;
  65:   ;
  66:  WAIT RI[3:PINZA AP.]=ON    ;
  67:   ;
  68:L PR[9:POS_STAZ.DP] 1000mm/sec CNT100 Offset,PR[53:*]    ;
  69:   ;
  70:  !------------------------------- ;
  71:  WAIT RI[3:PINZA AP.]=ON AND RI[4:PINZA CH]=OFF    ;
  72:  DO[75:RICH.DEPO.BAIA2 ]=OFF ;
  73:  DO[73:RBT Fuor.ing.St2]=ON ;
  74:J P[3] 100% CNT50    ;
  75:J P[2] 100% CNT50    ;
  76:J P[1] 100% CNT50    ;
  77:   ;
  78:  $WAITTMOUT=500 ;
  79:  WAIT DI[76:TAPPARE.CH-2]=ON TIMEOUT,LBL[100] ;
  80:  LBL[100] ;
  81:  $WAITTMOUT=3000 ;
  82:   ;
  83:   ;
/POS
P[1]{
   GP1:
	UF : 6, UT : 1,	
	J1=     0.000 deg,	J2=   -55.000 deg,	J3=    25.000 deg,
	J4=      .000 deg,	J5=   -25.000 deg,	J6=   -90.000 deg,
	E1=  9000.001  mm
};
P[2]{
   GP1:
	UF : 6, UT : 1,	
	J1=      .892 deg,	J2=   -27.834 deg,	J3=    -5.554 deg,
	J4=   -73.640 deg,	J5=    21.203 deg,	J6=   -16.082 deg,
	E1=  8999.999  mm
};
P[3]{
   GP1:
	UF : 6, UT : 1,		CONFIG : 'F U T, 0, 0, 0',
	X =    -5.141  mm,	Y =  1090.862  mm,	Z =   427.005  mm,
	W =     -.191 deg,	P =     -.105 deg,	R =   -89.658 deg,
	E1=  9000.001  mm
};
/END
