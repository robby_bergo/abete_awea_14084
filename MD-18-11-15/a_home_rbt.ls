/PROG  A_HOME_RBT
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "HOME_ROBOT";
PROG_SIZE	= 1268;
CREATE		= DATE 15-05-27  TIME 19:02:22;
MODIFIED	= DATE 15-05-29  TIME 12:17:04;
FILE_NAME	= HOME_RBT;
VERSION		= 0;
LINE_COUNT	= 46;
MEMORY_SIZE	= 1716;
PROTECT		= READ;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= 1,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/APPL
/MN
   1:  UFRAME_NUM=0 ;
   2:  UTOOL_NUM=0 ;
   3:   ;
   4:  PR[99:RIC_SIS]=JPOS    ;
   5:  R[98:ACQ_SIS_1]=PR[99,1:RIC_SIS]    ;
   6:  R[99:ACQ_SIS_7]=PR[99,7:RIC_SIS]    ;
   7:  IF R[99:ACQ_SIS_7]>2000 AND R[99:ACQ_SIS_7]<4000,JMP LBL[1] ;
   8:   ;
   9:  IF R[99:ACQ_SIS_7]>8000 AND R[99:ACQ_SIS_7]<16000,JMP LBL[1] ;
  10:   ;
  11:  PR[100:HOME]=PR[48:Zero JOINT]    ;
  12:  IF R[98:ACQ_SIS_1]<(-90) OR R[98:ACQ_SIS_1]>181,JMP LBL[10] ;
  13:  PR[100,1:HOME]=0    ;
  14:  JMP LBL[11] ;
  15:  LBL[10] ;
  16:  IF R[98:ACQ_SIS_1]>(-90) OR R[98:ACQ_SIS_1]<(-181),JMP LBL[11] ;
  17:  PR[100,1:HOME]=(-180)    ;
  18:  LBL[11] ;
  19:   ;
  20:  PR[100,2:HOME]=(-55)    ;
  21:  PR[100,3:HOME]=25    ;
  22:  PR[100,4:HOME]=0    ;
  23:  PR[100,5:HOME]=(-25)    ;
  24:  PR[100,6:HOME]=(-90)    ;
  25:  PR[100,7:HOME]=R[99:ACQ_SIS_7]    ;
  26:   ;
  27:J PR[100:HOME] 20% FINE ACC30    ;
  28:   ;
  29:  JMP LBL[2] ;
  30:   ;
  31:  LBL[1] ;
  32:   ;
  33:  IF R[98:ACQ_SIS_1]<(-45) OR R[98:ACQ_SIS_1]>181,JMP LBL[20] ;
  34:   ;
  35:J P[1] 20% FINE ACC30    ;
  36:  JMP LBL[2] ;
  37:  LBL[20] ;
  38:   ;
  39:  IF R[98:ACQ_SIS_1]>(-45) OR R[98:ACQ_SIS_1]<(-181),JMP LBL[11] ;
  40:   ;
  41:J P[2] 20% FINE ACC30    ;
  42:  JMP LBL[2] ;
  43:  LBL[30] ;
  44:   ;
  45:  LBL[2] ;
  46:   ;
/POS
P[1]{
   GP1:
	UF : 0, UT : 0,	
	J1=     0.000 deg,	J2=   -55.000 deg,	J3=    25.000 deg,
	J4=     -.000 deg,	J5=   -25.000 deg,	J6=   -90.000 deg,
	E1= 15000.000  mm
};
P[2]{
   GP1:
	UF : 0, UT : 0,	
	J1=  -180.000 deg,	J2=   -55.000 deg,	J3=    25.000 deg,
	J4=     -.000 deg,	J5=   -25.000 deg,	J6=   -90.000 deg,
	E1=  5000.000  mm
};
/END
