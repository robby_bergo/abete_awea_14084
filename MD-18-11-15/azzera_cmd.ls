/PROG  AZZERA_CMD
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "AZZERA";
PROG_SIZE	= 393;
CREATE		= DATE 15-05-27  TIME 13:55:46;
MODIFIED	= DATE 15-05-28  TIME 11:20:16;
FILE_NAME	= AZZERA;
VERSION		= 0;
LINE_COUNT	= 13;
MEMORY_SIZE	= 877;
PROTECT		= READ;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= 1,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/APPL

AUTO_SINGULARITY_HEADER;
  ENABLE_SINGULARITY_AVOIDANCE   : FALSE;
/MN
   1:   ;
   2:   ;
   3:  $WAITTMOUT=300 ;
   4:  RO[4:CH.PZ]=OFF ;
   5:  RO[3:AP PZ]=ON ;
   6:  WAIT RI[3:PINZA AP.]=ON TIMEOUT,LBL[1] ;
   7:  WAIT    .50(sec) ;
   8:   ;
   9:  LBL[1] ;
  10:  RO[3:AP PZ]=OFF ;
  11:  $WAITTMOUT=3000 ;
  12:   ;
  13:   ;
/POS
/END
