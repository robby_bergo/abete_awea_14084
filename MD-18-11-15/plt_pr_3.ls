/PROG  PLT_PR_3
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "Prel.MAG_3";
PROG_SIZE	= 2518;
CREATE		= DATE 15-10-13  TIME 18:34:02;
MODIFIED	= DATE 15-11-17  TIME 13:25:26;
FILE_NAME	= PLT_PR_2;
VERSION		= 0;
LINE_COUNT	= 98;
MEMORY_SIZE	= 3046;
PROTECT		= READ;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= 1,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/APPL

AUTO_SINGULARITY_HEADER;
  ENABLE_SINGULARITY_AVOIDANCE   : TRUE;
/MN
   1:  !------------------------------- ;
   2:  !Avvicinamento ai punti di presa ;
   3:  !------------------------------- ;
   4:  UFRAME_NUM=3 ;
   5:  UTOOL_NUM=1 ;
   6:   ;
   7:  LBL[31] ;
   8:  !Controllo PLT su pinza ;
   9:  IF DI[59:Pres.Pz.UT. Mag]=ON AND DI[58:Pres.Pz.PLT Mag]=OFF,JMP LBL[30] ;
  10:  CALL MESSAGGI(24) ;
  11:  PAUSE ;
  12:  R[9:Message Allarmi]=0    ;
  13:  CALL MESSAGGI(1) ;
  14:  JMP LBL[31] ;
  15:  LBL[30] ;
  16:   ;
  17:  LBL[20] ;
  18:  !Controllo PLT su pinza ;
  19:  IF RI[8]=OFF AND RI[3:PINZA AP.]=ON,JMP LBL[10] ;
  20:  CALL MESSAGGI(16) ;
  21:  PAUSE ;
  22:  R[9:Message Allarmi]=0    ;
  23:  CALL MESSAGGI(1) ;
  24:  JMP LBL[20] ;
  25:  LBL[10] ;
  26:   ;
  27:  CALL PLT3_XYZ    ;
  28:   ;
  29:J P[1] 80% CNT50    ;
  30:J P[2] 80% CNT50    ;
  31:J P[3] 80% CNT50    ;
  32:   ;
  33:  !------------------------------- ;
  34:  !Prelievo plt ;
  35:  !------------------------------- ;
  36:  R[20:Valore OFFSET X]=314.5*R[19:N'COLLONNE]    ;
  37:  R[21:Valore OFFSET Y]=640*R[18:N'FILE]    ;
  38:  PR[1:Prel.Dep MAGAZ.]=PR[49:Zero XYZ FUT]    ;
  39:  PR[1,1:Prel.Dep MAGAZ.]=R[20:Valore OFFSET X]+R[23:CORRET.IN.X]-R[30:OFFSET_X]    ;
  40:  PR[1,2:Prel.Dep MAGAZ.]=R[21:Valore OFFSET Y]+R[24:CORRET.IN.Y]-R[32:OFFSET_Z]    ;
  41:  PR[1,3:Prel.Dep MAGAZ.]=R[22:CORRET.IN.Z]+R[31:OFFSET_Y]    ;
  42:  PR[1,4:Prel.Dep MAGAZ.]=93.149    ;
  43:  PR[1,5:Prel.Dep MAGAZ.]=89.906    ;
  44:  PR[1,6:Prel.Dep MAGAZ.]=(-176.399)    ;
  45:  PR[1,7:Prel.Dep MAGAZ.]=8000+R[20:Valore OFFSET X]+R[23:CORRET.IN.X]    ;
  46:   ;
  47:  PR[50:*]=PR[49:Zero XYZ FUT]    ;
  48:  PR[50,1:*]=0    ;
  49:  PR[50,2:*]=5    ;
  50:  PR[50,3:*]=600    ;
  51:   ;
  52:  PR[51:*]=PR[49:Zero XYZ FUT]    ;
  53:  PR[51,2:*]=0    ;
  54:  PR[51,3:*]=300    ;
  55:   ;
  56:  PR[52:*]=PR[49:Zero XYZ FUT]    ;
  57:  PR[52,2:*]=10    ;
  58:   ;
  59:  PR[53:*]=PR[49:Zero XYZ FUT]    ;
  60:  PR[53,2:*]=10    ;
  61:  PR[53,3:*]=700    ;
  62:   ;
  63:  PR[54:*]=PR[49:Zero XYZ FUT]    ;
  64:  PR[54,1:*]=0    ;
  65:  PR[54,2:*]=(-345)    ;
  66:  PR[54,3:*]=600    ;
  67:  !------------------------------- ;
  68:L PR[1:Prel.Dep MAGAZ.] 1000mm/sec FINE Offset,PR[54:*]    ;
  69:  CALL CNT_PRES_PLT    ;
  70:   ;
  71:L PR[1:Prel.Dep MAGAZ.] 1000mm/sec CNT2 Offset,PR[50:*]    ;
  72:  IF R[132:PLT ASSEN IN POS]=1,JMP LBL[1] ;
  73:  !------------------------------ ;
  74:  COL DETECT ON ;
  75:  COL GUARD ADJUST 150 ;
  76:L PR[1:Prel.Dep MAGAZ.] 600mm/sec CNT2 Offset,PR[51:*]    ;
  77:  !------------------------------- ;
  78:  !PUNTO FINALE ;
  79:L PR[1:Prel.Dep MAGAZ.] 200mm/sec FINE    ;
  80:  COL DETECT OFF ;
  81:  !------------------------------- ;
  82:  !CHIUSURA PINZA ;
  83:  CALL P1_CHIUD    ;
  84:   ;
  85:L PR[1:Prel.Dep MAGAZ.] 200mm/sec FINE Offset,PR[52:*]    ;
  86:   ;
  87:  COL DETECT OFF ;
  88:  WAIT RI[4:PINZA CH]=OFF    ;
  89:   ;
  90:L PR[1:Prel.Dep MAGAZ.] 1000mm/sec CNT20 Offset,PR[53:*]    ;
  91:  WAIT RI[3:PINZA AP.]=OFF AND RI[4:PINZA CH]=OFF    ;
  92:  !------------------------------- ;
  93:   ;
  94:  LBL[1] ;
  95:L P[3] 1500mm/sec CNT50    ;
  96:J P[2] 80% CNT50    ;
  97:J P[1] 80% CNT50    ;
  98:   ;
/POS
P[1]{
   GP1:
	UF : 3, UT : 1,	
	J1=     -.000 deg,	J2=   -54.999 deg,	J3=    24.999 deg,
	J4=     -.000 deg,	J5=   -25.000 deg,	J6=   -90.000 deg,
	E1=  8000.000  mm
};
P[2]{
   GP1:
	UF : 3, UT : 1,	
	J1=    -8.942 deg,	J2=    -7.203 deg,	J3=    -8.202 deg,
	J4=    68.730 deg,	J5=    26.804 deg,	J6=  -159.143 deg,
	E1=  8000.000  mm
};
P[3]{
   GP1:
	UF : 3, UT : 1,		CONFIG : 'F U T, 0, 0, 0',
	X =   396.089  mm,	Y =   329.253  mm,	Z =   799.108  mm,
	W =    93.445 deg,	P =    89.906 deg,	R =  -176.462 deg,
	E1=  8000.000  mm
};
/END
