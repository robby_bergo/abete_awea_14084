/PROG  M1_AWEA850_SCAR
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "SCAR AWEA850 1";
PROG_SIZE	= 2912;
CREATE		= DATE 12-04-17  TIME 00:03:20;
MODIFIED	= DATE 15-10-28  TIME 20:12:04;
FILE_NAME	= ;
VERSION		= 0;
LINE_COUNT	= 120;
MEMORY_SIZE	= 3352;
PROTECT		= READ;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= 1,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/MN
   1:  !------------------------------- ;
   2:  !SCARICO la Macch M1_AWEA850 ;
   3:  !------------------------------- ;
   4:  UFRAME_NUM=8 ;
   5:  UTOOL_NUM=1 ;
   6:   ;
   7:  CALL USER_8_MU1    ;
   8:   ;
   9:  LBL[31] ;
  10:  !Controllo PLT su pinza ;
  11:  IF DI[59:Pres.Pz.UT. Mag]=ON AND DI[58:Pres.Pz.PLT Mag]=OFF,JMP LBL[30] ;
  12:  CALL MESSAGGI(24) ;
  13:  PAUSE ;
  14:  R[9:Message Allarmi]=0    ;
  15:  CALL MESSAGGI(1) ;
  16:  JMP LBL[31] ;
  17:  LBL[30] ;
  18:   ;
  19:  LBL[20] ;
  20:  !Controllo PLT su pinza ;
  21:  IF RI[3:PINZA AP.]=ON AND RI[4:PINZA CH]=OFF,JMP LBL[10] ;
  22:  CALL MESSAGGI(16) ;
  23:  PAUSE ;
  24:  R[9:Message Allarmi]=0    ;
  25:  CALL MESSAGGI(1) ;
  26:  JMP LBL[20] ;
  27:  LBL[10] ;
  28:   ;
  29:  !------------------------------ ;
  30:J P[1] 100% CNT50    ;
  31:J P[2] 100% CNT50    ;
  32:J P[3] 75% CNT50    ;
  33:L P[4] 1500mm/sec CNT50    ;
  34:   ;
  35:  DO[106:CH.PT.Autom.MU1]=OFF ;
  36:  DO[105:AP.PT.Autom.MU1]=ON ;
  37:  ! Verifico stato CNC ;
  38:  WAIT DI[89:Ch.Scar.PLT MU1]=ON AND DI[108:Porta Auto.AP.MU1]=ON    ;
  39:  DO[99:RBT Fuor.ing.MU1]=OFF ;
  40:   ;
  41:  !CNT Sblocco Del pallet --------- ;
  42:  IF DI[109:PLT Sblocc.MU1]=OFF,CALL CMD_SBL_PLT_M1 ;
  43:   ;
  44:  !------------------------------- ;
  45:  !Calcolo Pos. pezzo ;
  46:  !------------------------------- ;
  47:  PR[12:POS_MU]=PR[47:Zero XYZ_NUT]    ;
  48:  PR[12,1:POS_MU]=R[30:OFFSET_X]+0    ;
  49:  PR[12,2:POS_MU]=R[31:OFFSET_Y]+0    ;
  50:  PR[12,3:POS_MU]=0-R[32:OFFSET_Z]    ;
  51:  PR[12,4:POS_MU]=0    ;
  52:  PR[12,5:POS_MU]=0    ;
  53:  PR[12,6:POS_MU]=(-90.005)    ;
  54:  PR[12,7:POS_MU]=1000    ;
  55:   ;
  56:  PR[51:*]=PR[49:Zero XYZ FUT]    ;
  57:  PR[51,1:*]=0    ;
  58:  PR[51,2:*]=700    ;
  59:  PR[51,3:*]=200    ;
  60:   ;
  61:  PR[52:*]=PR[49:Zero XYZ FUT]    ;
  62:  PR[52,1:*]=0    ;
  63:  PR[52,2:*]=0    ;
  64:  PR[52,3:*]=150    ;
  65:   ;
  66:  PR[53:*]=PR[49:Zero XYZ FUT]    ;
  67:  PR[53,3:*]=20    ;
  68:   ;
  69:  PR[54:*]=PR[49:Zero XYZ FUT]    ;
  70:  PR[54,1:*]=0    ;
  71:  PR[54,2:*]=1400    ;
  72:  PR[54,3:*]=300    ;
  73:  PR[54,7:*]=600    ;
  74:   ;
  75:  ! Sblocco il pallet ------------- ;
  76:  CALL CMD_SBL_PLT_M1    ;
  77:  !------------------------------ ;
  78:  COL DETECT ON ;
  79:  COL GUARD ADJUST 120 ;
  80:   ;
  81:L PR[12:POS_MU] 1000mm/sec CNT50 Offset,PR[54:*]    ;
  82:L PR[12:POS_MU] 1000mm/sec CNT50 Offset,PR[51:*]    ;
  83:L PR[12:POS_MU] 600mm/sec CNT20 Offset,PR[52:*]    ;
  84:L PR[12:POS_MU] 300mm/sec CNT1 Offset,PR[53:*]    ;
  85:   ;
  86:  ! Punto finale ----------------- ;
  87:L PR[12:POS_MU] 100mm/sec FINE    ;
  88:   ;
  89:  CALL P1_CHIUD    ;
  90:  CALL CMD_SBL_PLT_M1    ;
  91:  !------------------------------- ;
  92:   ;
  93:L PR[12:POS_MU] 300mm/sec FINE Offset,PR[52:*]    ;
  94:   ;
  95:  !------------------------------- ;
  96:  !Blocco il pallet ;
  97:  WAIT RI[3:PINZA AP.]=OFF    ;
  98:  COL DETECT OFF ;
  99:   ;
 100:  !------------------------------- ;
 101:L PR[12:POS_MU] 600mm/sec FINE Offset,PR[51:*]    ;
 102:L PR[12:POS_MU] 1000mm/sec CNT50 Offset,PR[54:*]    ;
 103:   ;
 104:  WAIT RI[3:PINZA AP.]=OFF    ;
 105:L P[4] 1500mm/sec CNT50    ;
 106:J P[3] 75% CNT50    ;
 107:   ;
 108:  !FINE SCARICO ------------- ;
 109:  DO[99:RBT Fuor.ing.MU1]=ON ;
 110:  WAIT DO[99:RBT Fuor.ing.MU1]=ON    ;
 111:  WAIT    .50(sec) ;
 112:  DO[105:AP.PT.Autom.MU1]=OFF ;
 113:  DO[106:CH.PT.Autom.MU1]=ON ;
 114:  DO[89:Fin.Scar.PLT MU1]=PULSE,2.0sec ;
 115:  R[6:Strobe]=3    ;
 116:  !------------------------------ ;
 117:   ;
 118:J P[2] 80% CNT50    ;
 119:J P[1] 80% CNT50    ;
 120:   ;
/POS
P[1]{
   GP1:
	UF : 8, UT : 1,	
	J1=     -.001 deg,	J2=   -54.999 deg,	J3=    24.998 deg,
	J4=     0.000 deg,	J5=   -25.000 deg,	J6=   -90.000 deg,
	E1=   500.000  mm
};
P[2]{
   GP1:
	UF : 8, UT : 1,	
	J1=   -16.288 deg,	J2=   -29.147 deg,	J3=     3.450 deg,
	J4=    76.255 deg,	J5=   -14.249 deg,	J6=  -165.837 deg,
	E1=   500.000  mm
};
P[3]{
   GP1:
	UF : 8, UT : 1,		CONFIG : 'N U T, 0, 0, 0',
	X =  1220.789  mm,	Y =  1321.189  mm,	Z =  1200.292  mm,
	W =      .784 deg,	P =    -1.910 deg,	R =   -87.107 deg,
	E1=  1500.000  mm
};
P[4]{
   GP1:
	UF : 8, UT : 1,	
	J1=  -137.231 deg,	J2=   -48.959 deg,	J3=    16.334 deg,
	J4=   -59.469 deg,	J5=   -28.660 deg,	J6=   -34.306 deg,
	E1=  2000.000  mm
};
/END
