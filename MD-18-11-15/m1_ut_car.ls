/PROG  M1_UT_CAR
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "CAR UT 1";
PROG_SIZE	= 2957;
CREATE		= DATE 12-04-17  TIME 00:03:20;
MODIFIED	= DATE 15-11-18  TIME 20:47:48;
FILE_NAME	= ;
VERSION		= 0;
LINE_COUNT	= 122;
MEMORY_SIZE	= 3397;
PROTECT		= READ;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= 1,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/MN
   1:  !------------------------------- ;
   2:  !CARICO la Macch M1_AWEA850_UT ;
   3:  !------------------------------- ;
   4:  UFRAME_NUM=9 ;
   5:  UTOOL_NUM=2 ;
   6:   ;
   7:  CALL USER_9_UT1    ;
   8:   ;
   9:  LBL[31] ;
  10:  !Controllo PLT su pinza ;
  11:  IF DI[58:Pres.Pz.PLT Mag]=ON AND DI[59:Pres.Pz.UT. Mag]=OFF,JMP LBL[30] ;
  12:  CALL MESSAGGI(24) ;
  13:  PAUSE ;
  14:  R[9:Message Allarmi]=0    ;
  15:  CALL MESSAGGI(1) ;
  16:  JMP LBL[31] ;
  17:  LBL[30] ;
  18:   ;
  19:   ;
  20:  !------------------------------ ;
  21:J P[1] 80% CNT50    ;
  22:J P[2] 80% CNT50    ;
  23:L P[3] 1500mm/sec CNT50    ;
  24:   ;
  25:  DO[106:CH.PT.Autom.MU1]=OFF ;
  26:  DO[105:AP.PT.Autom.MU1]=ON ;
  27:  ! Verifico stato CNC ;
  28:  WAIT DI[92:Ch.Cari.UT MU1]=ON AND DI[108:Porta Auto.AP.MU1]=ON AND DI[101:UT.Orient.MU1]=ON    ;
  29:  DO[99:RBT Fuor.ing.MU1]=OFF ;
  30:   ;
  31:  !------------------------------- ;
  32:  !Calcolo Pos. pezzo ;
  33:  !------------------------------- ;
  34:  PR[12:POS_MU]=PR[47:Zero XYZ_NUT]    ;
  35:  PR[12,1:POS_MU]=0    ;
  36:  PR[12,2:POS_MU]=0    ;
  37:  PR[12,3:POS_MU]=0    ;
  38:  PR[12,4:POS_MU]=(-.003)    ;
  39:  PR[12,5:POS_MU]=.002    ;
  40:  PR[12,6:POS_MU]=(-85)    ;
  41:  PR[12,7:POS_MU]=200    ;
  42:   ;
  43:  PR[51:*]=PR[49:Zero XYZ FUT]    ;
  44:  PR[51,1:*]=(-700)    ;
  45:  PR[51,2:*]=50    ;
  46:  PR[51,6:*]=10    ;
  47:   ;
  48:  PR[52:*]=PR[49:Zero XYZ FUT]    ;
  49:  PR[52,1:*]=50    ;
  50:  PR[52,2:*]=100    ;
  51:  PR[52,3:*]=(-110)    ;
  52:  PR[52,6:*]=20    ;
  53:   ;
  54:  PR[53:*]=PR[49:Zero XYZ FUT]    ;
  55:  PR[53,3:*]=(-110)    ;
  56:   ;
  57:  PR[54:*]=PR[49:Zero XYZ FUT]    ;
  58:  PR[54,1:*]=(-1400)    ;
  59:  PR[54,2:*]=(-100)    ;
  60:  PR[54,6:*]=10    ;
  61:  PR[54,7:*]=600    ;
  62:   ;
  63:  PR[55:*]=PR[49:Zero XYZ FUT]    ;
  64:  PR[55,2:*]=100    ;
  65:   ;
  66:  PR[56:*]=PR[49:Zero XYZ FUT]    ;
  67:  PR[56,6:*]=.5    ;
  68:  PR[57]=PR[49:Zero XYZ FUT]    ;
  69:  PR[57,6]=(-.5)    ;
  70:  !------------------------------ ;
  71:  COL DETECT ON ;
  72:  COL GUARD ADJUST 120 ;
  73:   ;
  74:L PR[12:POS_MU] 1000mm/sec CNT50 Offset,PR[52:*] Tool_Offset,PR[54:*]    ;
  75:L PR[12:POS_MU] 1000mm/sec CNT20 Offset,PR[52:*] Tool_Offset,PR[51:*]    ;
  76:L PR[12:POS_MU] 600mm/sec CNT1 Offset,PR[52:*]    ;
  77:L PR[12:POS_MU] 300mm/sec CNT1 Offset,PR[53:*]    ;
  78:  !------------------------------ ;
  79:  ! Sblocco il pallet ------------- ;
  80:  CALL CMD_SBL_UT_M1    ;
  81:  !------------------------------ ;
  82:   ;
  83:  ! Punto finale ----------------- ;
  84:L PR[12:POS_MU] 100mm/sec FINE    ;
  85:L PR[12:POS_MU] 100mm/sec CNT1 Offset,PR[56:*]    ;
  86:L PR[12:POS_MU] 100mm/sec CNT1 Offset,PR[57]    ;
  87:   ;
  88:  !------------------------------- ;
  89:  !Blocco il pallet ;
  90:  !------------------------------- ;
  91:  !Blocco Uensile ;
  92:  CALL CMD_BL_UT_M1    ;
  93:   ;
  94:  CALL P1_APRI    ;
  95:  !------------------------------- ;
  96:   ;
  97:L PR[12:POS_MU] 600mm/sec FINE Offset,PR[55:*]    ;
  98:   ;
  99:  !------------------------------- ;
 100:  WAIT RI[3:PINZA AP.]=ON    ;
 101:  COL DETECT OFF ;
 102:   ;
 103:  !------------------------------- ;
 104:L PR[12:POS_MU] 1000mm/sec CNT50 Offset,PR[52:*] Tool_Offset,PR[51:*]    ;
 105:L PR[12:POS_MU] 1000mm/sec CNT50 Offset,PR[52:*] Tool_Offset,PR[54:*]    ;
 106:   ;
 107:  WAIT RI[3:PINZA AP.]=ON AND RI[4:PINZA CH]=OFF    ;
 108:L P[3] 1500mm/sec CNT50    ;
 109:   ;
 110:  !FINE CARICO ------------- ;
 111:  DO[99:RBT Fuor.ing.MU1]=ON ;
 112:  WAIT DO[99:RBT Fuor.ing.MU1]=ON    ;
 113:  WAIT    .50(sec) ;
 114:  DO[105:AP.PT.Autom.MU1]=OFF ;
 115:  DO[106:CH.PT.Autom.MU1]=ON ;
 116:  DO[92:Fin.Cari.UT MU1]=PULSE,2.0sec ;
 117:  R[6:Strobe]=3    ;
 118:  !------------------------------ ;
 119:   ;
 120:J P[2] 100% CNT50    ;
 121:J P[1] 100% CNT50    ;
 122:   ;
/POS
P[1]{
   GP1:
	UF : 9, UT : 2,	
	J1=      .000 deg,	J2=   -55.000 deg,	J3=    25.000 deg,
	J4=      .000 deg,	J5=   -25.000 deg,	J6=    90.000 deg,
	E1=   500.000  mm
};
P[2]{
   GP1:
	UF : 9, UT : 2,	
	J1=   -90.000 deg,	J2=   -55.000 deg,	J3=    25.000 deg,
	J4=      .000 deg,	J5=   -25.000 deg,	J6=    90.000 deg,
	E1=   542.648  mm
};
P[3]{
   GP1:
	UF : 9, UT : 2,	
	J1=  -128.236 deg,	J2=   -55.560 deg,	J3=    10.951 deg,
	J4=   -73.996 deg,	J5=   -35.037 deg,	J6=   160.587 deg,
	E1=  1000.000  mm
};
/END
