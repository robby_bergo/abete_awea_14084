/PROG  PLT_PR_5
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "Prel.MAG_1";
PROG_SIZE	= 2481;
CREATE		= DATE 15-10-14  TIME 19:08:02;
MODIFIED	= DATE 15-11-18  TIME 12:38:18;
FILE_NAME	= PLT_PR_1;
VERSION		= 0;
LINE_COUNT	= 100;
MEMORY_SIZE	= 3009;
PROTECT		= READ;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= 1,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/APPL

AUTO_SINGULARITY_HEADER;
  ENABLE_SINGULARITY_AVOIDANCE   : TRUE;
/MN
   1:  !------------------------------- ;
   2:  !Avvicinamento ai punti di presa ;
   3:  !------------------------------- ;
   4:  UFRAME_NUM=5 ;
   5:  UTOOL_NUM=2 ;
   6:   ;
   7:  LBL[31] ;
   8:  !Controllo PLT su pinza ;
   9:  IF DI[58:Pres.Pz.PLT Mag]=ON AND DI[59:Pres.Pz.UT. Mag]=OFF,JMP LBL[30] ;
  10:  CALL MESSAGGI(24) ;
  11:  PAUSE ;
  12:  R[9:Message Allarmi]=0    ;
  13:  CALL MESSAGGI(1) ;
  14:  JMP LBL[31] ;
  15:  LBL[30] ;
  16:   ;
  17:  LBL[20] ;
  18:  !Controllo PLT su pinza ;
  19:  IF RI[3:PINZA AP.]=ON AND RI[4:PINZA CH]=OFF,JMP LBL[10] ;
  20:  CALL MESSAGGI(16) ;
  21:  PAUSE ;
  22:  R[9:Message Allarmi]=0    ;
  23:  CALL MESSAGGI(1) ;
  24:  JMP LBL[20] ;
  25:  LBL[10] ;
  26:   ;
  27:  CALL PLT5_XYZ    ;
  28:   ;
  29:J P[1] 100% CNT50    ;
  30:J P[2] 80% CNT50    ;
  31:   ;
  32:  !------------------------------- ;
  33:  !Prelievo plt ;
  34:  !------------------------------- ;
  35:  R[20:Valore OFFSET X]=100*R[19:N'COLLONNE]    ;
  36:  R[21:Valore OFFSET Y]=400.5*R[18:N'FILE]    ;
  37:  PR[1:Prel.Dep MAGAZ.]=PR[49:Zero XYZ FUT]    ;
  38:  PR[1,1:Prel.Dep MAGAZ.]=R[20:Valore OFFSET X]+R[23:CORRET.IN.X]    ;
  39:  PR[1,2:Prel.Dep MAGAZ.]=R[21:Valore OFFSET Y]+R[24:CORRET.IN.Y]    ;
  40:  PR[1,3:Prel.Dep MAGAZ.]=R[22:CORRET.IN.Z]    ;
  41:  PR[1,4:Prel.Dep MAGAZ.]=128.991    ;
  42:  PR[1,5:Prel.Dep MAGAZ.]=89.493    ;
  43:  PR[1,6:Prel.Dep MAGAZ.]=(-140.832)    ;
  44:  PR[1,7:Prel.Dep MAGAZ.]=14200+R[20:Valore OFFSET X]+R[23:CORRET.IN.X]    ;
  45:   ;
  46:  PR[50:*]=PR[49:Zero XYZ FUT]    ;
  47:  PR[50,1:*]=0    ;
  48:  PR[50,2:*]=5    ;
  49:  PR[50,3:*]=300    ;
  50:   ;
  51:  PR[51:*]=PR[49:Zero XYZ FUT]    ;
  52:  PR[51,2:*]=0    ;
  53:  PR[51,3:*]=60    ;
  54:   ;
  55:  PR[52:*]=PR[49:Zero XYZ FUT]    ;
  56:  PR[52,3:*]=60    ;
  57:   ;
  58:  PR[54:*]=PR[49:Zero XYZ FUT]    ;
  59:  PR[54,1:*]=0    ;
  60:  PR[54,2:*]=300    ;
  61:  PR[54,3:*]=500    ;
  62:   ;
  63:  !------------------------------- ;
  64:L PR[1:Prel.Dep MAGAZ.] 1000mm/sec FINE Offset,PR[54:*]    ;
  65:  CALL CNT_PRES_CONO    ;
  66:   ;
  67:  IF R[131:CONO ASSE IN POS]=1,JMP LBL[1] ;
  68:  !------------------------------- ;
  69:L PR[1:Prel.Dep MAGAZ.] 1000mm/sec CNT2 Offset,PR[50:*]    ;
  70:L PR[1:Prel.Dep MAGAZ.] 600mm/sec CNT2 Offset,PR[51:*]    ;
  71:  !------------------------------ ;
  72:  COL DETECT ON ;
  73:  COL GUARD ADJUST 150 ;
  74:  !------------------------------- ;
  75:  !PUNTO FINALE ;
  76:L PR[1:Prel.Dep MAGAZ.] 100mm/sec FINE    ;
  77:  COL DETECT OFF ;
  78:  !------------------------------- ;
  79:  !CHIUSURA PINZA ;
  80:  CALL P1_CHIUD    ;
  81:   ;
  82:L PR[1:Prel.Dep MAGAZ.] 200mm/sec FINE Offset,PR[52:*]    ;
  83:   ;
  84:  COL DETECT OFF ;
  85:  WAIT RI[4:PINZA CH]=OFF    ;
  86:   ;
  87:L PR[1:Prel.Dep MAGAZ.] 1000mm/sec CNT20 Offset,PR[50:*]    ;
  88:  !------------------------------- ;
  89:  WAIT RI[3:PINZA AP.]=OFF AND RI[4:PINZA CH]=OFF    ;
  90:   ;
  91:  CALL RIP_UT    ;
  92:  LBL[1] ;
  93:  UFRAME_NUM=5 ;
  94:  UTOOL_NUM=2 ;
  95:   ;
  96:  JMP LBL[11] ;
  97:J P[2] 75% CNT50    ;
  98:  LBL[11] ;
  99:   ;
 100:J P[1] 80% CNT50    ;
/POS
P[1]{
   GP1:
	UF : 5, UT : 2,		CONFIG : 'N U T, 0, 0, 0',
	X =   228.205  mm,	Y =  2080.547  mm,	Z =   926.985  mm,
	W =   -89.810 deg,	P =      .046 deg,	R =      .079 deg,
	E1= 14199.986  mm
};
P[2]{
   GP1:
	UF : 5, UT : 2,	
	J1=    -5.738 deg,	J2=   -35.890 deg,	J3=    -3.081 deg,
	J4=    90.027 deg,	J5=    96.091 deg,	J6=     3.249 deg,
	E1= 14200.000  mm
};
/END
