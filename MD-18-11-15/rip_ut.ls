/PROG  RIP_UT
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "RIP_UTENSILE";
PROG_SIZE	= 2170;
CREATE		= DATE 15-11-18  TIME 12:27:48;
MODIFIED	= DATE 15-11-18  TIME 12:50:48;
FILE_NAME	= PLT_DP_5;
VERSION		= 0;
LINE_COUNT	= 74;
MEMORY_SIZE	= 2658;
PROTECT		= READ;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= 1,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/APPL

AUTO_SINGULARITY_HEADER;
  ENABLE_SINGULARITY_AVOIDANCE   : TRUE;
/MN
   1:  !------------------------------- ;
   2:  !Avvicinamento ai punti di presa ;
   3:  !------------------------------- ;
   4:  UFRAME_NUM=0 ;
   5:  UTOOL_NUM=2 ;
   6:   ;
   7:  LBL[20] ;
   8:  !Controllo PLT su pinza ;
   9:  IF DI[58:Pres.Pz.PLT Mag]=ON AND DI[59:Pres.Pz.UT. Mag]=OFF,JMP LBL[10] ;
  10:  CALL MESSAGGI(24) ;
  11:  PAUSE ;
  12:  R[9:Message Allarmi]=0    ;
  13:  CALL MESSAGGI(1) ;
  14:  JMP LBL[20] ;
  15:  LBL[10] ;
  16:   ;
  17:   ;
  18:  !------------------------------- ;
  19:  !CALCOLO  ASSE EXT ;
  20:  !------------------------------- ;
  21:  PR[99:RIC_SIS]=JPOS    ;
  22:  R[99:ACQ_SIS_7]=PR[99,7:RIC_SIS]    ;
  23:  PR[91:@@]=PR[90:ASSE EXT JOINT]    ;
  24:  PR[91,7:@@]=R[99:ACQ_SIS_7]    ;
  25:  !------------------------------- ;
  26:   ;
  27:  JMP LBL[12] ;
  28:J P[1] 80% CNT50 Offset,PR[91:@@]    ;
  29:  LBL[12] ;
  30:J P[2] 80% CNT50 Offset,PR[91:@@]    ;
  31:  !------------------------------- ;
  32:  !CALCOLO ;
  33:  !------------------------------- ;
  34:  PR[50:*]=PR[47:Zero XYZ_NUT]    ;
  35:  PR[50,1:*]=(-400)    ;
  36:  PR[50,3:*]=200    ;
  37:   ;
  38:  PR[51:*]=PR[47:Zero XYZ_NUT]    ;
  39:  PR[51,3:*]=20    ;
  40:   ;
  41:  PR[53:*]=PR[47:Zero XYZ_NUT]    ;
  42:  PR[53,1:*]=(-300)    ;
  43:  PR[53,3:*]=10    ;
  44:   ;
  45:  !------------------------------- ;
  46:J P[4] 50% FINE Offset,PR[91:@@] Tool_Offset,PR[50:*]    ;
  47:L P[4] 1000mm/sec CNT2 Offset,PR[91:@@] ACC40 Tool_Offset,PR[53:*]    ;
  48:  !------------------------------ ;
  49:  COL DETECT ON ;
  50:  COL GUARD ADJUST 150 ;
  51:L P[4] 500mm/sec CNT2 Offset,PR[91:@@] Tool_Offset,PR[51:*]    ;
  52:  !------------------------------- ;
  53:  !PUNTO FINALE ;
  54:L P[4] 100mm/sec FINE Offset,PR[91:@@]    ;
  55:  !------------------------------- ;
  56:  !Apre PINZA ;
  57:  COL DETECT OFF ;
  58:  CALL P1_APRI    ;
  59:  CALL P1_CHIUD    ;
  60:  !------------------------------ ;
  61:  COL DETECT OFF ;
  62:   ;
  63:L P[4] 200mm/sec FINE Offset,PR[91:@@] Tool_Offset,PR[51:*]    ;
  64:L P[4] 600mm/sec CNT20 Offset,PR[91:@@] Tool_Offset,PR[53:*]    ;
  65:L P[4] 1000mm/sec CNT20 Offset,PR[91:@@] Tool_Offset,PR[50:*]    ;
  66:   ;
  67:  WAIT RI[3:PINZA AP.]=OFF AND RI[4:PINZA CH]=OFF    ;
  68:   ;
  69:  !------------------------------- ;
  70:J P[2] 75% CNT50 Offset,PR[91:@@]    ;
  71:  JMP LBL[13] ;
  72:J P[1] 100% CNT100 Offset,PR[91:@@]    ;
  73:  LBL[13] ;
  74:   ;
/POS
P[1]{
   GP1:
	UF : 0, UT : 2,	
	J1=     0.000 deg,	J2=   -55.000 deg,	J3=    25.000 deg,
	J4=      .000 deg,	J5=   -25.000 deg,	J6=    90.000 deg,
	E1=     0.000  mm
};
P[2]{
   GP1:
	UF : 0, UT : 2,	
	J1=    12.944 deg,	J2=   -52.169 deg,	J3=   -18.679 deg,
	J4=   -96.241 deg,	J5=   106.877 deg,	J6=   160.507 deg,
	E1=     0.000  mm
};
P[4]{
   GP1:
	UF : 0, UT : 2,	
	J1=   -20.572 deg,	J2=    16.991 deg,	J3=   -69.101 deg,
	J4=  -158.712 deg,	J5=   109.263 deg,	J6=    97.604 deg,
	E1=     0.000  mm
};
/END
