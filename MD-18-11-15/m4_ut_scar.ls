/PROG  M4_UT_SCAR
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "SCAR UT 4";
PROG_SIZE	= 2949;
CREATE		= DATE 12-04-17  TIME 00:03:20;
MODIFIED	= DATE 15-10-28  TIME 20:43:06;
FILE_NAME	= ;
VERSION		= 0;
LINE_COUNT	= 122;
MEMORY_SIZE	= 3389;
PROTECT		= READ;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= 1,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/MN
   1:  !------------------------------- ;
   2:  !SCARICO la Macch M4_AWEA850_UT ;
   3:  !------------------------------- ;
   4:  UFRAME_NUM=9 ;
   5:  UTOOL_NUM=2 ;
   6:   ;
   7:  CALL USER_9_UT4    ;
   8:   ;
   9:  LBL[31] ;
  10:  !Controllo PLT su pinza ;
  11:  IF DI[58:Pres.Pz.PLT Mag]=ON AND DI[59:Pres.Pz.UT. Mag]=OFF,JMP LBL[30] ;
  12:  CALL MESSAGGI(24) ;
  13:  PAUSE ;
  14:  R[9:Message Allarmi]=0    ;
  15:  CALL MESSAGGI(1) ;
  16:  JMP LBL[31] ;
  17:  LBL[30] ;
  18:   ;
  19:  LBL[20] ;
  20:  !Controllo PLT su pinza ;
  21:  IF RI[3:PINZA AP.]=ON AND RI[4:PINZA CH]=OFF,JMP LBL[10] ;
  22:  CALL MESSAGGI(16) ;
  23:  PAUSE ;
  24:  R[9:Message Allarmi]=0    ;
  25:  CALL MESSAGGI(1) ;
  26:  JMP LBL[20] ;
  27:  LBL[10] ;
  28:   ;
  29:  !------------------------------ ;
  30:J P[1] 100% CNT50    ;
  31:J P[2] 100% CNT50    ;
  32:L P[3] 1500mm/sec CNT50    ;
  33:   ;
  34:  DO[202:CH.PT.Autom.MU4]=OFF ;
  35:  DO[201:AP.PT.Autom.MU4]=ON ;
  36:  ! Verifico stato CNC ;
  37:  WAIT DI[187:Ch.Scar.UT MU4]=ON AND DI[204:Porta Auto.AP.MU4]=ON    ;
  38:  DO[195:RBT Fuor.ing.MU4]=OFF ;
  39:   ;
  40:  !------------------------------- ;
  41:  !Calcolo Pos. pezzo ;
  42:  !------------------------------- ;
  43:  PR[12:POS_MU]=PR[47:Zero XYZ_NUT]    ;
  44:  PR[12,1:POS_MU]=0    ;
  45:  PR[12,2:POS_MU]=0    ;
  46:  PR[12,3:POS_MU]=0    ;
  47:  PR[12,4:POS_MU]=(-.003)    ;
  48:  PR[12,5:POS_MU]=.002    ;
  49:  PR[12,6:POS_MU]=(-90.01)    ;
  50:  PR[12,7:POS_MU]=200    ;
  51:   ;
  52:  PR[51:*]=PR[49:Zero XYZ FUT]    ;
  53:  PR[51,1:*]=(-700)    ;
  54:  PR[51,2:*]=0    ;
  55:  PR[51,6:*]=10    ;
  56:   ;
  57:  PR[52:*]=PR[49:Zero XYZ FUT]    ;
  58:  PR[52,1:*]=50    ;
  59:  PR[52,2:*]=100    ;
  60:  PR[52,3:*]=(-110)    ;
  61:  PR[52,6:*]=20    ;
  62:   ;
  63:  PR[53:*]=PR[49:Zero XYZ FUT]    ;
  64:  PR[53,3:*]=(-110)    ;
  65:   ;
  66:  PR[54:*]=PR[49:Zero XYZ FUT]    ;
  67:  PR[54,1:*]=(-1400)    ;
  68:  PR[54,2:*]=(-200)    ;
  69:  PR[54,6:*]=10    ;
  70:  PR[54,7:*]=600    ;
  71:   ;
  72:  PR[55:*]=PR[49:Zero XYZ FUT]    ;
  73:  PR[55,2:*]=100    ;
  74:   ;
  75:  !------------------------------ ;
  76:  COL DETECT ON ;
  77:  COL GUARD ADJUST 120 ;
  78:   ;
  79:L PR[12:POS_MU] 1000mm/sec CNT50 Offset,PR[52:*] Tool_Offset,PR[54:*]    ;
  80:L PR[12:POS_MU] 1000mm/sec CNT20 Offset,PR[52:*] Tool_Offset,PR[51:*]    ;
  81:L PR[12:POS_MU] 600mm/sec CNT1 Offset,PR[55:*]    ;
  82:   ;
  83:  ! Punto finale ----------------- ;
  84:L PR[12:POS_MU] 100mm/sec FINE    ;
  85:   ;
  86:  CALL P1_CHIUD    ;
  87:  !------------------------------- ;
  88:  ! Sblocco utensile ------------- ;
  89:  CALL CMD_SBL_UT_M4    ;
  90:  !------------------------------- ;
  91:   ;
  92:L PR[12:POS_MU] 200mm/sec FINE Offset,PR[53:*]    ;
  93:   ;
  94:  !------------------------------- ;
  95:  !Blocco utensile ------------- ;
  96:  CALL CMD_BL_UT_M4    ;
  97:  !------------------------------- ;
  98:  !Blocco il pallet ;
  99:  WAIT RI[3:PINZA AP.]=OFF AND RI[4:PINZA CH]=OFF    ;
 100:  COL DETECT OFF ;
 101:   ;
 102:  !------------------------------- ;
 103:L PR[12:POS_MU] 500mm/sec FINE Offset,PR[52:*]    ;
 104:L PR[12:POS_MU] 600mm/sec FINE Offset,PR[52:*] Tool_Offset,PR[51:*]    ;
 105:L PR[12:POS_MU] 1000mm/sec CNT50 Offset,PR[52:*] Tool_Offset,PR[54:*]    ;
 106:   ;
 107:  WAIT RI[3:PINZA AP.]=OFF AND RI[4:PINZA CH]=OFF    ;
 108:L P[3] 1500mm/sec CNT50    ;
 109:   ;
 110:  !FINE SCARICO ------------- ;
 111:  DO[195:RBT Fuor.ing.MU4]=ON ;
 112:  WAIT DO[195:RBT Fuor.ing.MU4]=ON    ;
 113:  WAIT    .50(sec) ;
 114:  DO[201:AP.PT.Autom.MU4]=OFF ;
 115:  DO[202:CH.PT.Autom.MU4]=ON ;
 116:  DO[187:Fin.Scar.UT MU4]=PULSE,2.0sec ;
 117:  R[6:Strobe]=3    ;
 118:  !------------------------------ ;
 119:   ;
 120:J P[2] 80% CNT50    ;
 121:J P[1] 80% CNT50    ;
 122:   ;
/POS
P[1]{
   GP1:
	UF : 9, UT : 2,	
	J1=     0.000 deg,	J2=   -54.999 deg,	J3=    24.999 deg,
	J4=     0.000 deg,	J5=   -25.000 deg,	J6=    90.000 deg,
	E1=  7000.000  mm
};
P[2]{
   GP1:
	UF : 9, UT : 2,	
	J1=   -90.000 deg,	J2=   -55.000 deg,	J3=    25.000 deg,
	J4=     0.000 deg,	J5=   -25.000 deg,	J6=    90.000 deg,
	E1=  7000.000  mm
};
P[3]{
   GP1:
	UF : 9, UT : 2,	
	J1=  -128.236 deg,	J2=   -55.560 deg,	J3=    10.951 deg,
	J4=   -73.996 deg,	J5=   -35.037 deg,	J6=   160.587 deg,
	E1=  7500.000  mm
};
/END
