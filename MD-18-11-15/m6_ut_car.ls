/PROG  M6_UT_CAR
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "CAR UT 6";
PROG_SIZE	= 3025;
CREATE		= DATE 12-04-17  TIME 00:03:20;
MODIFIED	= DATE 15-10-28  TIME 20:43:08;
FILE_NAME	= ;
VERSION		= 0;
LINE_COUNT	= 124;
MEMORY_SIZE	= 3457;
PROTECT		= READ;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= 1,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/MN
   1:  !------------------------------- ;
   2:  !CARICO la Macch M6_AWEA850_UT ;
   3:  !------------------------------- ;
   4:  UFRAME_NUM=9 ;
   5:  UTOOL_NUM=2 ;
   6:   ;
   7:  CALL USER_9_UT6    ;
   8:   ;
   9:  LBL[31] ;
  10:  !Controllo PLT su pinza ;
  11:  IF DI[58:Pres.Pz.PLT Mag]=ON AND DI[59:Pres.Pz.UT. Mag]=OFF,JMP LBL[30] ;
  12:  CALL MESSAGGI(24) ;
  13:  PAUSE ;
  14:  R[9:Message Allarmi]=0    ;
  15:  CALL MESSAGGI(1) ;
  16:  JMP LBL[31] ;
  17:  LBL[30] ;
  18:   ;
  19:  !CNT Sblocco Del pallet --------- ;
  20:  IF DI[255:OK UT.Sblocc.MU6]=OFF,CALL CMD_SBL_UT_M6 ;
  21:   ;
  22:  !------------------------------ ;
  23:J P[1] 80% CNT50    ;
  24:J P[2] 80% CNT50    ;
  25:L P[3] 1500mm/sec CNT50    ;
  26:   ;
  27:  DO[266:CH.PT.Autom.MU6]=OFF ;
  28:  DO[265:Ap PT.Autom.MU6]=ON ;
  29:  ! Verifico stato CNC ;
  30:  WAIT DI[252:Ch.Cari.UT MU6]=ON AND DI[268:Porta Auto.AP.MU6]=ON    ;
  31:  DO[259:RBT Fuor.ing.MU6]=OFF ;
  32:   ;
  33:  !------------------------------- ;
  34:  !Calcolo Pos. pezzo ;
  35:  !------------------------------- ;
  36:  PR[12:POS_MU]=PR[47:Zero XYZ_NUT]    ;
  37:  PR[12,1:POS_MU]=0    ;
  38:  PR[12,2:POS_MU]=0    ;
  39:  PR[12,3:POS_MU]=0    ;
  40:  PR[12,4:POS_MU]=(-.003)    ;
  41:  PR[12,5:POS_MU]=.002    ;
  42:  PR[12,6:POS_MU]=(-90.01)    ;
  43:  PR[12,7:POS_MU]=200    ;
  44:   ;
  45:  PR[51:*]=PR[49:Zero XYZ FUT]    ;
  46:  PR[51,1:*]=(-700)    ;
  47:  PR[51,2:*]=(-100)    ;
  48:  PR[51,6:*]=10    ;
  49:   ;
  50:  PR[52:*]=PR[49:Zero XYZ FUT]    ;
  51:  PR[52,1:*]=50    ;
  52:  PR[52,2:*]=100    ;
  53:  PR[52,3:*]=(-110)    ;
  54:  PR[52,6:*]=20    ;
  55:   ;
  56:  PR[53:*]=PR[49:Zero XYZ FUT]    ;
  57:  PR[53,3:*]=(-110)    ;
  58:   ;
  59:  PR[54:*]=PR[49:Zero XYZ FUT]    ;
  60:  PR[54,1:*]=(-1400)    ;
  61:  PR[54,2:*]=(-200)    ;
  62:  PR[54,6:*]=10    ;
  63:  PR[54,7:*]=600    ;
  64:   ;
  65:  PR[55:*]=PR[49:Zero XYZ FUT]    ;
  66:  PR[55,2:*]=100    ;
  67:   ;
  68:  PR[56:*]=PR[49:Zero XYZ FUT]    ;
  69:  PR[56,6:*]=(-1)    ;
  70:  PR[57]=PR[49:Zero XYZ FUT]    ;
  71:  PR[57,6]=3    ;
  72:  !------------------------------ ;
  73:  COL DETECT ON ;
  74:  COL GUARD ADJUST 120 ;
  75:   ;
  76:L PR[12:POS_MU] 1000mm/sec CNT50 Offset,PR[52:*] Tool_Offset,PR[54:*]    ;
  77:L PR[12:POS_MU] 1000mm/sec CNT20 Offset,PR[52:*] Tool_Offset,PR[51:*]    ;
  78:L PR[12:POS_MU] 600mm/sec CNT1 Offset,PR[52:*]    ;
  79:L PR[12:POS_MU] 300mm/sec CNT1 Offset,PR[53:*]    ;
  80:  !------------------------------ ;
  81:  ! Sblocco il pallet ------------- ;
  82:  CALL CMD_SBL_UT_M6    ;
  83:  !------------------------------ ;
  84:   ;
  85:  ! Punto finale ----------------- ;
  86:L PR[12:POS_MU] 100mm/sec FINE    ;
  87:L PR[12:POS_MU] 100mm/sec CNT1 Offset,PR[56:*]    ;
  88:L PR[12:POS_MU] 100mm/sec CNT1 Offset,PR[57]    ;
  89:   ;
  90:  !------------------------------- ;
  91:  !Blocco il pallet ;
  92:  !------------------------------- ;
  93:  !Blocco Uensile ;
  94:  CALL CMD_BL_UT_M6    ;
  95:   ;
  96:  CALL P1_APRI    ;
  97:  !------------------------------- ;
  98:   ;
  99:L PR[12:POS_MU] 600mm/sec FINE Offset,PR[55:*]    ;
 100:   ;
 101:  !------------------------------- ;
 102:  WAIT RI[3:PINZA AP.]=ON    ;
 103:  COL DETECT OFF ;
 104:   ;
 105:  !------------------------------- ;
 106:L PR[12:POS_MU] 1000mm/sec CNT50 Offset,PR[52:*] Tool_Offset,PR[51:*]    ;
 107:L PR[12:POS_MU] 1000mm/sec CNT50 Offset,PR[52:*] Tool_Offset,PR[54:*]    ;
 108:   ;
 109:  WAIT RI[3:PINZA AP.]=ON AND RI[4:PINZA CH]=OFF    ;
 110:L P[3] 1500mm/sec CNT50    ;
 111:   ;
 112:  !FINE CARICO ------------- ;
 113:  DO[259:RBT Fuor.ing.MU6]=ON ;
 114:  WAIT DO[259:RBT Fuor.ing.MU6]=ON    ;
 115:  WAIT    .50(sec) ;
 116:  DO[265:Ap PT.Autom.MU6]=OFF ;
 117:  DO[266:CH.PT.Autom.MU6]=ON ;
 118:  DO[252:Fin.Cari.UT MU6]=PULSE,2.0sec ;
 119:  R[6:Strobe]=3    ;
 120:  !------------------------------ ;
 121:   ;
 122:J P[2] 100% CNT50    ;
 123:J P[1] 100% CNT50    ;
 124:   ;
/POS
P[1]{
   GP1:
	UF : 9, UT : 2,	
	J1=     0.000 deg,	J2=   -54.999 deg,	J3=    24.999 deg,
	J4=     0.000 deg,	J5=   -25.000 deg,	J6=    90.000 deg,
	E1=  7000.000  mm
};
P[2]{
   GP1:
	UF : 9, UT : 2,	
	J1=   -90.000 deg,	J2=   -55.000 deg,	J3=    25.000 deg,
	J4=     0.000 deg,	J5=   -25.000 deg,	J6=    90.000 deg,
	E1=  7000.000  mm
};
P[3]{
   GP1:
	UF : 9, UT : 2,	
	J1=  -128.236 deg,	J2=   -55.560 deg,	J3=    10.951 deg,
	J4=   -73.996 deg,	J5=   -35.037 deg,	J6=   160.587 deg,
	E1=  7500.000  mm
};
/END
