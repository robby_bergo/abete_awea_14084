/PROG  M6_AWEA850_SCAR
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "SCAR AWEA850 6";
PROG_SIZE	= 2837;
CREATE		= DATE 12-04-17  TIME 00:03:20;
MODIFIED	= DATE 15-10-28  TIME 20:42:10;
FILE_NAME	= ;
VERSION		= 0;
LINE_COUNT	= 118;
MEMORY_SIZE	= 3285;
PROTECT		= READ;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= 1,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/MN
   1:  !------------------------------- ;
   2:  !SCARICO la Macch M6_AWEA850 ;
   3:  !------------------------------- ;
   4:  UFRAME_NUM=8 ;
   5:  UTOOL_NUM=2 ;
   6:   ;
   7:  CALL USER_8_MU6    ;
   8:   ;
   9:  LBL[31] ;
  10:  !Controllo PLT su pinza ;
  11:  IF DI[58:Pres.Pz.PLT Mag]=ON AND DI[59:Pres.Pz.UT. Mag]=OFF,JMP LBL[30] ;
  12:  CALL MESSAGGI(24) ;
  13:  PAUSE ;
  14:  R[9:Message Allarmi]=0    ;
  15:  CALL MESSAGGI(1) ;
  16:  JMP LBL[31] ;
  17:  LBL[30] ;
  18:   ;
  19:  LBL[20] ;
  20:  !Controllo PLT su pinza ;
  21:  IF RI[3:PINZA AP.]=ON AND RI[4:PINZA CH]=OFF,JMP LBL[10] ;
  22:  CALL MESSAGGI(16) ;
  23:  PAUSE ;
  24:  R[9:Message Allarmi]=0    ;
  25:  CALL MESSAGGI(1) ;
  26:  JMP LBL[20] ;
  27:  LBL[10] ;
  28:   ;
  29:  !CNT Sblocco Del pallet --------- ;
  30:  IF DI[269:PLT Sblocc.MU6]=OFF,CALL CMD_SBL_PLT_M6 ;
  31:   ;
  32:  !------------------------------ ;
  33:J P[1] 100% CNT50    ;
  34:J P[2] 100% CNT50    ;
  35:L P[3] 1500mm/sec CNT50    ;
  36:   ;
  37:  DO[266:CH.PT.Autom.MU6]=OFF ;
  38:  DO[265:Ap PT.Autom.MU6]=ON ;
  39:  ! Verifico stato CNC ;
  40:  WAIT DI[249:Ch.Scar.PLT MU6]=ON AND DI[268:Porta Auto.AP.MU6]=ON    ;
  41:  DO[259:RBT Fuor.ing.MU6]=OFF ;
  42:   ;
  43:  !------------------------------- ;
  44:  !Calcolo Pos. pezzo ;
  45:  !------------------------------- ;
  46:  PR[12:POS_MU]=PR[47:Zero XYZ_NUT]    ;
  47:  PR[12,1:POS_MU]=R[30:OFFSET_X]+0    ;
  48:  PR[12,2:POS_MU]=R[31:OFFSET_Y]+0    ;
  49:  PR[12,3:POS_MU]=0-R[32:OFFSET_Z]    ;
  50:  PR[12,4:POS_MU]=0    ;
  51:  PR[12,5:POS_MU]=0    ;
  52:  PR[12,6:POS_MU]=(-89.996)    ;
  53:  PR[12,7:POS_MU]=6500    ;
  54:   ;
  55:  PR[51:*]=PR[49:Zero XYZ FUT]    ;
  56:  PR[51,1:*]=0    ;
  57:  PR[51,2:*]=700    ;
  58:  PR[51,3:*]=200    ;
  59:   ;
  60:  PR[52:*]=PR[49:Zero XYZ FUT]    ;
  61:  PR[52,1:*]=0    ;
  62:  PR[52,2:*]=0    ;
  63:  PR[52,3:*]=150    ;
  64:   ;
  65:  PR[53:*]=PR[49:Zero XYZ FUT]    ;
  66:  PR[53,3:*]=20    ;
  67:   ;
  68:  PR[54:*]=PR[49:Zero XYZ FUT]    ;
  69:  PR[54,1:*]=0    ;
  70:  PR[54,2:*]=1400    ;
  71:  PR[54,3:*]=300    ;
  72:  PR[54,7:*]=(-600)    ;
  73:   ;
  74:  ! Sblocco il pallet ------------- ;
  75:  CALL CMD_SBL_PLT_M6    ;
  76:  !------------------------------ ;
  77:  COL DETECT ON ;
  78:  COL GUARD ADJUST 120 ;
  79:   ;
  80:L PR[12:POS_MU] 1000mm/sec CNT50 Offset,PR[54:*]    ;
  81:L PR[12:POS_MU] 1000mm/sec CNT50 Offset,PR[51:*]    ;
  82:L PR[12:POS_MU] 600mm/sec CNT20 Offset,PR[52:*]    ;
  83:L PR[12:POS_MU] 300mm/sec CNT1 Offset,PR[53:*]    ;
  84:   ;
  85:  ! Punto finale ----------------- ;
  86:L PR[12:POS_MU] 100mm/sec FINE    ;
  87:   ;
  88:  CALL P1_CHIUD    ;
  89:  CALL CMD_SBL_PLT_M6    ;
  90:  !------------------------------- ;
  91:   ;
  92:L PR[12:POS_MU] 600mm/sec FINE Offset,PR[52:*]    ;
  93:   ;
  94:  !------------------------------- ;
  95:  !Blocco il pallet ;
  96:  WAIT RI[3:PINZA AP.]=OFF    ;
  97:  COL DETECT OFF ;
  98:   ;
  99:  !------------------------------- ;
 100:L PR[12:POS_MU] 600mm/sec FINE Offset,PR[51:*]    ;
 101:L PR[12:POS_MU] 1000mm/sec CNT50 Offset,PR[54:*]    ;
 102:   ;
 103:  WAIT RI[3:PINZA AP.]=OFF    ;
 104:L P[3] 1500mm/sec CNT50    ;
 105:   ;
 106:  !FINE SCARICO ------------- ;
 107:  DO[259:RBT Fuor.ing.MU6]=ON ;
 108:  WAIT DO[259:RBT Fuor.ing.MU6]=ON    ;
 109:  WAIT    .50(sec) ;
 110:  DO[265:Ap PT.Autom.MU6]=OFF ;
 111:  DO[266:CH.PT.Autom.MU6]=ON ;
 112:  DO[249:Fin.Scar.PLT MU6]=PULSE,2.0sec ;
 113:  R[6:Strobe]=3    ;
 114:  !------------------------------ ;
 115:   ;
 116:J P[2] 80% CNT50    ;
 117:J P[1] 80% CNT50    ;
 118:   ;
/POS
P[1]{
   GP1:
	UF : 8, UT : 1,	
	J1=     -.001 deg,	J2=   -54.999 deg,	J3=    24.998 deg,
	J4=     0.000 deg,	J5=   -25.000 deg,	J6=   -90.000 deg,
	E1=  5500.000  mm
};
P[2]{
   GP1:
	UF : 8, UT : 1,	
	J1=   -16.288 deg,	J2=   -29.147 deg,	J3=     3.450 deg,
	J4=    76.255 deg,	J5=   -14.249 deg,	J6=  -165.837 deg,
	E1=  5500.000  mm
};
P[3]{
   GP1:
	UF : 8, UT : 1,		CONFIG : 'N U T, 0, 0, 0',
	X =   185.503  mm,	Y =  1748.104  mm,	Z =   856.003  mm,
	W =     0.000 deg,	P =     0.000 deg,	R =   -89.998 deg,
	E1=  5900.000  mm
};
/END
