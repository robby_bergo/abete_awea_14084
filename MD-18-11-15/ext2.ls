/PROG  EXT2	  Macro
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "CARICA PAGE1 ";
PROG_SIZE	= 224;
CREATE		= DATE 15-01-16  TIME 22:45:18;
MODIFIED	= DATE 15-02-27  TIME 16:34:12;
FILE_NAME	= EXT1;
VERSION		= 0;
LINE_COUNT	= 4;
MEMORY_SIZE	= 588;
PROTECT		= READ;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 7;
DEFAULT_GROUP	= *,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/APPL
/MN
   1:   ;
   2:  WAIT   1.00(sec) ;
   3:  CALL WEB_PAGE('PAG1') ;
   4:   ;
/POS
/END
