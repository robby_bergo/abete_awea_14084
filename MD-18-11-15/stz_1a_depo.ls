/PROG  STZ_1A_DEPO
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "Depo.Staz.1A ";
PROG_SIZE	= 1971;
CREATE		= DATE 13-07-15  TIME 19:33:46;
MODIFIED	= DATE 15-10-15  TIME 18:43:02;
FILE_NAME	= STZ_2_P3;
VERSION		= 0;
LINE_COUNT	= 71;
MEMORY_SIZE	= 2471;
PROTECT		= READ;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= 1,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/APPL
/MN
   1:  !------------------------------- ;
   2:  !Avvicinamento ai punti di presa ;
   3:  !------------------------------- ;
   4:  UFRAME_NUM=0 ;
   5:  UTOOL_NUM=2 ;
   6:   ;
   7:  LBL[31] ;
   8:  !Controllo PLT su pinza ;
   9:  IF DI[58:Pres.Pz.PLT Mag]=ON AND DI[59:Pres.Pz.UT. Mag]=OFF,JMP LBL[30] ;
  10:  CALL MESSAGGI(24) ;
  11:  PAUSE ;
  12:  R[9:Message Allarmi]=0    ;
  13:  CALL MESSAGGI(1) ;
  14:  JMP LBL[31] ;
  15:  LBL[30] ;
  16:   ;
  17:  DO[66:RICH.DEPO.BAIA-1A]=ON ;
  18:J P[1] 80% CNT50    ;
  19:J P[2] 80% CNT50    ;
  20:  !------------------------------- ;
  21:  !ATTESA ENTRARE ;
  22:  WAIT DI[64:TAPPARE.CH-1]=ON AND DI[65:OK ENTRA St1-C1]=ON AND DI[66:PLT Pres.St-1-C1]=OFF    ;
  23:  DO[64:RBT Fuor.ing.St1]=OFF ;
  24:  !------------------------------- ;
  25:  !Prelievo pezzo ;
  26:  !------------------------------- ;
  27:  PR[50:*]=PR[47:Zero XYZ_NUT]    ;
  28:  PR[50,2:*]=650    ;
  29:  PR[50,3:*]=80    ;
  30:   ;
  31:  PR[51:*]=PR[47:Zero XYZ_NUT]    ;
  32:  PR[51,3:*]=20    ;
  33:   ;
  34:  PR[52:*]=PR[47:Zero XYZ_NUT]    ;
  35:  PR[52,2:*]=10    ;
  36:   ;
  37:  PR[53:*]=PR[47:Zero XYZ_NUT]    ;
  38:  PR[53,2:*]=400    ;
  39:  PR[53,3:*]=320    ;
  40:   ;
  41:  PR[54:*]=PR[47:Zero XYZ_NUT]    ;
  42:  PR[54,2:*]=650    ;
  43:  PR[54,3:*]=320    ;
  44:   ;
  45:  !------------------------------- ;
  46:L P[3] 1000mm/sec CNT2 Offset,PR[50:*]    ;
  47:L P[3] 600mm/sec CNT2 Offset,PR[51:*]    ;
  48:   ;
  49:  !------------------------------- ;
  50:  !PUNTO FINALE ;
  51:L P[3] 100mm/sec FINE    ;
  52:  !------------------------------- ;
  53:  CALL P1_APRI    ;
  54:L P[3] 500mm/sec FINE Offset,PR[52:*]    ;
  55:   ;
  56:  WAIT RI[3:PINZA AP.]=ON AND RI[4:PINZA CH]=OFF    ;
  57:   ;
  58:L P[3] 1000mm/sec CNT10 Offset,PR[53:*]    ;
  59:   ;
  60:  CALL CNT_PRES_CONO    ;
  61:   ;
  62:L P[3] 1000mm/sec CNT100 Offset,PR[54:*]    ;
  63:   ;
  64:  !------------------------------- ;
  65:  WAIT RI[3:PINZA AP.]=ON AND RI[4:PINZA CH]=OFF    ;
  66:  DO[66:RICH.DEPO.BAIA-1A]=OFF ;
  67:  DO[64:RBT Fuor.ing.St1]=ON ;
  68:J P[2] 100% CNT50    ;
  69:J P[1] 100% CNT50    ;
  70:   ;
  71:   ;
/POS
P[1]{
   GP1:
	UF : 0, UT : 2,	
	J1=     0.000 deg,	J2=   -55.000 deg,	J3=    25.000 deg,
	J4=      .000 deg,	J5=   -25.000 deg,	J6=    90.000 deg,
	E1=  7000.000  mm
};
P[2]{
   GP1:
	UF : 0, UT : 2,	
	J1=   -19.524 deg,	J2=   -41.156 deg,	J3=    -2.945 deg,
	J4=   -88.797 deg,	J5=    70.517 deg,	J6=   176.620 deg,
	E1=  7000.000  mm
};
P[3]{
   GP1:
	UF : 0, UT : 2,		CONFIG : 'F U T, 0, 0, 0',
	X =  8179.712  mm,	Y = -1804.410  mm,	Z =   180.377  mm,
	W =      .202 deg,	P =     -.150 deg,	R =   -90.005 deg,
	E1=  7000.000  mm
};
/END
