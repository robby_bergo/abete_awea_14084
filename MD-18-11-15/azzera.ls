/PROG  AZZERA
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "AZZERA";
PROG_SIZE	= 875;
CREATE		= DATE 11-09-26  TIME 14:07:04;
MODIFIED	= DATE 15-11-18  TIME 17:16:16;
FILE_NAME	= ;
VERSION		= 0;
LINE_COUNT	= 44;
MEMORY_SIZE	= 1363;
PROTECT		= READ;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= 1,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/APPL

AUTO_SINGULARITY_HEADER;
  ENABLE_SINGULARITY_AVOIDANCE   : FALSE;
/MN
   1:  JMP LBL[2] ;
   2:  R[1:N� Missione]=0    ;
   3:  R[2:N�Baia]=0    ;
   4:  R[3:N� Maggaz.Pallet]=0    ;
   5:  R[4:N�posto staz.OP]=0    ;
   6:  R[5:Tipo di pinza]=0    ;
   7:  R[6:Strobe]=0    ;
   8:  R[7:N ID pallet Pz]=0    ;
   9:  R[8:N�Macchina UT.]=0    ;
  10:  R[10:N'POSTAZ.MIS]=0    ;
  11:  R[11:OVERRIDE]=10    ;
  12:  R[13:ERRORE_H ST1]=0    ;
  13:  R[14:ERRORE_H ST2]=0    ;
  14:   ;
  15:  LBL[2] ;
  16:  R[5:Tipo di pinza]=1    ;
  17:  R[9:Message Allarmi]=0    ;
  18:  R[10:N'POSTAZ.MIS]=10    ;
  19:  R[13:ERRORE_H ST1]=0    ;
  20:  R[14:ERRORE_H ST2]=0    ;
  21:   ;
  22:  $OVRDSLCT.$OVSL_ENB=(0) ;
  23:  R[59:APPOG_OVER]=($MCR.$GENOVERRIDE) ;
  24:   ;
  25:  R[60:OVERRIDE]=R[59:APPOG_OVER]    ;
  26:  $MCR.$GENOVERRIDE=R[60:OVERRIDE] ;
  27:   ;
  28:  DO[51:Top Master]=OFF ;
  29:  DO[65:RICH.PREL BAIA-1A]=OFF ;
  30:  DO[66:RICH.DEPO.BAIA-1A]=OFF ;
  31:  DO[67:RICH.PREL BAIA-1B]=OFF ;
  32:  DO[68:RICH.DEPO.BAIA-1B]=OFF ;
  33:  DO[69:RICH.PREL BAIA-1C]=OFF ;
  34:  DO[70:RICH.DEPO.BAIA-1C]=OFF ;
  35:  DO[71:RICH.PREL BAIA-1D]=OFF ;
  36:  DO[72:RICH.DEPO.BAIA-1D]=OFF ;
  37:  DO[74:RICH.PREL.BAIA2]=OFF ;
  38:  DO[75:RICH.DEPO.BAIA2 ]=OFF ;
  39:   ;
  40:  R[98:ACQ_SIS_1]=($SCR_GRP[1].$MCH_ANG[1]) ;
  41:   ;
  42:   ;
  43:  END ;
  44:   ;
/POS
/END
