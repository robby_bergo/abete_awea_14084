/PROG  STZ_1B_DEPO
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "Depo.Staz.1B ";
PROG_SIZE	= 2123;
CREATE		= DATE 13-07-15  TIME 19:33:46;
MODIFIED	= DATE 15-10-21  TIME 11:59:36;
FILE_NAME	= STZ_2_P3;
VERSION		= 0;
LINE_COUNT	= 82;
MEMORY_SIZE	= 2579;
PROTECT		= READ;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= 1,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/APPL
/MN
   1:  !------------------------------- ;
   2:  !Avvicinamento ai punti di presa ;
   3:  !------------------------------- ;
   4:  UFRAME_NUM=6 ;
   5:  UTOOL_NUM=1 ;
   6:   ;
   7:  LBL[31] ;
   8:  !Controllo PLT su pinza ;
   9:  IF DI[59:Pres.Pz.UT. Mag]=ON AND DI[58:Pres.Pz.PLT Mag]=OFF,JMP LBL[30] ;
  10:  CALL MESSAGGI(24) ;
  11:  PAUSE ;
  12:  R[9:Message Allarmi]=0    ;
  13:  CALL MESSAGGI(1) ;
  14:  JMP LBL[31] ;
  15:  LBL[30] ;
  16:   ;
  17:  CALL USER_6_ST1    ;
  18:   ;
  19:J P[1] 80% CNT50    ;
  20:J P[2] 80% CNT50    ;
  21:J P[3] 80% CNT50    ;
  22:   ;
  23:  !------------------------------- ;
  24:  !ATTESA ENTRARE ;
  25:  DO[68:RICH.DEPO.BAIA-1B]=ON ;
  26:  WAIT DI[64:TAPPARE.CH-1]=ON AND DI[67:OK ENTRA St1-C2]=ON AND DI[68:PLT Pres.St-1-C2]=OFF    ;
  27:   ;
  28:  DO[64:RBT Fuor.ing.St1]=OFF ;
  29:   ;
  30:  !------------------------------- ;
  31:  !Prelievo pezzo ;
  32:  !------------------------------- ;
  33:  PR[9:POS_STAZ.DP]=PR[49:Zero XYZ FUT]    ;
  34:  PR[9,1:POS_STAZ.DP]=R[30:OFFSET_X]+0    ;
  35:  PR[9,2:POS_STAZ.DP]=R[31:OFFSET_Y]+0    ;
  36:  PR[9,3:POS_STAZ.DP]=0-R[32:OFFSET_Z]    ;
  37:  PR[9,4:POS_STAZ.DP]=0    ;
  38:  PR[9,5:POS_STAZ.DP]=0    ;
  39:  PR[9,6:POS_STAZ.DP]=(-90.004)    ;
  40:  PR[9,7:POS_STAZ.DP]=7000    ;
  41:   ;
  42:   ;
  43:  PR[50:*]=PR[47:Zero XYZ_NUT]    ;
  44:  PR[50,2:*]=650    ;
  45:  PR[50,3:*]=100    ;
  46:   ;
  47:  PR[51:*]=PR[47:Zero XYZ_NUT]    ;
  48:  PR[51,3:*]=50    ;
  49:   ;
  50:  PR[52:*]=PR[47:Zero XYZ_NUT]    ;
  51:  PR[52,2:*]=20    ;
  52:  PR[52,3:*]=20    ;
  53:   ;
  54:  PR[53:*]=PR[47:Zero XYZ_NUT]    ;
  55:  PR[53,2:*]=650    ;
  56:  PR[53,3:*]=100    ;
  57:   ;
  58:  !------------------------------- ;
  59:L PR[9:POS_STAZ.DP] 1000mm/sec CNT2 Offset,PR[50:*]    ;
  60:L PR[9:POS_STAZ.DP] 600mm/sec CNT2 Offset,PR[51:*]    ;
  61:   ;
  62:  !------------------------------- ;
  63:  !PUNTO FINALE ;
  64:L PR[9:POS_STAZ.DP] 100mm/sec FINE    ;
  65:  !------------------------------- ;
  66:  CALL P1_APRI    ;
  67:L PR[9:POS_STAZ.DP] 500mm/sec FINE Offset,PR[52:*]    ;
  68:   ;
  69:  WAIT RI[4:PINZA CH]=OFF    ;
  70:   ;
  71:L PR[9:POS_STAZ.DP] 1000mm/sec CNT100 Offset,PR[53:*]    ;
  72:   ;
  73:  !------------------------------- ;
  74:  WAIT RI[4:PINZA CH]=OFF AND RI[7:PRES.PEZZO]=OFF    ;
  75:  DO[68:RICH.DEPO.BAIA-1B]=OFF ;
  76:  DO[64:RBT Fuor.ing.St1]=ON ;
  77:   ;
  78:J P[3] 100% CNT50    ;
  79:J P[2] 100% CNT50    ;
  80:J P[1] 100% CNT50    ;
  81:   ;
  82:   ;
/POS
P[1]{
   GP1:
	UF : 6, UT : 1,	
	J1=     0.000 deg,	J2=   -55.000 deg,	J3=    25.000 deg,
	J4=     0.000 deg,	J5=   -25.000 deg,	J6=   -90.000 deg,
	E1=  7000.000  mm
};
P[2]{
   GP1:
	UF : 6, UT : 1,	
	J1=     1.000 deg,	J2=   -27.334 deg,	J3=    -1.515 deg,
	J4=   -84.120 deg,	J5=    20.527 deg,	J6=    -4.879 deg,
	E1=  7000.000  mm
};
P[3]{
   GP1:
	UF : 6, UT : 1,	
	J1=    -7.011 deg,	J2=   -17.715 deg,	J3=    -2.557 deg,
	J4=   -89.533 deg,	J5=    83.482 deg,	J6=    -2.773 deg,
	E1=  7000.000  mm
};
/END
