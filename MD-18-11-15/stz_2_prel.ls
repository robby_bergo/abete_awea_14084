/PROG  STZ_2_PREL
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "Prel.Staz.2 ";
PROG_SIZE	= 3117;
CREATE		= DATE 13-07-15  TIME 19:33:54;
MODIFIED	= DATE 15-10-22  TIME 18:17:28;
FILE_NAME	= STZ_2_P3;
VERSION		= 0;
LINE_COUNT	= 128;
MEMORY_SIZE	= 3677;
PROTECT		= READ;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= 1,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/APPL
/MN
   1:  !------------------------------- ;
   2:  !Avvicinamento ai punti di presa ;
   3:  !------------------------------- ;
   4:  UFRAME_NUM=6 ;
   5:  UTOOL_NUM=1 ;
   6:   ;
   7:  LBL[31] ;
   8:  !Controllo PLT su pinza ;
   9:  IF DI[59:Pres.Pz.UT. Mag]=ON AND DI[58:Pres.Pz.PLT Mag]=OFF,JMP LBL[30] ;
  10:  CALL MESSAGGI(24) ;
  11:  PAUSE ;
  12:  R[9:Message Allarmi]=0    ;
  13:  CALL MESSAGGI(1) ;
  14:  JMP LBL[31] ;
  15:  LBL[30] ;
  16:   ;
  17:  CALL USER_6_ST2    ;
  18:   ;
  19:  LBL[20] ;
  20:  !Controllo pinza AP. ;
  21:  IF RI[3:PINZA AP.]=ON AND RI[4:PINZA CH]=OFF,JMP LBL[10] ;
  22:  CALL MESSAGGI(16) ;
  23:  PAUSE ;
  24:  R[9:Message Allarmi]=0    ;
  25:  CALL MESSAGGI(1) ;
  26:  JMP LBL[20] ;
  27:  LBL[10] ;
  28:   ;
  29:  !------------------------------- ;
  30:  DO[75:RICH.DEPO.BAIA2 ]=OFF ;
  31:  DO[74:RICH.PREL.BAIA2]=ON ;
  32:J P[1] 100% CNT50    ;
  33:J P[2] 100% CNT50    ;
  34:J P[3] 100% CNT50    ;
  35:   ;
  36:  !------------------------------- ;
  37:  !ATTESA ENTRARE ;
  38:  WAIT DI[73:OK ENTRARE Staz2]=ON AND DI[75:PLT Pres.Staz2]=ON    ;
  39:  DO[73:RBT Fuor.ing.St2]=OFF ;
  40:   ;
  41:  !------------------------------- ;
  42:  !Prelievo pezzo ;
  43:  !------------------------------- ;
  44:  PR[8:POS_STAZ.PR]=PR[49:Zero XYZ FUT]    ;
  45:  PR[8,1:POS_STAZ.PR]=R[30:OFFSET_X]+0    ;
  46:  PR[8,2:POS_STAZ.PR]=R[31:OFFSET_Y]+0    ;
  47:  PR[8,3:POS_STAZ.PR]=0-R[32:OFFSET_Z]    ;
  48:  PR[8,4:POS_STAZ.PR]=(-.191)    ;
  49:  PR[8,5:POS_STAZ.PR]=(-.106)    ;
  50:  PR[8,6:POS_STAZ.PR]=(-89.657)    ;
  51:  PR[8,7:POS_STAZ.PR]=9000    ;
  52:   ;
  53:  PR[50:*]=PR[47:Zero XYZ_NUT]    ;
  54:  PR[50,2:*]=900    ;
  55:  PR[50,3:*]=200    ;
  56:   ;
  57:  PR[51:*]=PR[47:Zero XYZ_NUT]    ;
  58:  PR[51,3:*]=50    ;
  59:   ;
  60:  PR[52:*]=PR[47:Zero XYZ_NUT]    ;
  61:  PR[52,2:*]=70    ;
  62:   ;
  63:  PR[53:*]=PR[47:Zero XYZ_NUT]    ;
  64:  PR[53,2:*]=800    ;
  65:  PR[53,3:*]=10    ;
  66:   ;
  67:  PR[54:*]=PR[47:Zero XYZ_NUT]    ;
  68:  PR[54,2:*]=150    ;
  69:  PR[54,3:*]=615    ;
  70:   ;
  71:  PR[55:*]=PR[47:Zero XYZ_NUT]    ;
  72:  PR[55,2:*]=620    ;
  73:  PR[55,3:*]=615    ;
  74:   ;
  75:  PR[56:*]=PR[47:Zero XYZ_NUT]    ;
  76:  PR[56,3:*]=(-500)    ;
  77:  !------------------------------- ;
  78:L PR[8:POS_STAZ.PR] 1000mm/sec CNT2 Offset,PR[53:*]    ;
  79:L PR[8:POS_STAZ.PR] 600mm/sec CNT2 Offset,PR[52:*]    ;
  80:   ;
  81:  !------------------------------- ;
  82:  !PUNTO FINALE ;
  83:L PR[8:POS_STAZ.PR] 100mm/sec FINE    ;
  84:  CALL P1_CHIUD    ;
  85:  !------------------------------- ;
  86:L PR[8:POS_STAZ.PR] 500mm/sec FINE Offset,PR[51:*]    ;
  87:   ;
  88:  !ATTESA PRESENZA PEZZO ;
  89:  $WAITTMOUT=100 ;
  90:  WAIT RI[4:PINZA CH]=OFF TIMEOUT,LBL[2] ;
  91:  LBL[2] ;
  92:  $WAITTMOUT=3000 ;
  93:   ;
  94:  WAIT RI[3:PINZA AP.]=OFF AND RI[4:PINZA CH]=OFF    ;
  95:  !------------------------------- ;
  96:  !CNT H.PEZZO ;
  97:  !------------------------------- ;
  98:  R[14:ERRORE_H ST2]=0    ;
  99:L PR[8:POS_STAZ.PR] 1000mm/sec CNT20 Offset,PR[54:*]    ;
 100:  SKIP CONDITION DI[74:Cnt PLT Staz2]=ON    ;
 101:   ;
 102:L PR[8:POS_STAZ.PR] 200mm/sec CNT2 Offset,PR[55:*] Skip,LBL[1]    ;
 103:  R[14:ERRORE_H ST2]=1    ;
 104:  LBL[1] ;
 105:   ;
 106:  !------------------------------- ;
 107:  IF R[14:ERRORE_H ST2]=0,JMP LBL[40] ;
 108:  PR[20:CNC  ALTO]=LPOS    ;
 109:L PR[20:CNC  ALTO] 1000mm/sec CNT100 Offset,PR[56:*]    ;
 110:   ;
 111:  LBL[40] ;
 112:  !------------------------------- ;
 113:L PR[8:POS_STAZ.PR] 1000mm/sec CNT100 Offset,PR[50:*]    ;
 114:   ;
 115:  !------------------------------- ;
 116:  DO[74:RICH.PREL.BAIA2]=OFF ;
 117:  DO[73:RBT Fuor.ing.St2]=ON ;
 118:   ;
 119:J P[3] 80% CNT50    ;
 120:J P[2] 80% CNT50    ;
 121:J P[1] 80% CNT50    ;
 122:   ;
 123:  $WAITTMOUT=500 ;
 124:  WAIT DI[76:TAPPARE.CH-2]=ON TIMEOUT,LBL[100] ;
 125:  LBL[100] ;
 126:  $WAITTMOUT=3000 ;
 127:   ;
 128:   ;
/POS
P[1]{
   GP1:
	UF : 6, UT : 1,	
	J1=     0.000 deg,	J2=   -55.000 deg,	J3=    25.000 deg,
	J4=      .000 deg,	J5=   -25.000 deg,	J6=   -90.000 deg,
	E1=  9000.001  mm
};
P[2]{
   GP1:
	UF : 6, UT : 1,	
	J1=      .892 deg,	J2=   -27.834 deg,	J3=    -5.554 deg,
	J4=   -73.640 deg,	J5=    21.203 deg,	J6=   -16.082 deg,
	E1=  8999.999  mm
};
P[3]{
   GP1:
	UF : 6, UT : 1,		CONFIG : 'F U T, 0, 0, 0',
	X =    -5.141  mm,	Y =  1090.862  mm,	Z =   427.005  mm,
	W =     -.191 deg,	P =     -.105 deg,	R =   -89.658 deg,
	E1=  9000.001  mm
};
/END
