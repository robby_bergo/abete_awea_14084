ERRCURR.LS     Robot Name ROBOT 18-NOV-15 21:10     

F Number:F00000    
 VERSION: HandlingTool          
$VERSION: V7.7035        3/6/2011 
$FEATURE[1]: HandlingTool          
$FEATURE[2]: English Dictionary    
$FEATURE[3]: AA Vision Mastering   
$FEATURE[4]: Analog I/O            
$FEATURE[5]: Auto Software Update  
$FEATURE[6]: Automatic Backup      
$FEATURE[7]: Background Editing    
$FEATURE[8]: Camera I/F            
$FEATURE[9]: Cell I/O              
$FEATURE[10]: Common shell          
$FEATURE[11]: Common shell core     
$FEATURE[12]: Common softpanel      
$FEATURE[13]: Common style select   
$FEATURE[14]: Condition Monitor     
$FEATURE[15]: Control Reliable      
$FEATURE[16]: Diagnostic log        
$FEATURE[17]: Dual Check Safety UIF 
$FEATURE[18]: Enhanced Rob Serv Req 
$FEATURE[19]: Enhanced User Frame   
$FEATURE[20]: Ext. DIO Config       
$FEATURE[21]: Extended Error Log    
$FEATURE[22]: External DI BWD       
$FEATURE[23]: FCTN Menu Save        
$FEATURE[24]: FTP Interface         
$FEATURE[25]: Group Mask Exchange   
$FEATURE[26]: High-Speed Skip       
$FEATURE[27]: Host Communications   
$FEATURE[28]: Hour Meter            
$FEATURE[29]: I/O Interconnect 2    
$FEATURE[30]: Incr Instruction      
$FEATURE[31]: KAREL Cmd. Language   
$FEATURE[32]: KAREL Run-Time Env    
$FEATURE[33]: Kernel + Basic S/W    
$FEATURE[34]: License Checker       
$FEATURE[35]: LogBook(System)       
$FEATURE[36]: MACROs, Skip/Offset   
$FEATURE[37]: MH Core               
$FEATURE[38]: MechStop Protection   
$FEATURE[39]: Mirror Shift          
$FEATURE[40]: Mixed logic           
$FEATURE[41]: Mode Switch           
$FEATURE[42]: Motion logger         
$FEATURE[43]: Multi-Tasking         
$FEATURE[44]: Position Registers    
$FEATURE[45]: Print Function        
$FEATURE[46]: Prog Num Selection    
$FEATURE[47]: Program Adjust        
$FEATURE[48]: Program Shift         
$FEATURE[49]: Program Status        
$FEATURE[50]: RDM Robot Discovery   
$FEATURE[51]: Robot Service Request 
$FEATURE[52]: Robot Servo Code      
$FEATURE[53]: SNPX basic            
$FEATURE[54]: Shift Library         
$FEATURE[55]: Shift and Mirror Lib  
$FEATURE[56]: Soft Parts in VCCM    
$FEATURE[57]: TCP Auto Set          
$FEATURE[58]: TCP/IP Interface      
$FEATURE[59]: TMILIB Interface      
$FEATURE[60]: TP Menu Accounting    
$FEATURE[61]: TPTX                  
$FEATURE[62]: Telnet Interface      
$FEATURE[63]: Tool Offset           
$FEATURE[64]: Unexcepted motn Check 
$FEATURE[65]: User Frame            
$FEATURE[66]: Vision Core           
$FEATURE[67]: Vision Library        
$FEATURE[68]: Vision SP CSXC        
$FEATURE[69]: Vision Shift Tool     
$FEATURE[70]: Web Server            
$FEATURE[71]: Web Svr Enhancements  
$FEATURE[72]: iPendant              
$FEATURE[73]: iPendant Grid Display 
$FEATURE[74]: iPendant Setup        
$FEATURE[75]: R-2000iB/125L         
$FEATURE[76]: Ascii Upload          
$FEATURE[77]: AutoSingularityAvoidM 
$FEATURE[78]: CE Mark               
$FEATURE[79]: Collision Guard       
$FEATURE[80]: Collision Guard Pack  
$FEATURE[81]: Constant Path         
$FEATURE[82]: Cycle Time Priority   
$FEATURE[83]: DHCP                  
$FEATURE[84]: Domain Name Serv.     
$FEATURE[85]: Extended Axis Control 
$FEATURE[86]: Extended User Frames  
$FEATURE[87]: FRL Params            
$FEATURE[88]: HMI Device (SNPX)     
$FEATURE[89]: High perf. softfloat  
$FEATURE[90]: Internet Conn/Custo   
$FEATURE[91]: PC Interface          
$FEATURE[92]: PMC(FAPT Ladder)      
$FEATURE[93]: PROFIBUS              
$FEATURE[94]: PROFIBUS-DP(Slave)    
$FEATURE[95]: Password Protection   
$FEATURE[96]: SNTP Client           
$FEATURE[97]: Soft Float            
$FEATURE[98]: Space Check           
$FEATURE[99]: USB port on iPendant  
$FEATURE[100]: YELLOW BOX            
$FEATURE[101]: Arc Advisor           
$FEATURE[102]: Auto SingularityAvoid 
$FEATURE[103]: Aux Servo Code        
$FEATURE[104]: Cycle time Opt.       
$FEATURE[105]: Disp 2nd analog port  
$FEATURE[106]: EMAIL Enhancements    
$FEATURE[107]: Email Client          
$FEATURE[108]: Enhanced T1 Mode      
$FEATURE[109]: Extended Axis Speed   
$FEATURE[110]: HTTP Proxy Svr        
$FEATURE[111]: IntelligentTP PC I/F  
$FEATURE[112]: JPN ARCPSU PRM        
$FEATURE[113]: PC Send Macros        
$FEATURE[114]: Real Simple Syn.(RSS) 
$FEATURE[115]: Requires CP           
$FEATURE[116]: Robot Library Setup   
$FEATURE[117]: SMB Client            
$FEATURE[118]: SSPC error text       
$FEATURE[119]: Socket Messaging      
$FEATURE[120]: Soft Limit            
$FEATURE[121]: TCPP Extention        
$FEATURE[122]: istdpnl               
$FEATURE[123]: SHADOW MOVE TO CMOS O 
$FEATURE[124]: SMB: vision interacti 
$FEATURE[125]: get_var fails to get  
$FEATURE[126]: Arc End STEP Hold Bus 
$FEATURE[127]: CVIS UPDATE IRTORCHMA 
$FEATURE[128]: CVIS UPDATE WELDTIP S 
$FEATURE[129]: CVIS: R741 should be  
$FEATURE[130]: BACKGROUND EDIT SHADO 
$FEATURE[131]: REPTCD CAN CRASH FRVR 
$FEATURE[132]: CVIS: Crashes 2-D Bar 
$FEATURE[133]: $FNO NOT RESTORED AUT 
$FEATURE[134]: JOINT QUICK STOP FLEN 
$FEATURE[135]: Local Hold TIMQ Adjus 
$FEATURE[136]: FPLN:  Support facepl 
$FEATURE[137]: FMD DEVICE ASSERT WIT 
$FEATURE[138]: KAREL CANNOT ACCESS M 
$FEATURE[139]: Joint Quick Stop FLEN 
$FEATURE[140]: USB insert & removal  
$FEATURE[141]: SMB NULL POINTER FIX  
$FEATURE[142]: MACHINE TOOL DEMO OPT 
$FEATURE[143]: PMC ETHERNET SUPPORT  
$FEATURE[144]: CPPOSTPC CAN CRASH DU 
$FEATURE[145]: CVIS Add reader visio 
$FEATURE[146]: $PASSWORD.$AUTOLOGIN  
$FEATURE[147]: CP:fix CPMO-046 issue 
$FEATURE[148]: J AUTO-T1 CPMO-130    
$FEATURE[149]: CVIS SET VARIABLES TO 
$FEATURE[150]: CVIS: 3D multi-view d 
$FEATURE[151]: Increase Number of FD 
$FEATURE[152]: CP: fix OS-144 with s 
$FEATURE[153]: Robot Settings are lo 
$FEATURE[154]: Maximum number of ele 
$FEATURE[155]: CP WAIT Mode 3 CNT0 T 
$FEATURE[156]: Mirror Image Shift ca 
$FEATURE[157]: SPOT:When viewing STY 
$FEATURE[158]: Attempt to do GET_VAR 
$FEATURE[159]: VMGR LOAD REALLOCATE  
$FEATURE[160]: Arc Production Monito 
$FEATURE[161]: SELECT FILTERING IMPR 
$FEATURE[162]: REMOVE KAREL PROGRAM  
$FEATURE[163]: RIPE:STARTUP MAIN-MAI 
$FEATURE[164]: ASCII UPLOAD OF LINE  
$FEATURE[165]: PTTB: New ML Setup me 
$FEATURE[166]: USB: memory allocatio 
$FEATURE[167]: Torch Angle supports  
$FEATURE[168]: Enhanced thickness ch 
$FEATURE[169]: Support program touch 
$FEATURE[170]: iRCalibration Signatu 
$FEATURE[171]: Support Thresh123     
$FEATURE[172]: CP:fix MOTN-110 by ma 
$FEATURE[173]: HOST : MSG_PING asser 
$FEATURE[174]: CP: All zero move tim 
$FEATURE[175]: CVIS: Improve 2D barc 
$FEATURE[176]: SOMETIMES ON STARTUP  
$FEATURE[177]: SREG: Support string  
$FEATURE[178]: SREG: KANJI string no 
$FEATURE[179]: HOST : SMB fails in c 
$FEATURE[180]: CVIS: UIF BUG FIX     
$FEATURE[181]: DOT PETTERN BUG       
$FEATURE[182]: EXPANSION DIGITAL I/O 
$FEATURE[183]: CVIS CCRG ENHANCEMENT 
$FEATURE[184]: MLOCK CHK WITH GUNCHG 
$FEATURE[185]: EXTEND PROFILE STEPS  
$FEATURE[186]: VISION MEMORYLEAK FIX 
$FEATURE[187]: AUTO CALC WRDN RATIO  
$FEATURE[188]: CVIS:SPEC CHANGE OF V 
$FEATURE[189]: VMASTER FIX           
$FEATURE[190]: GRID PATTERN BUG FIX  
$FEATURE[191]: PMC ETHERNET SUPPORT  
$FEATURE[192]: DISPENSE INST IMPROVE 
$FEATURE[193]: BROWSER TOOL RMV ADD  
$FEATURE[194]: FIX DCS COUNT3 ALARM  
$FEATURE[195]: SVGN EARLY WELD INIT  
$FEATURE[196]: STOP PATTERN DISPLAY  
$FEATURE[197]: IMP SPD SEARCH ALARMS 
$FEATURE[198]: TOUCHUP AND OFFSET    
$FEATURE[199]: ADD VISION ERROR CODE 
$FEATURE[200]: WRONG CURSOR POSITION 
$FEATURE[201]: FIX SVGN-158          
$FEATURE[202]: RSR IS NOT USABLE     
$FEATURE[203]: FIX GUNTCHUP FUNCTION 
$FEATURE[204]: TOUCHUP WITH OFFSET   
$FEATURE[205]: FIX DB BUSY RUNNING   

 MOTION PARAMETERS:: 
 ROBOT: R-2000iB/125L 
 SERVO ID: V18.00   
 Min Cycle: V3.00 
 Min Cart: V3.00 
GRP:  1 AXS:  1 MTR_ID ACaiSR30/3000 80A 
                MTR_INF: H1 DSP1-L 
                SRV_ID: P02.00
GRP:  1 AXS:  2 MTR_ID ACaiSR30/3000 80A 
                MTR_INF: H2 DSP1-M 
                SRV_ID: P02.00
GRP:  1 AXS:  3 MTR_ID ACaiSR30/3000 80A 
                MTR_INF: H3 DSP1-J 
                SRV_ID: P02.00
GRP:  1 AXS:  4 MTR_ID ACaiS12/4000 40A 
                MTR_INF: H4 DSP1-K 
                SRV_ID: P02.00
GRP:  1 AXS:  5 MTR_ID ACaiS12/4000 40A 
                MTR_INF: H5 DSP2-L 
                SRV_ID: P02.00
GRP:  1 AXS:  6 MTR_ID ACaiS12/4000 40A 
                MTR_INF: H6 DSP2-M 
                SRV_ID: P02.00
GRP:  1 AXS:  7 MTR_ID aiS40/4000 160A 
                MTR_INF: H  DSP - 
                SRV_ID: P00.39
 PWR: 23688 SRV 5193 RUN: 2126 WAIT 1349
$SCR.$NUM_GROUP  : 1
$SCR.$NUM_TOT_AXS: 7
$SCR.$MAXNUMTASK : 4
$SCR.$NUM_GROUP  : 1
$SCR.$MAXNUMUFRAM: 9
$SCR.$MAXNUMUTOOL: 10
$SCR.$NUM_GROUP  : 1
PAYLOAD          : 125

 SETUP PARAMETERS:: 
$MAXRERGNUM     : 200
$MAXPRERGNUM    : 100
HARDWARE CONFIGURATION::

         Slot  ID  FC  OP

 MEMORY INFORMATION:: 
Memory     Total      Free   Largest      Used
-----------------------------------------------
  PERM    1ee800     b33f8     b3258    13b408
   TPP     af000     5697c     5462c     58684
SYSTEM    682801       8f0       8f0    681f11
  TEMP   150d19c    8b25a4    5073e8    c5abf8
  CMOS    200000
  DRAM   2000000
  FROM   2000000
ETHERNET CONFIGURATION::
$HOSTNAME  : ROBOT 
Router Name: ROUTER 
Hardware address :  
Subnet mask :  
Server [1]:         FTP state: 3 
Server [2]:         FTP state: 3 
