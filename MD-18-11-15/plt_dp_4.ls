/PROG  PLT_DP_4
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "Depo.MAG_4";
PROG_SIZE	= 2354;
CREATE		= DATE 15-10-13  TIME 18:34:32;
MODIFIED	= DATE 15-10-22  TIME 17:25:36;
FILE_NAME	= PLT_DP_3;
VERSION		= 0;
LINE_COUNT	= 88;
MEMORY_SIZE	= 2794;
PROTECT		= READ;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= 1,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/APPL

AUTO_SINGULARITY_HEADER;
  ENABLE_SINGULARITY_AVOIDANCE   : TRUE;
/MN
   1:  !------------------------------- ;
   2:  !Avvicinamento ai punti di presa ;
   3:  !------------------------------- ;
   4:  UFRAME_NUM=4 ;
   5:  UTOOL_NUM=1 ;
   6:   ;
   7:  LBL[20] ;
   8:  !Controllo PLT su pinza ;
   9:  IF DI[59:Pres.Pz.UT. Mag]=ON AND DI[58:Pres.Pz.PLT Mag]=OFF,JMP LBL[10] ;
  10:  CALL MESSAGGI(24) ;
  11:  PAUSE ;
  12:  R[9:Message Allarmi]=0    ;
  13:  CALL MESSAGGI(1) ;
  14:  JMP LBL[20] ;
  15:  LBL[10] ;
  16:   ;
  17:  CALL PLT4_XYZ    ;
  18:   ;
  19:J P[1] 80% CNT50    ;
  20:J P[2] 80% CNT50    ;
  21:J P[3] 80% CNT50    ;
  22:  !------------------------------- ;
  23:  !DEPOSITO PLT ;
  24:  !------------------------------- ;
  25:  R[20:Valore OFFSET X]=314.5*R[19:N'COLLONNE]    ;
  26:  R[21:Valore OFFSET Y]=320*R[18:N'FILE]    ;
  27:  PR[1:Prel.Dep MAGAZ.]=PR[49:Zero XYZ FUT]    ;
  28:  PR[1,1:Prel.Dep MAGAZ.]=R[20:Valore OFFSET X]+R[23:CORRET.IN.X]-R[30:OFFSET_X]    ;
  29:  PR[1,2:Prel.Dep MAGAZ.]=R[21:Valore OFFSET Y]+R[24:CORRET.IN.Y]-R[32:OFFSET_Z]    ;
  30:  PR[1,3:Prel.Dep MAGAZ.]=R[22:CORRET.IN.Z]+R[31:OFFSET_Y]    ;
  31:  PR[1,4:Prel.Dep MAGAZ.]=(-29.242)    ;
  32:  PR[1,5:Prel.Dep MAGAZ.]=89.849    ;
  33:  PR[1,6:Prel.Dep MAGAZ.]=61.171    ;
  34:  PR[1,7:Prel.Dep MAGAZ.]=11000+R[20:Valore OFFSET X]+R[23:CORRET.IN.X]    ;
  35:   ;
  36:  PR[50:*]=PR[49:Zero XYZ FUT]    ;
  37:  PR[50,1:*]=0    ;
  38:  PR[50,2:*]=5    ;
  39:  PR[50,3:*]=600    ;
  40:   ;
  41:  PR[51:*]=PR[49:Zero XYZ FUT]    ;
  42:  PR[51,2:*]=0    ;
  43:  PR[51,3:*]=5    ;
  44:   ;
  45:  PR[52:*]=PR[49:Zero XYZ FUT]    ;
  46:  PR[52,2:*]=10    ;
  47:   ;
  48:  PR[53:*]=PR[49:Zero XYZ FUT]    ;
  49:  PR[53,2:*]=10    ;
  50:  PR[53,3:*]=700    ;
  51:   ;
  52:  PR[54:*]=PR[49:Zero XYZ FUT]    ;
  53:  PR[54,1:*]=0    ;
  54:  PR[54,2:*]=(-345)    ;
  55:  PR[54,3:*]=600    ;
  56:   ;
  57:  !------------------------------- ;
  58:L PR[1:Prel.Dep MAGAZ.] 1000mm/sec FINE Offset,PR[54:*]    ;
  59:  CALL CNT_PRES_PLT    ;
  60:   ;
  61:L PR[1:Prel.Dep MAGAZ.] 1000mm/sec CNT2 Offset,PR[53:*]    ;
  62:  IF R[133:PLT PRES.IN POST]=1,JMP LBL[1] ;
  63:  !------------------------------ ;
  64:  COL DETECT ON ;
  65:  COL GUARD ADJUST 150 ;
  66:L PR[1:Prel.Dep MAGAZ.] 800mm/sec CNT2 Offset,PR[52:*]    ;
  67:  !------------------------------- ;
  68:  !PUNTO FINALE ;
  69:L PR[1:Prel.Dep MAGAZ.] 100mm/sec FINE    ;
  70:  !------------------------------- ;
  71:  !Apre PINZA ;
  72:  COL DETECT OFF ;
  73:  CALL P1_APRI    ;
  74:  !------------------------------ ;
  75:  COL DETECT OFF ;
  76:   ;
  77:L PR[1:Prel.Dep MAGAZ.] 200mm/sec FINE Offset,PR[51:*]    ;
  78:   ;
  79:  WAIT RI[3:PINZA AP.]=ON    ;
  80:   ;
  81:L PR[1:Prel.Dep MAGAZ.] 1000mm/sec CNT20 Offset,PR[50:*]    ;
  82:  WAIT RI[3:PINZA AP.]=ON AND RI[4:PINZA CH]=OFF    ;
  83:  !------------------------------- ;
  84:   ;
  85:  LBL[1] ;
  86:L P[3] 1500mm/sec CNT50    ;
  87:J P[2] 100% CNT100    ;
  88:J P[1] 100% CNT100    ;
/POS
P[1]{
   GP1:
	UF : 4, UT : 1,	
	J1=     -.000 deg,	J2=   -54.999 deg,	J3=    24.998 deg,
	J4=     -.000 deg,	J5=   -25.000 deg,	J6=   -90.000 deg,
	E1= 11000.000  mm
};
P[2]{
   GP1:
	UF : 4, UT : 1,	
	J1=    -8.942 deg,	J2=    -7.203 deg,	J3=    -8.202 deg,
	J4=    68.730 deg,	J5=    26.804 deg,	J6=  -159.143 deg,
	E1= 11000.000  mm
};
P[3]{
   GP1:
	UF : 4, UT : 1,	
	J1=   -13.912 deg,	J2=   -19.869 deg,	J3=    -5.513 deg,
	J4=    91.626 deg,	J5=   103.844 deg,	J6=  -174.186 deg,
	E1= 11000.000  mm
};
/END
