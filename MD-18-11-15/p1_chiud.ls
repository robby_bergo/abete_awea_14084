/PROG  P1_CHIUD
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "CHIUDI PINZA 1";
PROG_SIZE	= 408;
CREATE		= DATE 07-04-06  TIME 19:45:16;
MODIFIED	= DATE 15-10-15  TIME 14:52:32;
FILE_NAME	= ;
VERSION		= 0;
LINE_COUNT	= 14;
MEMORY_SIZE	= 888;
PROTECT		= READ;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= *,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/MN
   1:  !------------------------------- ;
   2:  !CHISURA PINZA ;
   3:  !------------------------------- ;
   4:   ;
   5:  $WAITTMOUT=300 ;
   6:  RO[3:AP PZ]=OFF ;
   7:  RO[4:CH.PZ]=ON ;
   8:  WAIT RI[4:PINZA CH]=ON TIMEOUT,LBL[1] ;
   9:  WAIT    .50(sec) ;
  10:   ;
  11:  LBL[1] ;
  12:  $WAITTMOUT=3000 ;
  13:  END ;
  14:   ;
/POS
/END
